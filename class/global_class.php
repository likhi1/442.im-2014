<?php
//header("Content-Type: text/html; charset=utf-8"); 
require_once('db_connect.php');
require_once('db_class.php');
class GlobalClass   extends db_class { 

var $shortMonth=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
var $longMonth=array("January","February","March","April","May","June","July","August","September","October","November","December");
var $thaiMonth = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
var $engDay =array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
 
 
 	 
	
/*------------ Method Date Time ---------------------------------------------------------------------------------*/	
//Date Time Formate from yyyy-mm-dd h:i:s example : 2008-10-30 12:30:00  

	function convert2Timestamp($txt) { 
		$D=substr($txt,8,2);
		$M=substr($txt,5,2);
		$Y=substr($txt,0,4);
		$H=substr($txt,11,2);
		$I=substr($txt,14,2);
		$S=substr($txt,17,2); 
		return mktime($H,$I,$S,$M,$D,$Y);
	}	
			  
	 function ShortDate($txt) {
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year;
	}	

	function ShortDateTime($txt){
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,8);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year." ".$Time;
	}	
	
	function shortThaiDateTime($txt){
		$Year=  substr(substr($txt,0,4)+543   ,  2);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,5);
		$Month=$Month-1;
		return $DayNo." ".$this->thaiMonth[$Month].$Year." - ".$Time;

       // return $txt; // check  format of DateTime
	}	

	function iframeSet() {
		$iframe='<iframe id="savetarget" name="savetarget" src="" style="width:0px;height:0px;border:0"></iframe>';
		echo $iframe;
	}					
	 
		
}// end class
?>