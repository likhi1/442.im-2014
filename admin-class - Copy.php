<?php
require_once('class/db_connect.php');

class admin_class extends database {
	var $tblAdmin='tbl_admin';
	var $tblOffer='tbl_offers';
	var $tblEvent='tbl_event';
	var $tblNationality='tbl_nationality';
	var $tblSubscribe='tbl_subscribe';
	var $tblNewsletter='tbl_newsletter';
	var $tblHistory='tbl_newsletter_history';
	var $tblConfig='website_setting';
	var $tblPhone='tbl_phone';
	var $tblSport='tbl_sport';
	var $tblCasino='tbl_casino';
	var $tblLotto='tbl_lotto';
	var $tblScore='tbl_score';
	
	var $context='';
	
	//File Path for CMS -------------------------------------
	var $BasePath='../adminweb/fckeditor/';
		
	//newsletter
	var $NewsletterFilePath='newsletter-images';
	var $PresidentFilePath='images-store';
	
	function CheckExistValue($FieldName,$TableName,$Str) {
		$Sql="SELECT ".$FieldName." FROM ".$TableName." WHERE ".$FieldName."='".$Str."' LIMIT 1; ";
		$this->query($Sql);
		if ($this->num_rows()>0) {	return true; } else { return false; }
	}
	
	function ValidEmail($email) {
		if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
			return false;			
		return true;
	}
	
	function checkLogin($user, $pass) {
		$Sql="SELECT * FROM ".$this->tblAdmin." WHERE AdminUser='$user' AND AdminPass='$pass' AND AdminStatus<>'0' LIMIT 1; ";
		$this->query($Sql);
		if ($this->num_rows()>0) 
			return true;
		return false;
	}	
	
	
	function getAgeList($ListID, $Option=NULL) {
		if ($ListID=='18 to 25') { $Age1=' selected="selected" '; }
		if ($ListID=='26 to 34') { $Age2=' selected="selected" '; }
		if ($ListID=='35 to 44') { $Age3=' selected="selected" '; }
		if ($ListID=='45 to 55') { $Age4=' selected="selected" '; }
		if ($ListID=='55 above') { $Age5=' selected="selected" '; }
		
		if ($Option) {
			$Option='<option value="0">-- all --</option>';
			$width='  style="width:130px;" ';
		} else { $Option=''; $width=' style="width:200px;" '; }
		
		$AgeList='
			<select name="sAge" class="txtBlack11" id="sAge" '.$width.'>	'.$Option.'		
			  <option value="18 to 25" '.$Age1.'>18 to 25</option>
			  <option value="26 to 34" '.$Age2.'>26 to 34</option>
			  <option value="35 to 44" '.$Age3.'>35 to 44</option>
			  <option value="45 to 55" '.$Age4.'>45 to 55</option>
			  <option value="55 above" '.$Age5.'>55 above</option>
			</select>
		';
		return $AgeList;
	}
	
	//Write RSS
	function WritePromotionRSS() {
		$FileName='../files/promotionRSS.xml';
		$Sql=" SELECT offerID, Title, Thumbnail, ShortDefinition, Description, LastUpdate ".
				" FROM ".$this->tblOffer.
				" WHERE IsDisplay='1' ".
				" ORDER BY SortOrder ";
		$NumAll=$this->query($Sql);
		if ($NumAll>0) {
			$data ='<?xml version="1.0" encoding="utf-8" ?>
						<rss version="2.0">
						<channel>
							<generator>RSS Builder by Grand President Bangkok Apartments</generator> 
							<title>Grand President Bangkok Apartments</title> 
							<link>http://www.grandepresident.com/</link> 
							<description>Bangkok Apartments - Promotion</description> 
							<language>en-us</language>';
		
			while ($this->moveNext()) {
			
				$offerID=$this->getField('offerID');
				$Thumbnail=$this->getField('Thumbnail');
				$Title=stripslashes($this->getField('Title'));
				$ShortDefinition=stripslashes($this->getField('ShortDefinition'));
				//$Description=stripslashes($this->getField('Description'));		
				$LastUpdate=$this->getField('LastUpdate');
			
				$Detail=$this->DisplayOfferList($offerID, $Thumbnail, $Title, $ShortDefinition);
				
				$data .= '<item>'." ";
				$data .= '<title>'.$Title.'</title>'." ";
				$data .= '<description><![CDATA['.$Detail.']]></description>'." ";
				$data .= '<link>'.$this->FilesPath.'/offers-info.php?id='.$offerID.'</link>'." ";
				$data .= '<pubDate>'.date('l dS \of F Y h:i:s A',strtotime($LastUpdate)).'</pubDate>'." ";
				$data .= '</item>'." ";
			}// end while
			$data .= '</channel>'." ";
			$data .= '</rss>'." ";
		}
		
		$filenum = fopen($FileName, 'w' );
		fputs($filenum, $data );
		fclose($filenum);	
	}
		
	function getNationalityList($selectID) {				
		$Sql="SELECT * FROM ".$this->tblNationality." ORDER BY Nationality  ";
		$this->query($Sql);
		
		$Nationality.=' <select name="sNationality" class="txtBlack11" id="sNationality" style="width:200px">'.chr(13);
		if (!$selectID) {
			$Nationality.='<option value="0">--- Select ---</option>';
		} 
		if ($this->num_rows()>0) {
			
			while ($this->moveNext()) {
				$nID=$this->getField('NationalityID');
				$nName=stripslashes($this->getField('Nationality'));
				
				if ($selectID && $selectID==$nID) {
					$select=' selected="selected" ';
				} else {
					$select='';
				}
				$Nationality.='<option value="'.$nID.'" '.$select.'>'.$nName.'</option>'.chr(13);
				
			}//end while
		}//end num
		$Nationality.='</select>';
		return $Nationality;
	}
	
			
	function getNationalityListTrue($selectID=NULL) {		
			
		$Sql=" SELECT sNationality FROM ".$this->tblSubscribe.
				" GROUP BY sNationality ";
				
		$this->query($Sql);
		
		$Nationality.=' <select name="sNationality" class="txtBlack11" id="sNationality" style="width:130px;">';
		$Nationality.='<option value="0">--- All ---</option>';
		
		if ($this->num_rows()>0) {
			
			while ($this->moveNext()) {				
				$nName=$this->getField('sNationality');
				
				if ($selectID && $selectID==$nName) {
					$select=' selected="selected" ';
				} else {
					$select='';
				}
				
				$Nationality.='<option value="'.$nName.'" '.$select.'>'.ucwords($nName).'</option>'.chr(13);
				
			}//end while
		}//end num
		$Nationality.='</select>';
		return $Nationality;
	}
	
	function SaveResult($Text) {
		$result='<table width="500" border="0" cellpadding="0" cellspacing="0" id="result" style="visibility:hidden">
                <tr>
                  <td width="29" height="29" background="images/bgico.jpg">
				  	<img src="images/ico1.jpg" width="26" height="29" align="absmiddle" alt="result" /></td>
                  <td background="images/bgico.jpg" class="txtBlack11"><strong>'.$Text.'</strong></td>
                  <td width="46" align="right" class="txtBlack11"><img src="images/bgico2.jpg" width="46" height="29" /></td>
                </tr>
              </table>';
			  return $result;
	}
	
	function lineOver() {
		$Line=' onmouseover="this.bgColor=\'#efefef\';" onmouseout="this.bgColor=\'\';" ';
		return $Line;
	}
	
	function getNextOfferOrder() {
		$Sql="SELECT MAX( SortOrder ) AS MaxOrder FROM ".$this->tblOffer;
		$Re=mysql_query($Sql);
		$maxOrder=mysql_result($Re,0);
		mysql_free_result($Re);	
		
		if (empty($maxOrder)) {
			$nextOrder=1;
		} else {
			$nextOrder=($maxOrder+1);
		}
		return $nextOrder;
	}
	
	//Dissplay Offers List for RSS
	function DisplayOfferList($offerID, $Thumbnail, $Title, $ShortDefinition) {		
		
		if ($Thumbnail!='') {
			$Thumbnail='<div>
									<a href="'.$this->FilesPath.'/offers-info.php?id='.$offerID.'">
										<img src="'.$this->FilesPath.'/offers-images/'.$Thumbnail.'" alt="'.$Title.'" border="0" width="138" height="130" />
									</a>
								</div>';
		} else {
			$Thumbnail='<div '.$style.'>
									<img src="'.$this->FilesPath.'offers-images/not_available.gif" alt="Image Not Available.gif" border="0" width="132" height="70" />
								</div>';
		}
		
	
		$OfferList='
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  	<td width="144" align="center" valign="top">'.$Thumbnail.'</td>
			  	<td width="10">&nbsp;</td>
			  	<td valign="top"  class="blacktext">					
					<div style="padding-top:5px;">'.$ShortDefinition.' '.$BookButton.'</div>
				</td>
			 </tr>
		  	</table>
		';
		return $OfferList;
	}
	
	function getMonthEvent($getMonth=NULL) {	
		$Month=' <select name="EventMonth" class="txtBlack11" id="EventMonth">';
		
		for ($i=0;$i<12;$i++) {
			if ($getMonth) {
				$m=($getMonth-1);
				if ($i==$m) {
					$Select=' selected="selected" ';
				} else {
					$Select='';
				}
			}
			$Month.='<option value="'.($i+1).'" '.$Select.'>'.$this->longMonth[$i].'</option>';
		}
		$Month.='</select>';
		
		return $Month;
	}
	
	function showPage() {
		$result= $this->pu_pageloop($this->varURL);
		if ($result) {
			return 'Page: '.$result;
		} else {
			return $result;
		}
	}
	
  function check_email_address($email) {
  	 // First, we check that there's one @ symbol, and that the lengths are right
	   if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
	   // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
	   		return false;
	   }
	   
	   // Split it into sections to make life easier
	   $email_array = explode("@", $email);
	   $local_array = explode(".", $email_array[0]);
  		for ($i = 0; $i < sizeof($local_array); $i++) {
		   if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
			return false;
		   }
		}
  
  		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
  			$domain_array = explode(".", $email_array[1]);
   				if (sizeof($domain_array) < 2) {
   					return false; // Not enough parts to domain
			   }
   			for ($i = 0; $i < sizeof($domain_array); $i++) {
   				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
   					return false;
   				}
  			}
   		}
	  return true;
  }
  
  	/* Setting Website  Config ------------------------------------------------------------------*/
	function getConfig() {
		$Sql="SELECT * FROM ".$this->tblConfig;
		database::query($Sql);
		
		if (database::num_rows()>0) 
		{
			$this->context=array();
			while (database::moveNext()) 
			{
				$Name=database::getField('conf_name');
				$Value=stripslashes(database::getField('conf_value'));
				
				$this->context[$Name]=$Value;
			}//end while
						
		} else {
			$this->context='';
		}
	}
	
	function updateConfig($name, $value) {
		$Sql="UPDATE ".$this->tblConfig.
				" SET conf_value='".addslashes($value)."' ".
				" WHERE conf_name='$name' ".
				" LIMIT 1; ";
		return database::query($Sql);
	}
	
}//end admin class
?>