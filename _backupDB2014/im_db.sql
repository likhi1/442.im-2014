-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2012 at 08:09 AM
-- Server version: 5.1.63
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `im_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `AdminUser` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `AdminPass` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `AdminStatus` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`AdminUser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`AdminUser`, `AdminPass`, `AdminStatus`) VALUES
('admin', 'login258456', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_casino`
--

CREATE TABLE IF NOT EXISTS `tbl_casino` (
  `CasinoId` int(10) NOT NULL AUTO_INCREMENT,
  `GameID` int(10) NOT NULL,
  `Link` varchar(255) NOT NULL,
  PRIMARY KEY (`CasinoId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `tbl_casino`
--

INSERT INTO `tbl_casino` (`CasinoId`, `GameID`, `Link`) VALUES
(1, 1, 'http://www.viva3388.com'),
(2, 2, 'http://bbb.bacc1688.com/'),
(3, 1, 'http://www.viva9988.com'),
(16, 3, 'http://new.reddragon88.net/OLTGames/index.jsp'),
(17, 3, 'http://www.reddragon88.net'),
(18, 3, 'http://reddragon88.pentorbet.com/Login.html'),
(5, 2, 'http://cdn1.bacc1688.com/app/bacc168/gameroomTg.exe'),
(6, 2, 'http://cdn1.bacc1688.com/app/bacc168/gameroomEn.exe'),
(7, 1, 'http://neptune.viva9988.com/LobbyLogin.aspx?lg=EN&token=f6faf28bb4e9fe92ad5cf7b7d9be5d36'),
(8, 1, ''),
(9, 1, ''),
(10, 1, ''),
(11, 1, ''),
(12, 2, 'Note....!  ในการเล่นผ่าน ช่องทาง 01 จะไม่สามารถมองเห็นเกมส์ได้ทั้งหมดของ G-Club'),
(13, 2, ''),
(14, 2, ''),
(15, 2, ''),
(19, 3, ''),
(20, 3, ''),
(21, 3, ''),
(22, 3, ''),
(23, 4, 'http://www.855crown.com/cbv855/index.jsp?language=th'),
(24, 4, 'http://www.855crown.info'),
(25, 4, 'http://www.cbv855.com'),
(26, 4, 'http://www.cbv855.info'),
(27, 4, 'http://www.cbv888.com'),
(28, 4, 'http://www.cbv888.info'),
(29, 4, ''),
(30, 5, 'http://www3.royal1688.com/frame.php?2409'),
(31, 5, 'http://cath.gclub888.com/royal1/setup_th_th.exe'),
(32, 5, 'http://cath.gclub888.com/royal1/setup_en_us.exe'),
(33, 5, 'Note...!  หากใช้ช่องทางที่ 01 ต้องทำการติดตั้ง Flash Player ก่อนนะครับ  สามารถ Download ได้ในช่องทางที่ 05'),
(34, 5, 'http://www3.royal1688.com/install_flash_player_11_active_x_32bit.exe'),
(35, 5, ''),
(36, 5, ''),
(37, 6, 'http://www.ho788.com/mem/game/game.jsp?languages=Tg'),
(38, 6, 'http://www.ho788.com/mem/game/game.jsp?languages=En'),
(39, 6, 'http://dwn.ho788.com/mem/download/casinoTH.exe'),
(40, 6, 'http://dwn.ho788.com/mem/download/casinoEN.exe'),
(41, 6, 'http://www.ho788.com'),
(42, 6, ''),
(43, 6, ''),
(44, 7, 'http://www.live012.com/'),
(45, 7, 'http://www.live015.com/'),
(46, 7, 'http://www.live228.com/'),
(47, 7, ''),
(48, 7, ''),
(49, 7, ''),
(50, 7, ''),
(51, 8, 'https://casino.sbobet.com/'),
(52, 8, ''),
(53, 8, ''),
(54, 8, ''),
(55, 8, ''),
(56, 8, ''),
(57, 8, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lotto`
--

CREATE TABLE IF NOT EXISTS `tbl_lotto` (
  `LottoId` int(10) NOT NULL AUTO_INCREMENT,
  `GameID` int(10) NOT NULL,
  `Link` varchar(255) NOT NULL,
  PRIMARY KEY (`LottoId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_lotto`
--

INSERT INTO `tbl_lotto` (`LottoId`, `GameID`, `Link`) VALUES
(3, 1, 'http://www.cbv855.info'),
(4, 1, 'http://www.855crown.com/cbv855/index.jsp?language=th'),
(5, 2, 'http://www.bk2541.com'),
(6, 1, 'aaa'),
(7, 1, 'aa'),
(8, 1, ''),
(9, 1, ''),
(10, 1, ''),
(11, 2, 'http://www.bk2541.com/aos/'),
(12, 2, ''),
(13, 2, ''),
(14, 2, ''),
(15, 2, ''),
(16, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail`
--

CREATE TABLE IF NOT EXISTS `tbl_mail` (
  `MailID` int(11) NOT NULL AUTO_INCREMENT,
  `MailName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MailEmail` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MailTel` varchar(20) CHARACTER SET utf8 NOT NULL,
  `MailMsg` varchar(255) CHARACTER SET utf8 NOT NULL,
  `MailDateTime` datetime NOT NULL,
  `MailIp` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`MailID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `tbl_mail`
--

INSERT INTO `tbl_mail` (`MailID`, `MailName`, `MailEmail`, `MailTel`, `MailMsg`, `MailDateTime`, `MailIp`) VALUES
(15, 'new', 'new_q_oo@hotmail.com', '1234', 'แจ้งโอนทางนี้ได้ป้ะคับ  แอบเล่น 555+\r\n\r\nตอบทางเมลเลยครับ  \r\n\r\nตอบเร็ว โอนทันที ^^', '2012-03-28 19:28:19', '101.109.132.171'),
(16, 'a139', 'mezahaha@hotmail.com', '0907677464', 'ทำไมติดต่อไม่ได้', '2012-04-11 10:28:16', '49.49.88.177'),
(17, 'sakarin', 'sakarin@hotmail.com', '0906687366', 'เล่น', '2012-04-11 19:31:48', '110.49.232.3'),
(18, 'thelong', 'chahlong@yahoo.co.th', '0800402259', 'สมัคร', '2012-04-14 10:26:43', '118.173.57.73'),
(19, 'uree', 'seniorbet@hotmail.co.th', '0827040222-333-444', 'สนใจอยากร่วมงานกับทางคุณครับ\r\nmaster sb188 อยากทราบรายละเอียดครับ', '2012-04-28 06:34:58', '125.24.225.8'),
(20, 'สุวรรณี', 'Penyapa@hotmail.com', '0896660893', 'เข้า Login ของ Gclub ไม่ได้คะ  ( ขึ้นว่า ชื่อบํญชีและรหัสไม่ถูกต้อง หรือ บัญชีถูกระงับการใช้งาน )คะ', '2012-05-19 20:28:15', '124.120.16.233'),
(21, 'อภิญญา  ศรีพรมมา', 'oil69lio@gmail.com', '0876112111', 'ขอเปิดหน้า Master ต้องทำอย่างไรบ้างคร้า และรวมถึง   Gclub ด้วยรึป่าวคร้า หรือว่าแค่ sbobet อย่างเดียว', '2012-06-02 11:27:30', '110.171.124.60'),
(22, 'rachan', 'rachan.o1@hotmail.com', '0813540924', 'อยากทราบรายละเอียดของการเล่นพนันว่ามีออะไรบ้าง  แล้วเล่นอย่างไร  มีการให้ทดลองเข้าเล่นมั้ย  ', '2012-06-04 00:55:12', '27.55.9.162'),
(23, 'ดวงดาว   วงค์ชัย', 'skydaow_lovely@hotmail.com', '0896501461', 'ขอเลขบัญชีโอนเงินด้วยค่ะ\r\n', '2012-06-06 17:22:49', '118.173.68.19'),
(24, 'son', 'gaghaha001@hotmail.com', '0800411050', 'ผมจะแทงบอล', '2012-06-09 10:33:15', '1.0.234.255'),
(25, 'son', 'gaghaha001@hotmail.com', '0904073312', 'แทงบอล', '2012-06-09 12:39:15', '1.0.234.255'),
(26, 'ทดสอบ', 'xx@xx.com', '123456', 'ทดสอบ', '2012-06-27 10:59:00', '115.87.231.121'),
(27, 'god', 'gamo_2010@hotmail.com', '0900919901', 'แบ่งสู้ ibc สูงสุดได้เท่าไรครับ แล้วมีรายละเอียดอะไรบ้าง รบกวนตอบด้วนนะครับ ขอบคุณครับ', '2012-07-18 19:56:06', '182.52.15.58'),
(28, 'ทวีชัย', 'thawichok_love@hotmail.com', '0828009252', 'สมัครเล่นเกมยังไง  จ.ภูเก็ต', '2012-07-24 00:44:45', '213.229.109.166'),
(29, 'chakkaphong', 'dond88@windowslive.com', '0860671189', 'สนใจ   สอบถาม', '2012-08-07 07:32:58', '213.229.109.166'),
(30, 'chakkaphong', 'dong88@windowslive.com', '0860671189', 'สอบถาม วิธีการเล่น', '2012-08-08 08:46:11', '213.229.109.166'),
(31, 'สัญยา  ', 'service_mitsu@hotmail.com', '0836938649', 'ผมเป็นลูกค้าเล่นทุกวัน p.33829 ลองเช้คดูผมเปิดคอม 3 เครื่อง ไพรไม่ตรงกัน  ตอบผมหน่อย', '2012-08-11 16:29:39', '213.229.109.166'),
(32, 'กอล์ฟ', 'golfyno2009@hotmail.com', '082-8002629', 'ิอยากได้ค่าคอม วันนี้จังเลยคับ  15-08-55', '2012-08-15 02:31:23', '213.229.109.166');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phone`
--

CREATE TABLE IF NOT EXISTS `tbl_phone` (
  `PhoneID` int(11) NOT NULL AUTO_INCREMENT,
  `PhoneNumber` varchar(30) NOT NULL,
  `sDate` datetime NOT NULL,
  `PhoneIP` varchar(50) NOT NULL,
  PRIMARY KEY (`PhoneID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `tbl_phone`
--

INSERT INTO `tbl_phone` (`PhoneID`, `PhoneNumber`, `sDate`, `PhoneIP`) VALUES
(78, '0812729874', '2012-06-09 12:22:28', '110.49.234.130'),
(103, '0828009252', '2012-07-24 11:21:01', '213.229.109.166'),
(77, '0812729874', '2012-06-09 12:21:48', '110.49.234.130'),
(102, '0828009252', '2012-07-24 11:12:46', '213.229.109.166'),
(74, '0895921735', '2012-06-08 13:41:22', '1.0.226.158'),
(101, '0828009252', '2012-07-24 08:56:51', '213.229.109.166'),
(71, '0875084374', '2012-06-04 08:33:55', '110.49.232.41'),
(72, '0896501461', '2012-06-06 16:40:20', '118.173.68.19'),
(73, '0874703887', '2012-06-07 15:38:23', '180.183.193.66'),
(79, '0812729874', '2012-06-09 12:23:46', '110.49.234.130'),
(80, '0812729874', '2012-06-09 12:23:56', '110.49.234.130'),
(81, '0812729874', '2012-06-09 12:24:57', '110.49.234.130'),
(82, '0812729874', '2012-06-09 22:45:25', '110.77.140.9'),
(83, '0812729874', '2012-06-10 02:07:26', '110.49.243.201'),
(84, '0856559887', '2012-06-14 21:10:20', '101.109.245.146'),
(85, '0848454572', '2012-06-14 23:37:13', '118.173.65.178'),
(86, '0848454572', '2012-06-14 23:46:35', '118.173.65.178'),
(87, '0904035938', '2012-06-18 19:43:32', '110.171.168.254'),
(88, '0904035938', '2012-06-19 01:42:19', '110.171.168.254'),
(89, '0881671646', '2012-06-20 05:07:04', '1.0.239.86'),
(90, '0854784948', '2012-06-21 07:01:33', '49.229.155.178'),
(91, '0844784948', '2012-06-21 07:06:23', '49.229.155.178'),
(92, '0844784948', '2012-06-21 07:07:54', '49.229.155.178'),
(93, '0844784948', '2012-06-21 07:09:15', '49.229.155.178'),
(94, '0844784948', '2012-06-21 07:15:36', '49.229.155.178'),
(95, '0873822505', '2012-07-06 19:24:14', '110.168.46.182'),
(96, '0840656232', '2012-07-09 15:22:44', '223.207.78.240'),
(97, '0827141618', '2012-07-10 04:52:48', '180.183.90.98'),
(98, '0862670582', '2012-07-13 17:10:56', '171.100.217.204'),
(99, '0872670812', '2012-07-17 19:22:24', '183.88.80.55'),
(100, '0896497164', '2012-07-21 20:40:12', '27.55.9.111'),
(104, '0828009252', '2012-07-24 11:21:06', '213.229.109.166'),
(105, '0873128526', '2012-07-27 02:59:39', '213.229.109.166'),
(106, '0860671189', '2012-08-08 08:59:22', '213.229.109.166'),
(107, '0887338522', '2012-08-11 12:57:20', '213.229.109.166'),
(108, '0807023628', '2012-08-12 18:28:11', '213.229.109.166'),
(109, '0861777727', '2012-08-12 18:29:50', '213.229.109.166'),
(110, '0824209188', '2012-08-15 17:08:00', '213.229.109.166');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_score`
--

CREATE TABLE IF NOT EXISTS `tbl_score` (
  `ScoreID` int(10) NOT NULL AUTO_INCREMENT,
  `GameID` int(10) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Link` varchar(255) NOT NULL,
  PRIMARY KEY (`ScoreID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_score`
--

INSERT INTO `tbl_score` (`ScoreID`, `GameID`, `Name`, `Link`) VALUES
(1, 1, '', ''),
(2, 1, 'Sport  live..!', 'http://www.lshunter.tv/webmasters.html '),
(3, 1, 'Sport  live..!', 'http://lsh.lshunter.tv/index.php?option=com_lsh&view=lsh&layout=webmaster&tmpl'),
(4, 1, 'Sport  live..!', 'http://88.80.16.174/index.php?option=com_lsh&view=lsh&layout=webmaster&tmpl=component&sections=35&bg_color=f02648&font_type=Times%20New%20Roman&font_size=12px&font_color=edf3f5&link_color=e85217&link_hover_color=ebe2df&start_time_eventtitle_font_size=12px'),
(5, 1, 'Sport  live..!', 'http://www.atdhenet.tv/'),
(6, 1, 'Sport  live..!', 'http://www.rojadirecta.me/'),
(7, 1, 'Sport  live..!', 'http://www.sportlemon.tv/webmasters/webmaster_iframe.php?layout=webmaster_iframe&sport[]=1&timezone=0&fontcolor=000000&linkcolor=000000&backgroundcolor=d0d4d9&hoverlinkcolor=000000&contentbgcolor=ffffff&activetabbgcolor=ffffff&notactivetabbgcolor=6b7d98&a'),
(8, 1, '', ''),
(9, 2, 'NowGoal', 'http://www.nowgoal.com/asianbookie.htm'),
(10, 2, '7m', 'http://free.7m.cn/live.aspx?mark=th&TimeZone=+0700&wordAd=sportsforexclusive.com&wadurl=http://sportsforexclusive.com&width=680&cpageBgColor=ffffff&tableFontSize=12&cborderColor=78C9E6&ctdColor1=DCF0F8&ctdColor2=FFFFFF&clinkColor=248DB5&cdateFontColor=4EB'),
(11, 2, 'SoccerStand', 'http://www.soccerstand.com/'),
(12, 2, 'gooooal.com', 'http://en.gooooal.com/soccer/live/'),
(13, 2, 'Livescore', 'http://www.livescore.com/'),
(14, 2, 'SPBO', 'http://live5.spbo.com/'),
(15, 2, 'ESPN   All', 'http://soccernet.espn.go.com/scores?league=all&cc=4716'),
(16, 2, 'Goals..365', 'http://www.goals365.com/index.php'),
(17, 3, '', ''),
(18, 3, 'BasKetBall', 'http://basket.7m.cn/free_en.aspx'),
(19, 3, '', ''),
(20, 3, 'Tennis', 'http://www.tennis.com/livescores/index.aspx');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sport`
--

CREATE TABLE IF NOT EXISTS `tbl_sport` (
  `SportID` int(11) NOT NULL AUTO_INCREMENT,
  `GameID` int(10) DEFAULT NULL,
  `Link` text,
  PRIMARY KEY (`SportID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `tbl_sport`
--

INSERT INTO `tbl_sport` (`SportID`, `GameID`, `Link`) VALUES
(1, 1, 'http://sports.sbo222.com/th-th/?p=sport'),
(2, 1, 'http://sports.sbo333.com/th-th/?p=sport'),
(3, 1, 'http://sports.clocksea.com/th-th/?p='),
(4, 1, 'http://sports.sbo168.com/th-th/?p='),
(5, 1, 'http://sports.sbo128.com/th-th/?p=sport'),
(6, 2, 'http://www.betwing.com'),
(7, 2, 'http://www.betrapid.com'),
(8, 2, 'http://www.155551.com'),
(9, 3, ''),
(10, 3, ''),
(11, 1, 'http://sports.sbo555.com/th-th/?p=sport'),
(12, 1, 'http://sports.sbo666.com/th-th/?p=sport'),
(13, 1, 'http://sports.onlinesbobet.com/th-th/?p=sport'),
(14, 1, 'http://sports.pureshield.com/th-th/?p=sport'),
(15, 1, 'http://sports.ssobet.com/th-th/?p=sport'),
(16, 1, 'http://sports.sbobetasia.com/th-th/?p=sport'),
(17, 1, 'http://sports.sbobet.com/th-th/?p=sport'),
(18, 1, 'http://sports.sbobet2.com/th-th/?p=sport'),
(19, 1, 'http://sports.sbobet.com/th-th/?p=sport'),
(20, 1, 'http://sports.sbobetonline.com/th-th/?p=sport'),
(21, 1, 'http://sports.easymario.com/th-th/?p=sport'),
(22, 1, ''),
(23, 1, ''),
(24, 1, ''),
(25, 1, ''),
(26, 1, ''),
(27, 1, ''),
(28, 1, 'http://wap.sbobet.com'),
(29, 1, 'http://aw.sbobet.com'),
(33, 2, 'http://www.622222.com'),
(32, 2, 'http://www.1100011.com'),
(34, 2, 'http://www.302203.com'),
(35, 2, 'http://www.205502.com'),
(36, 2, 'http://wap.betwing.com'),
(37, 2, 'http://ag.ibc88.com'),
(38, 2, 'http://wap.ibc88.com'),
(39, 2, ''),
(40, 2, ''),
(41, 3, ''),
(42, 3, ''),
(43, 3, ''),
(44, 3, ''),
(45, 3, ''),
(46, 3, ''),
(47, 3, ''),
(48, 3, ''),
(49, 3, ''),
(50, 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `website_setting`
--

CREATE TABLE IF NOT EXISTS `website_setting` (
  `conf_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `conf_value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`conf_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `website_setting`
--

INSERT INTO `website_setting` (`conf_name`, `conf_value`) VALUES
('QUESTIONNAIRE_EMAIL_TO', 'lexdozy@442.im,lexdozy@hotmail.com'),
('QUESTIONNAIRE_EMAIL_CC', 'lexdozy@hotmail.com'),
('EMAIL_SYSTEM', 'lexdozy@442.im');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
