<?php
include_once('inc_chk_no_have_sess.php'); 
require_once('../common/connect.php');  
require_once('admin-class.php');
$obj=new admin_class();  
$obj->close();
//print_r($_SESSION);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>Administration Login</title>
<?php  include_once('inc_script_header.php'); ?>
<script type="text/javascript">
<!--
function returnDeleteOffer(txt) {
	if (txt=='true') {
		window.location.href='home.php';
	}else {
		alert('Can not change password, try again later.');
	}
}
function PasswdSubmit()
{
	var frm = document.form1;
	
	if (frm.Password.value=='') 
	{
		alert("กรุณากรอก รหัสผ่านใหม่ ของคุณด้วยครับ");
		document.getElementById('Password').focus();
		return false;
	}
	else if (frm.Password2.value=='') 
	{
		alert("กรุณากรอก รหัสผ่านใหม่ ของคุณด้วยครับ");
		document.getElementById('Password2').focus();
		return false;
	}
	else if (frm.Password.value!= frm.Password2.value) 
	{
		alert("กรุณากรอก รหัสผ่าน ให้เหมือนกัน ด้วยครับ");
		document.getElementById('Password2').focus();
		return false;
	}
	else
	{
		return true;
	}
}	
-->
</script>
</head>

<body>
<div  id="container">  
<? include_once('inc_navigation.php');?>
<div id="content"> 

 
<div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>   
 </div>  <!--   end  class="breadCrumb" -->
 
<div class="pageHome">
          <h1>ระบบจัดการ Backend ของ 442.im</h1>
            <h3>23 ก.ค. 2012</h3>
            
            
            
           <ol> 
           <li> เพิ่มส่วนจัดการ Webboard 
                <ul>
                <li>เพิ่มมัลติอัพโหลดรูปใน Front end</li>
                <li>ดูรายละเอียดผู้ถามในแต่ละกระทู้ในเวบบอร์ดได้</li>
                <li>ดูรายละเอียดผู้ตอบในแต่ละกระทู้ได้ (โดยคลิ๊กที่ชื่อกระทู้)</li>
                <li>ลบคำถามที่ไม่ต้องการได้</li>
                 <li>ลบคำตอบที่ไม่ต้องการได้</li>
                <li>สามารถตั้งล๊อค และปลดล๊อคกระทู้ได้ (เมื่อปักหมุดกระทู้จะอยู่บนสุด)</li>  
                </ul>
                
            
            </li>
            </ol>
            
         <h3>มิ.ย. 2012</h3>   
            <ol>
              <li> เพิ่มส่วนจัดการสร้าง ผู้ช่วย Admin ได้
                <ul>
                  <li>superadmin ที่มีสิทธ์ใหญ่ที่สุด ในการสร้าง, แก้ไข, ลบ รายชื่อ ผู้ช่วย Admin</li>
                  <li>สามารถสร้างผู้ช่วย Admin ในการจัดการเวบได้ (โดยผู้ช่วย Admin จะไม่สามารถเข้าบางเมนูได้ เช่น Email Setting , Password Setting ฯลฯ  )</li> 
                </ul>
              </li>
              </ol>
<!--              <li>Live Chat
                <ol>
                  <li> เมื่อมี User มีการ Online เข้ามา Admin ท่านใดที่คลิ๊กที่ชื่อ User เป็นคนแรกจะมีสิทธิในการพูดคุยกับ User  คนนั้น<br />
 (Job Status  จะถูกกำหนดเป็น In Use และหลังจากคลิ๊กชื่อ User แล้ว Admin ท่านอื่นจะไม่สามารถคลิ๊กชื่อ User คนนี้เพื่อคุยได้)</li>
 
                  <li> เมื่อคุยหรือรับข้อมูลของ User เสร็จเรียบร้อยแล้ว  Admin  สามารถรายงาน Job Status ของการสนทนาจาก In Use ให้เปลี่ยนให้ Finish ได้ </li>
                  <li> จำกัดจำนวน User ที่เข้ามาในระบบพร้อมกันไม่เกิน 10 คน (เพื่อป้องกันเซอร์เวอร์ทำงานหนัก)</li>
                  <li>Admin ทุกคนที่ login เข้ามาในระบบ หากไม่ได้ใช้งานแล้วต้อง logout จากระบบด้วย  <br />
                    (หากมี Admin  อย่างน้อยหนึ่งคนไม่ Logout จะมีผลทำให้ user เข้าใจผิดว่าขณะนั้นมี Admin ออนไลน์อยู่ )</li>
                <li> ระบบ Live Chat  ทำงานช้าเนื่องจาก การตั้งค่าในการ query ข้อมูลไปที่เซอร์เวอร์ไม่ให้ถี่ไป เพื่อป้องกันระบบล่ม</li>
                </ol>
              </li>-->
              
            </ul>
            </div><!--   end  class="pageHome"  -->
            
</div><!--  end  id="content" --->
 
<div class="clear"></div>
<? include_once('inc_footer.php');?>
 <?php //print_r($_SESSION); ?>
</div><!--  end  id="container" -->


 <iframe src="" id="savetarget" name="savetarget" style=" display:none; width:800px;"></iframe>

</body>
</html>
