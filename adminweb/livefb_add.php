<?php
require_once('inc_chk_no_have_sess.php');
require_once('../common/global.php');
require_once( '../common/connect.php' ); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Contact Us</title>
<?php  include_once('inc_script_header.php'); ?>
 
<script>
$(function(){
	  
 
	function chkAddFbOnline(){
		var LCat = $("#LiveMatchCat").val() ; 
		var LDate = $("#date").val() ;
		var LMonth = $("#month").val() ;
		var LYear = $("#year").val() ;
		
		if( LCat == ""  ){
			alert("กรอกข้อมูลที่บังคับให้ครบด้วยครับ") ; 
			return false ; 
		}else { 
			return false;
		}
		
	}//end function
	
})// end ready 
</script>
</head>

<body>
<div  id="container">
  <? include_once('inc_navigation.php');?>
  <div id="content">
    <div class="breadCrumb"> <a href="<?php  echo  $base_url ;?>/adminweb/">Home</a> > <a href="<?php  breadCrumb() ;?>">ดูบอล Online Add </a></div>
    <!--   end  class="breadCrumb" -->
    
    <form   method="post"   action="livefb_add-script.php"    onsubmit="return chkAddFbOnline();" > 
   
<table width="100%"   class="liveFb"  >
      <tr>
        <td width="52"><label>ลีค :</label></td>
        <td width="916">
     <select name="LiveMatchCat"  id="LiveMatchCat" >
          <option value="">----โปรดระบุ----</option>
          <option value="1">Premier League</option>
          <option value="2">La Liga League</option>
          <option value="3">Thailand Premier League</option>
          <option value="4">Bundesliga League</option>
        </select>
        
        <span class="star">*</span>
        </td>
       
      </tr>
      <tr>
        <td><label>วันที่ :</label></td>
        <td>
 
     
        <select name="date"  id="date" >  
        <option   selected="selected"  value=""  >วัน</option> 
    <?php
	 for($i=1; $i<=31 ; $i++){
		  $v =  ($i<10 ?"0".$i : $i) ;
     echo '<option value="'. $v.'">'.$v.'</option>' ;
	 }
     ?>
        </select> 
        
        
         <select name="month"   >
        <option   selected="selected"  value=""  >เดือน</option> 
             <?php
	 for($i=1; $i<13 ; $i++){
		   $v =  ($i<10 ?"0".$i : $i) ;
	 $arrayM = array('' , 'มกราคม' ,'กุมภาพันธ์' , 'มีนาคม' , 'เมษายน'   ,  'พฤษภาคม' , 'มิถุนายน' , 'กรกฎาคม' ,   'สิงหาคม' ,  'กันยายน' ,  'ตุลาคม' , 'พฤศจิกายน' ,  'ธันวาคม'  );
     echo '<option value="'.$v.'">'.$arrayM[$i].'</option>' ;
	 }
     ?>
     </select>
     
        
 
        
     <select name="year"     >
        <option   selected="selected"  value=""    id="year">ปี</option>
      <?php
	 for($i=0; $i<5; $i++){
		 $y = 2012+$i;
		 $z = (2012+543)+$i ;
 
     echo '<option value="'.$y.'">'.$z.'</option>' ;
	 } 
     ?>
    </select>
    
    
    <span class="star">*</span>
    
        </td>
        
      </tr>
      <tr>
        <td colspan="2">     
       <input  type="button"   id="btnAddMatch"   class="button"  value="เพิ่มแมทซ์แข่งขัน" />  </td>
        </tr>
        
        
      <tr>
        <td colspan="2">
        
        
<script>  
$(function(){  
	var  i = 1;   
	var  j = 0; 
	//---debug------------// 
	//alert("var i val = "+i );  
	//alert ( "#matchForm.length = "+$(" body #matchForm").length );
	 
			 
	$("#btnAddMatch").click( function(e){  
			e.preventDefault(); //  prevent  bahavior  of element  a
			
			//---debug------------// 
			//alert("var j val = "+j );    
			
			var row = $("#matchForm");   
			row.clone().appendTo($("#cloneArea"));   //clone add link  input  
			
			
			var  elm1 =  $('#cloneArea  #matchForm:eq('+j+')');
			var  elm2 =  $('#cloneArea #matchNumber:eq('+j+')') ; 
			//elm1.css( {"border" : "2px solid red" })  ; 
			elm2.html( "<strong>Match ที่ "+(j+2)+"</strong>" );
			
			//---debug------------// 
			//alert(elm1);  
			//console.log(elm1 ) ;  

			
			i++ ;  j++ ; // increase  number  
			 
			
			return false ;	 
			
	}) //end click   

});  // end ready
</script>
  
 
     
     
     
      
        <table width="100%"   id="matchForm"   >
          <tr>
            <td colspan="4" ><h4 id="matchNumber" ><strong>Match ที่ 1</strong> </h4></td>
            </tr>
          <tr>
            <td width="9%" ><label>รายการแข่งขัน :</label></td>
            <td width="35%" ><input type="text" name="LiveMatchName[]"  value=""  style="width:250px;"/></td>
            <td width="9%" ><label>เวลา :</label></td>
            <td width="47%" ><input type="text" name="LiveMatchTime[]"   value=""  style="width:250px;"/></td>
            </tr>
          <tr>
            <td></td>
            <td>  <span  class="red" >ลิงค์ดูผ่านเวบ </span></td>
            <td></td>
            <td><span class="blue">ลิงค์ดูผ่านโปรแกรม </span></td>
            </tr>
          <tr>
            <td> <div align="right">Link 1 : </div></td>
            <td><input type="text" name="LinkWeb[]" value=""  style="width:250px;"/></td>
            <td><div align="right">Link 1 : </div></td>
            <td><input type="text" name="LinkApp[]2" value=""  style="width:250px;"/></td>
          </tr>
          <tr>
            <td><div align="right">Link 2 : </div></td>
            <td><input type="text" name="LinkWeb[]" value=""  style="width:250px;"/></td>
            <td><div align="right">Link 2 : </div></td>
            <td><input type="text" name="LinkApp[]" value=""  style="width:250px;"/></td>
            </tr>
          <tr>
            <td><div align="right">Link 3 : </div></td>
            <td><input type="text" name="LinkWeb[]" value=""  style="width:250px;"/></td>
            <td><div align="right">Link 3 : </div></td>
            <td><input type="text" name="LinkApp[]" value=""  style="width:250px;"/></td>
            </tr>
          <tr>
            <td><div align="right">Link 4 : </div></td>
            <td><input type="text" name="LinkWeb[]2" value=""  style="width:250px;"/></td>
            <td><div align="right">Link 4 : </div></td>
            <td><input type="text" name="LinkApp[]" value=""  style="width:250px;"/></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
      
        </table>
       
    <div id="cloneArea">  </div>  
   
       
   </td>
      </tr>
      <tr>
        <td colspan="2"><div style="margin-left:400px;"><input  type="submit" value="ตกลง" class="button" />&nbsp;<input  type="reset"  value="รีเซ็ต"  class="button"   /></div>     </td>
      </tr>
    </table>
</form>
     
    <br />
    <p style="padding-left:40px;" class="txtBlack11"><strong>Example Link :</strong> http://www.sample.com </p>
  </div>
  <!--  end  id="content" --->
  
  <div class="clear"></div>
  <? include_once('inc_footer.php');?>
</div>
<!--  end  id="container" -->

<iframe src="" id="savetarget" name="savetarget" style=" display:none;"></iframe>
</body>
</html>
