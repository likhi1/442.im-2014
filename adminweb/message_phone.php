<?php
include_once('inc_chk_no_have_sess.php');
require_once('admin-class.php');
require_once( '../common/connect.php' ); 
require_once('../common/global.php');

$Path='press-images';
$PathFile='../press-pdf';
$ConList='';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> '; 
$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> '; 

$obj = new admin_class();
$objGlobal = new GlobalClass();

////Id  Title  Description  Thumbnail  FilePic  LastUpdate  IPAddress  
$Sql="SELECT * FROM ".$obj->tblPhone." ORDER BY PhoneID DESC";
$obj->query($Sql); 
$NumAll=$obj->num_rows();

//Page --------
	//$obj->varURL='sAge='.$_GET['sAge'].'&sNationality='.$_GET['sNationality'].'&keyword='.$_GET['keyword'].'&SubmitFrom='.$_GET['SubmitFrom']; //return variable
	$obj->ListPerPage=25;   //record per page
	$obj->pageSQL=$Sql;	
	$obj->pu_query(); //Call split page		
	$pageDisplay=$obj->showPage();
//--end page

$queue = 1; 
$Num=$obj->num_rows(); 

if ($Num>0) {
	while ($obj->moveNext()) {
	
		$PhoneID=$obj->getField('PhoneID');
		$PhoneNumber=$obj->getField('PhoneNumber');
		$sDate=$obj->getField('sDate');
		$PhoneIP=$obj->getField('PhoneIP');
		
		$Delete='<a href="message_phone_script.php?PhoneID='.$PhoneID.'" onclick="return deleteOffer();" class="text1" target="savetarget">'.$icoDelete.'</a>';
		 		
		//$MyName = "$prefix $FName $LName"	;
		$ConList.='
			<tr height="20" align="center" bgcolor="#F5F5F5" class="txtBlack11">
		      <td id="line-bottom1">'.$PhoneID.'</td>	
		      <td id="line-bottom1">'.$obj->shortThaiDateTime($sDate).'</td>	
              <td id="line-bottom1" align="center" style="padding-left:5px;">'.$PhoneNumber.'</td>
			    <td id="line-bottom1">'.$PhoneIP.'</td>
              <td id="line-bottom1">'.$Delete.'</td>
            </tr>
		';
		
		$queue++;
		
	}//end while
} else {

}//end if

$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Contact Us</title>
<?php  include_once('inc_script_header.php'); ?>
<script language="javascript">

/*------------  Confirm befor delete --------------------------------*/
function deleteOffer(){
if (confirm("Do you want to Delect?")) {
		return true;
	}else {
		return false;
	}
}

/*------------ After run script php --------------------------------*/
function returnDeleteOffer(txt) {
	if (txt=='true') {
		window.location.href='message_phone.php';
	}else {
		alert('Can not delete contact, try again later.');
	}
}

 
 
</script>
</head>

<body>
<div  id="container">  
<?php  include_once('inc_navigation.php');?>
<div id="content"> 

<div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>  >   
<a href="<?php  breadCrumb() ;?>">Message Phone </a> 
</div>  <!--   end  class="breadCrumb" --> 

 <form action="message_phone_script.php" target="savetarget">
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
        <td width="91%" align="left" valign="top" class="txtBlack11"><?php echo $pageDisplay;?></td>
        <td width="9%" align="right" valign="top" class="txtBlack11">All <?php  echo $NumAll;?> results</td>
        </tr>
        <tr>
        <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="1" align="center"  > 
        
        <tr>
        <td width="6%" height="20" align="center" class="headTable"><strong>No.</strong></td>
        <td width="17%" height="20" align="center" class="headTable"><strong>Date -Time</strong></td>
        <td width="37%" align="center" class="headTable"><strong>Phone Number</strong></td>
        <td width="31%" align="center" class="headTable"><strong>IP Address</strong></td>
        <td width="9%" align="center" class="headTable"><strong>Delete</strong></td>
        </tr>
        
        <?php echo $ConList;?>
      
        </table>     
        
   </form>       
            <!-- <input name="btnUpdateOrder" type="submit" class="btnMenu" id="btnUpdateOrder" value="Update Order" />--></td>
          </tr>
      </table>
      
      
</div><!--  end  id="content" --->
 
<div class="clear"></div>
<? include_once('inc_footer.php');?>
</div><!--  end  id="container" -->
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
