<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$obj=new admin_class();


//Add new Event
if ($_POST['btnSave']) {
	
	$StartDate=date('Y-m-d', strtotime($_POST['StartDate']));
	$EndDate=date('Y-m-d', strtotime($_POST['EndDate']));
	$Description=$_POST['Description'];
	$EventLocation=addslashes($_POST['EventLocation']);
	
	if (strip_tags($Description)=='') {
		$return='description';

	} else {
				
			//EventID, StartDate, EndDate, Description, EventLocation
			$Sql="INSERT INTO ".$obj->tblEvent." (EventID, StartDate, EndDate, Description, EventLocation) ".
					" VALUES (NULL,'$StartDate', '$EndDate', '$Description', '$EventLocation') ";
			$result=$obj->query($Sql);
			if ($result) {
				$return='true';
			} else {
				$return='false';
			}																						
	} //end description check
		
	$returnScript='<script>window.parent.returnAddEvent("'.$return.'");</script>';
}//end btnSave 

//-- Update Event ----------------------------
if ($_POST['btnUpdate']) {
	$EventID=$_POST['EventID'];
	$StartDate=date('Y-m-d', strtotime($_POST['StartDate']));
	$EndDate=date('Y-m-d', strtotime($_POST['EndDate']));
	$Description=$_POST['Description'];
	$EventLocation=$_POST['EventLocation'];
	

	$Sql=" UPDATE ".$obj->tblEvent.
			" SET StartDate='$StartDate', EndDate='$EndDate', Description='$Description', EventLocation='$EventLocation' ".
			" WHERE EventID='$EventID' LIMIT 1; ";		
	$result=$obj->query($Sql);
	
	if ($result) {
		$return='true';		
	} else {
		$return='false';
	}
	
	$returnScript='<script>window.parent.returnEditEvent("'.$return.'");</script>';
}//end btnUpdate


//------Delete Event --------------
if ($_GET['act']=='delete-event' && $_GET['id']!='') {

	$result=$obj->query('DELETE FROM '.$obj->tblEvent.' WHERE EventID="'.$_GET['id'].'" LIMIT 1; ');
	
	if ($result) {	
		$return ='true';
	} else {
		$return='false';
	}

	$returnScript='<script>window.parent.returnDeleteEvent("'.$return.'");</script>';	
}//End Delte offer

$obj->close();

echo $returnScript;
?>