<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$SubscribeList='';
$Condition='';
$Query='';
$Filter='';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();


//For CMS ---------------------------------------------

$sBasePath=$obj->BasePath;
$_SESSION['UserFilesPath']=$obj->FilesPath.'/'.$obj->NewsletterFilePath.'/';
$_SESSION['UserFilesAbsolutePath']=$obj->FilesAbsolutePath.'/'.$obj->NewsletterFilePath.'/';

include_once("../fckeditor/fckeditor.php") ;
//-- end for CMS ----------------------------------------
//Get Newsletter -------------------------------------------------------------------------------------------
if ($_GET['nid'] && $_GET['nid']!='') {
	$Sql="SELECT * FROM ".$obj->tblNewsletter." WHERE NewsID='".$_GET['nid']."' LIMIT 1; ";
	$obj->query($Sql);
	
	if ($obj->num_rows()>0) {
		$obj->moveNext();
		$NewsID=$obj->getField('NewsID');
		$NewsSubject=stripslashes($obj->getField('NewsSubject'));
		$NewsDetail=stripslashes($obj->getField('NewsDetail'));
		
	} //end if
}//end get newsletter

//Get from History -------------
if ($_GET['hid'] && $_GET['hid']!='') {
	//HistoryID, Subject, Message, SendTo, SendQuery, SentDate
	$Sql="SELECT * FROM ".$obj->tblHistory." WHERE HistoryID='".$_GET['hid']."' LIMIT 1; ";
	$obj->query($Sql);
	
	if ($obj->num_rows()>0) {
		$obj->moveNext();
		$NewsID=$obj->getField('HistoryID');
		$NewsSubject=stripslashes($obj->getField('Subject'));
		$NewsDetail=stripslashes($obj->getField('Message'));
		$BCC=trim($obj->getField('SendTo'));
		$SenderEmail=trim($obj->getField('SenderEmail'));
		$SenderName=trim($obj->getField('SenderName'));
	} //end if
}


//--Search for send ------------------------------------
if ($_GET['btnOK'] && $_GET['btnOK']!='') {
	if ($_GET['sAge']) {
		$Condition.=" AND sAge='".$_GET['sAge']."' ";
		$Filter.=' &bull; Age: '.$_GET['sAge'].' ';
		$Query.=$Filter.'<br />';
	} elseif ($_GET['sAge']=='0') {
		$Filter.=' &bull; all age ';
		$Query.=$Filter.'<br />';
	}
	
	if ($_GET['sNationality']) {
		$Condition.=" AND sNationality='".$_GET['sNationality']."' ";
		$Filter.=' &bull; Nationality: '.$_GET['sNationality'].' ';
		$Query.=$Filter.'<br />';
	} elseif ($_GET['sNationality']=='0') {
			$Filter.=' &bull; All nationality ';
			$Query.=$Filter.'<br />';
	}
/*	
	if ($_GET['SubmitFrom']) {
		$Condition.=" AND SubmitFrom='".$_GET['SubmitFrom']."' ";
		$Filter.=' &bull; Submit From: '.$_GET['SubmitFrom'].' ';
		$Query.=$Filter.'<br />';
	} elseif ($_GET['SubmitFrom']=='0') {
			$Filter.=' &bull; All submit from ';
			$Query.=$Filter.'<br />';
	}		
*/	
	if ($_GET['keyword']) {
		$key=$_GET['keyword'];
		$Condition.=" AND (sName LIKE '%$key%' OR sAge LIKE '%$key%'  OR sEmail LIKE '%$key%'  OR sNationality LIKE '%$key%' ) ";
		$Filter.=' &bull; Keyword: '.$key;
		$Query.=$Filter.'<br />';
	}
	
	///SubscribeID, sName, sEmail, sAge, sNationality, sDate, sStatus
	$Sql="SELECT SubscribeID, sName, sEmail  FROM ".$obj->tblSubscribe." WHERE SubscribeID<>'' ".$Condition." ORDER BY sName ";
	$obj->query($Sql);
	$NumAll=$obj->num_rows();
	
	
	if ($NumAll>0) {
		$i=1;
		while ($obj->moveNext()) {
			$SubscribeID=$obj->getField('SubscribeID');
			$sName=stripslashes($obj->getField('sName'));
			$sEmail=$obj->getField('sEmail');				
			
			if ($obj->check_email_address($sEmail))  {
					$BCC.=$sEmail;
					if ($i>=$NumAll) { $BCC.=''; } else { $BCC.=', '; }
			} else {
				$BCC.='';
			}
			
			$i++;
		}//end while
		
	} else {
		$BCC='';
	}//end if
}//end btnOK


$AgeList=$obj->getAgeList($_GET['sAge'], '1');
$NationalityList=$obj->getNationalityListTrue($_GET['sNationality']);



$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter (Subscribe)</title>
<script language="javascript" src="../js/fnc_backend.js" type="text/javascript"></script>
<script language="javascript">
function onSend() {
var frm=document.frmNewsletter;
if (frm.SenderEmail.value=='') {
	alert('Please fill sender e-mail address.');
	frm.SenderEmail.focus();
} else if (!checkemail(frm.SenderEmail.value)) {
	alert('Invalide e-mail address of sender.');
	frm.SenderEmail.focus();	
} else if (frm.SenderName.value=='') {
	alert('Please fill sender name.');
	frm.SenderName.focus();
} else if (frm.Bcc.value=='') {
	alert('Recipient is required ! (Use to filter tools.)');
} else if (frm.Subject.value=='') {
	alert('Subject is required !');	
} else {
return true;
}
return false;
}

function returnSend(txt,error) {
	if (txt=='true') {
		alert('Successfully sent newsletter message.');
		window.location.href='newsletter-history.php';
	} else {
		alert('Error ! Cannot send message.\n'+error);
	}
}
</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt; <a href="newsletter.php">Newsletter</a> &gt; Send newsletter </td>
      </tr>
      
    </table>
     
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          
          <!--<tr>
            <td height="25" align="center" class="txtBlack11">&nbsp;</td>
            <td align="right" class="txtBlack11"><form id="form1" name="form1" method="get" action="<?=$PHP_SELF;?>">
              <table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#e1e1e1" style="border:1px solid #bbbbbb;">
                <tr>
                  <td align="left" bgcolor="#cccccc" class="txtBlack11"><strong>:: Recipient Filter </strong></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f1f1f1" class="txtBlack11"><div style="padding:10px;line-height:20px;">
                      <table width="100%" border="0" cellpadding="2" cellspacing="0">
                        <tr>
                          <td align="left" class="txtBlack11">Age:</td>
                          <td align="left" class="txtBlack11">Nationality:</td>
                          <td align="left" class="txtBlack11">Keyword:</td>
                          <td align="left" class="txtBlack11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" class="txtBlack11"><?=$AgeList;?></td>
                          <td align="left" class="txtBlack11"><?=$NationalityList;?></td>
                          <td align="left" class="txtBlack11"><input name="keyword" type="text" class="txtBlack11" id="keyword"  value="<?=$_GET['keyword'];?>" size="25" /></td>
                          <td align="left" class="txtBlack11"><input type="hidden" name="nid" id="nid" value="<?=$_GET['nid'];?>" />
                              <input name="btnOK" type="submit" class="btn1" id="btnOK" value="OK" /></td>
                        </tr>
                      </table>
                  </div></td>
                </tr>
              </table>
            </form></td>
          </tr>-->
          <tr>
            <td colspan="2" align="left" valign="top"><form action="newsletter-scripts.php" method="post" name="frmNewsletter" target="savetarget" id="frmNewsletter" onsubmit="return onSend();">
              <table width="100%" border="0" cellpadding="2" cellspacing="0">
                

                <tr>
                  <td height="22" valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="txtBlack11"><? 
if ($Filter!='') {	
	if ($NumAll>=2) { $s='s'; } else { $s=''; }
	echo '<div class="noteHelp" style="width:99%;text-align:left;"><strong>'.number_format($NumAll).' Result'.$s.'</strong> for '.strtolower($Filter).'</div>';
}
?></td>
                </tr>
                <tr>
                  <td width="12%" valign="top" class="txtBlack11"><strong>Recipient</strong> * <br /></td>
                  <td width="1%" valign="top" class="txtBlack11">:</td>
                  <td width="87%" valign="top" class="txtBlack11"><textarea name="Bcc" cols="80" rows="4" class="txtBlack11" id="Bcc" style="width:99%;"><?=$BCC;?></textarea>                  </td>
                </tr>
                <tr>
                  <td height="30" valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="noteGray10">* Separate email by a comma ( , )</td>
                </tr>
                <tr>
                  <td class="txtBlack11"><strong>Sender Email* </strong></td>
                  <td class="txtBlack11">:</td>
                  <td class="txtBlack11"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td><input name="SenderEmail" type="text" class="txtBlack12" id="SenderEmail"  value="<?=$SenderEmail;?>" size="50" maxlength="50" /></td>
                        <td align="right">&nbsp;<strong>Sender Name: * </strong>
                          <input name="SenderName" type="text" class="txtBlack12" id="SenderName"  value="<?=$SenderName;?>" size="35" maxlength="50" /></td>
                      </tr>
                    </table></td>
                </tr>				
                <tr>
                  <td height="22" class="txtBlack11"><strong>Subject * </strong></td>
                  <td class="txtBlack11">:</td>
                  <td height="22" class="txtBlack11"><input name="Subject" type="text" class="txtBlack12" id="Subject" style="width:99%;" value="<?=$NewsSubject;?>" size="80" maxlength="255" />                  </td>
                </tr>
                <tr>
                  <td valign="top" class="txtBlack11"><strong>Message</strong></td>
                  <td valign="top" class="txtBlack11">:</td>
                  <td valign="top" class="txtBlack11">
<?php
$oFCKeditor = new FCKeditor('Message') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;

$oFCKeditor->ToolbarSet = 'Standard2';
$oFCKeditor->Height=500;
$oFCKeditor->Width='100%';

$oFCKeditor->Value = $NewsDetail;
$oFCKeditor->Config['EnterMode'] = 'br';



$oFCKeditor->Create() ;

?></td>
                </tr>
                <tr>
                  <td height="37" valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="txtBlack11">&nbsp;</td>
                  <td height="37" valign="bottom" class="txtBlack11"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="13%"><input type="hidden" id="Query" name="Query" value="<?=$Query;?>" />
                        <input name="btnSendEmail" type="submit" class="btn1" id="btnSendEmail" value="  Send  " /></td>
                      <td width="87%" align="right"><input name="btnNews2" type="button" class="btnMenu" id="btnNews2" value="Select Template" onclick="window.location.href='newsletter-list.php';" />
                        <input name="btnNews" type="button" class="btnMenu" id="btnNews" value="History" onclick="window.location.href='newsletter-history.php';" />
                        <input name="Button2" type="button" class="btnMenu" value="« Back" onclick="history.back();" /></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="top" class="txtBlack11">&nbsp;</td>
                  <td valign="bottom" class="txtBlack11">&nbsp;</td>
                </tr>
              </table>
                        </form>            </td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
