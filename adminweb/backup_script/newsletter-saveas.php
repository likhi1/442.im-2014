<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');


$obj=new admin_class();

//For CMS ---------------------------------------------

$sBasePath=$obj->BasePath;
$_SESSION['UserFilesPath']=$obj->FilesPath.'/'.$obj->NewsletterFilePath.'/';
$_SESSION['UserFilesAbsolutePath']=$obj->FilesAbsolutePath.'/'.$obj->NewsletterFilePath.'/';

include_once("../fckeditor/fckeditor.php") ;
//-- end for CMS ------------------------------------


$Sql="SELECT * FROM ".$obj->tblNewsletter." WHERE NewsID='".$_GET['id']."' LIMIT 1; ";
$obj->query($Sql);

if ($obj->num_rows()>0) {
	$obj->moveNext();
	$NewsID=$obj->getField('NewsID');
	$NewsSubject=stripslashes($obj->getField('NewsSubject'));
	$NewsDetail=stripslashes($obj->getField('NewsDetail'));
	
} //end if

$obj->close();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter</title>
<script language="javascript" src="../js/fnc_backend.js" type="text/javascript"></script>
<script language="javascript">
function onSubmit() {
var frm=document.frmNewsletter;
	if (frm.Subject.value=='') {
		alert('Template name is required.');
		frm.Subject.focus();
	} else {
		return true;
	}
	return false;
}

function returnNewsletter(txt) {
	if (txt=='true') {
			window.location.href='newsletter-list.php';
	} else {
		alert('Error!\nCan not processing. try again later.');
	}
}

window.onload=function() {
	if (document.getElementById('Subject').value=='') {
		alert('Please fill name of template.');
		document.getElementById('Subject').focus(); 
	}
}
</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  <a href="newsletter.php">Newsletter</a> &gt; <a href="newsletter-list.php">List of templates</a> &gt; Save as template </td>
      </tr>
      
    </table>
     
      <form action="newsletter-scripts.php" method="post" name="frmNewsletter" target="savetarget" id="frmNewsletter" onsubmit="return onSubmit();">
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td><?=$obj->SaveResult('Successfully saved template.');?></td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="16%" class="txtBlack11">Template Name: * </td>
                <td width="84%" class="txtBlack11">                  <input name="Subject" type="text" class="txtBlack11" id="Subject" size="60" maxlength="255" />                </td>
              </tr>
              
              <tr>
                <td valign="top" class="txtBlack11">Message:</td>
                <td valign="top" class="txtBlack11">
<?php
$oFCKeditor = new FCKeditor('Message') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;

$oFCKeditor->ToolbarSet = 'Standard2';
$oFCKeditor->Height=550;
$oFCKeditor->Width='100%';

$oFCKeditor->Value = $NewsDetail;
$oFCKeditor->Config['EnterMode'] = 'br';



$oFCKeditor->Create() ;

?></td>
              </tr>
              <tr>
                <td class="txtBlack11">&nbsp;</td>
                <td class="txtBlack11"><input type="hidden" name="NewsID" id="NewsID" value="<?=$NewsID;?>" /></td>
              </tr>
              <tr>
                <td class="txtBlack11">&nbsp;</td>
                <td class="txtBlack11"><input name="btnSaveNewsletter" type="submit" class="btnMenu" id="btnSaveNewsletter" value="Save" />
                  <input name="Button2" type="button" class="btnMenu" value="« Back" onclick="window.location.href='newsletter-list.php';" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" width="0" height="0" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
