<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');


$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();

$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter</title>

</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  Newsletter </td>
      </tr>
      
    </table>
     
        <table width="90%" border="0" align="center" cellpadding="2" cellspacing="5">
          <tr>
            <td width="33%" align="center">&nbsp;</td>
            <td width="33%" align="center">&nbsp;</td>
            <td width="34%" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="btnNews22" type="button" class="btnMenu" id="btnNews22" value="Send Newsletter" onclick="window.location.href='newsletter-send.php';" /></td>
            <td align="center"><input name="btnNews2" type="button" class="btnMenu" id="btnNews2" value="Template" onclick="window.location.href='newsletter-list.php';" />            </td>
            <td align="center"><input name="btnNews" type="button" class="btnMenu" id="btnNews" value="History" onclick="window.location.href='newsletter-history.php';" />            </td>
          </tr>
          
          <tr>
            <td height="52" align="center" class="noteHelp">Select to send newsletter and then <br />
            choose from pre-designed   templates.</td>
            <td align="center" class="noteHelp">Create new template, <br />
              Edit existing template 
            or 
            View old   template.</td>
            <td align="center" class="noteHelp">View past templates in the system</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
</body>
</html>
