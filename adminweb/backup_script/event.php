<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$EventList='';
$Condition='';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();

if ($_GET['EventMonth'] && $_GET['EventYear']) {
	$viewMonth=$_GET['EventMonth'];
	$viewYear=$_GET['EventYear'];	
} else {
	$viewMonth=date('m');
	$viewYear=date('Y');
}

$Condition.=" AND 
					(							
						MONTH(StartDate)='".$viewMonth."'  OR 	MONTH(EndDate)='".$viewMonth."'
					)  AND (
						YEAR(StartDate)='".$viewYear."'  OR YEAR(EndDate)='".$viewYear."'
					) ";


////EventID, StartDate, EndDate, Description, EventLocation
$Sql="SELECT *  FROM ".$obj->tblEvent." WHERE EventID<>'' ".$Condition." ORDER BY StartDate ";
$obj->query($Sql);
$NumAll=$obj->num_rows();

if ($NumAll>0) {
	while ($obj->moveNext()) {
		$EventID=$obj->getField('EventID');
		$StartDate=$obj->getField('StartDate');
		$EndDate=$obj->getField('EndDate');
		$Description=stripslashes($obj->getField('Description'));
		$EventLocation=$obj->getField('EventLocation');
		
		if ($EventLocation!='') {
			$EventLocation=stripslashes($EventLocation);
		} else {
			$EventLocation='-';
		}

		$Edit='<a href="event-edit.php?id='.$EventID.'">'.$icoEdit.'</a>';
		$Delete='<a href="event-script.php?id='.$EventID.'&act=delete-event" onclick="return deleteEvent();" class="text1" target="savetarget">'.$icoDelete.'</a>';
				
		$EventList.='
			<tr  align="center" class="txtBlack11" '.$obj->lineOver().'>
		       <td id="line-bottom1">'.date('M d, y',strtotime($StartDate)).'</td>	
              <td id="line-bottom1">'.date('M d, y',strtotime($EndDate)).'</td>
              <td id="line-bottom1" align="left">'.$Description.'</td>
              <td id="line-bottom1">'.$EventLocation.'</td>
              <td id="line-bottom1">'.$Edit.'</td>
              <td id="line-bottom1">'.$Delete.'</td>
            </tr>
		';
	}//end while
} else {
	$EventList.='<tr><td align="left" height="50" colspan="6" class="txtBlack11">0 Result</td></tr>';
}//end if

$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Event Calendar</title>
<script language="javascript" src="../../scripts/CalendarPopup.js" type="text/javascript"></script>
<script language="javascript">
function deleteEvent() {
	var chk=confirm('Are you sure you want to delete this event.');
	if (chk==true) {
		return true;
	} else {
		return false;
	}
}

function returnDeleteEvent(txt) {
	if (txt=='true') {
		window.location.href='event.php';
	}else {
		alert('Can not delete event, try again later.');
	}
}

</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  Event Calendar</td>
      </tr>
      
    </table>
     
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td width="721" align="left" valign="top"><form id="form1" name="form1" method="get" action="">
              <table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td class="txtBlack11"><?=$obj->getMonthEvent($viewMonth);?>
                    <input name="EventYear" type="text" class="txtBlack11" id="EventYear" style="text-align:center;" value="<?=$viewYear;?>" size="5" maxlength="4" />
                  <input name="btnGo" type="submit" class="btn1" id="btnGo" value="View Selected" />
                  <input name="btnGo2" type="button" class="btn1" id="btnGo2" value="View Today" onclick="window.location.href='event.php';" /></td>
                  <td class="txtBlack11">&nbsp;</td>
                </tr>
              </table>
                        </form>
            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                <tr>
                  <td height="16" align="center" class="headTable">Start Date</td>
                  <td align="center" class="headTable">End Date </td>
                  <td align="center" class="headTable">Event </td>
                  <td align="center" class="headTable">Location</td>
                  <td align="center" class="headTable">Edit</td>
                  <td align="center" class="headTable">Delete</td>
                </tr>
                <?=$EventList;?>
            </table>
			</td>
            <td width="141" align="left" valign="top"><input name="Button" type="button" class="btnMenu" value="+Add new event" onclick="window.location.href='event-add.php';" />
              <input name="Button2" type="button" class="btnMenu" value="« Back" onclick="history.back();" /></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
