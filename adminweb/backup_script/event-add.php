<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$obj=new admin_class();

//For CMS ---------------------------------------------
$sBasePath=$obj->BasePath;
$_SESSION['UserFilesPath']=$obj->FilesPath.'/'.$obj->PresidentFilePath.'/';
$_SESSION['UserFilesAbsolutePath']=$obj->FilesAbsolutePath.'/'.$obj->PresidentFilePath.'/';
include_once("../fckeditor/fckeditor.php") ;
//-- end for CMS ------------------------------------
$obj->close();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="../../scripts/CalendarPopup.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
var varInterval='';
var sumTime=0;
var cal = new CalendarPopup("testdiv1");
cal.showYearNavigation();

function onStartDate() {
	cal.select(document.forms[0].StartDate,'anchor1','MMM dd, yyyy'); 
	setIntervalValue();	
}
function onEndDate() {	
	cal.select(document.forms[0].EndDate,'anchor2','MMM dd, yyyy',(document.forms[0].EndDate.value=='')?document.forms[0].StartDate.value:null); 	
}
function setIntervalValue() {
	varInterval=setInterval(countTime,500);
}

function countTime() {
	
	if (sumTime>=2) {
		//cal.addDisabledDates(null, document.forms[0].rsvnArrDate.value);		
		if (document.getElementById('StartDate').value!='' && document.getElementById('EndDate').value=='') { 
			document.getElementById('EndDate').value=document.getElementById('StartDate').value;
			clearInterval(varInterval);
		}			
		sumTime=0;
	} else {
		sumTime++;
	}
}
</SCRIPT>

<script type="text/javascript">
function onSubmit() {
	var frm=document.frmEvent;
	if (frm.StartDate.value=='') {
		alert('Start date is required.');
		frm.StartDate.focus();
	} else if (frm.EndDate.value=='') {
		alert('End date is required.');
		frm.EndDate.focus();	
	} else {
		return true;
	}
	return false;
}

function returnAddEvent(txt) {
	if (txt=='true') {
		window.location.href='event.php';	
	} else if (txt=='description') {
		alert('Description is required.');	
	} else {
		alert('Can not add event, try again');		
	}
}
</script>

<title>CMS: Offers</title>
</head>

<body>
<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt; <a href="event.php"> Event Calendar</a> &gt; Add new event </td>
      </tr>
      
    </table>
      <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td width="721" align="left" valign="top"><form action="event-script.php" method="post" enctype="multipart/form-data" name="frmEvent" target="savetarget" id="frmEvent" onsubmit="return onSubmit();">
            <table width="100%" border="0" cellpadding="3" cellspacing="0">
			  
			  <tr>
                <td width="137" class="txtBlack11">Start Date  <span class="noteRed10">*</span> </td>
                <td width="5" align="left" class="txtBlack11">:</td>
                <td width="561" align="left" class="txtBlack11"><input name="StartDate" type="text" class="txtBlack11" id="StartDate"    readonly>
                  <A HREF="#" onClick="onStartDate(); return false;" TITLE="Calendar" NAME="anchor1" ID="anchor1"><img src="../images/calendar.png" width="16" height="16" border="0" align="absmiddle"></a></td>
              </tr>
			 
            
              <tr>
                <td valign="top" class="txtBlack11">End Date <span class="noteRed10">*</span></td>
                <td align="left" valign="top" class="txtBlack11">:</td>
                <td align="left" valign="top" class="txtBlack11"><input name="EndDate" type="text" class="txtBlack11" id="EndDate" readonly>
                  <A HREF="#" onClick="onEndDate();  return false;" TITLE="Calendar" NAME="anchor2" ID="anchor2"><img src="../images/calendar.png" width="16" height="16" border="0" align="absmiddle"></a> </td>
              </tr>
              <tr>
                <td valign="top" class="txtBlack11">Description <span class="noteRed10">*</span></td>
                <td align="left" valign="top" class="txtBlack11">:</td>
                <td align="left" valign="top" class="txtBlack11">  
<?php

//$sBasePath = $_SERVER['PHP_SELF'] ;
//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "_samples" ) ) ;


$oFCKeditor = new FCKeditor('Description') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;
$oFCKeditor->ToolbarSet = 'Basic';
$oFCKeditor->Height=150;
$oFCKeditor->Width=450;

$oFCKeditor->Value = '' ;
$oFCKeditor->Config['EnterMode'] = 'br';

$oFCKeditor->Create() ;
?></td>
              </tr>
              

              <tr>
                <td class="txtBlack11">Location</td>
                <td align="left" class="txtBlack11">:</td>
                <td align="left" class="txtBlack11"><input name="EventLocation" type="text" class="txtBlack11" id="EventLocation" size="60" maxlength="255" /></td>
              </tr>
              
              <tr>
                <td class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11" id="line-bottom1">&nbsp;</td>
              </tr>
              <tr>
                <td height="42" class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11">                  <input name="btnSave" type="submit" class="btn1" id="btnSave" value=" Save " />
                  <input name="btnReset" type="button" class="btn1" id="btnReset" value="Reset" onclick="window.location.href='event-add.php';" /></td>
              </tr>
              <tr>
                <td class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11">&nbsp;</td>
                <td align="left" class="txtBlack11">&nbsp;</td>
              </tr>
            </table>
           </form>
          </td>
          <td width="141" align="left" valign="top"><input name="Button2" type="button" class="btnMenu" value="&laquo; Back" onclick="history.back();" /></td>
        </tr>
      </table></td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
