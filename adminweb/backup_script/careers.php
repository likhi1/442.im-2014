<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');



$obj=new admin_class();

//For CMS ---------------------------------------------
$sBasePath=$obj->BasePath;
$UserFilesPath=$obj->FilesPath.'/'.$obj->PresidentFilePath.'/';
$UserFilesAbsolutePath=$obj->FilesAbsolutePath.'/'.$obj->PresidentFilePath.'/';
include_once("../fckeditor/fckeditor.php") ;
//-- end for CMS ------------------------------------


$FileName='../files/career.txt';
if (file_exists($FileName)) {
	
	$CareerData=file_get_contents($FileName);
}
$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="../js/fnc_backend.js" type="text/javascript"></script>
<script language="javascript">
function returnUpdateCareer(txt) {
	if (txt=='true') {		
		HideResult();
	}else {
		alert('Can not delete event, try again later.');
	}
}
</script>
<title>CMS: Manage Careers</title>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  Careers</td>
      </tr>
      
    </table>     
      <form action="careers-script.php" method="post" name="frmCareer" target="savetarget" id="frmCareer">
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td height="29" colspan="2"><?=$obj->SaveResult('Successfully saved career.');?></td>
          </tr>
          <tr>
            <td align="left" class="txtBlack11"><strong>Career Description: </strong></td>
            <td align="right" class="txtBlack11"><input name="btnSaveCareer" type="submit" class="btn1" id="btnSaveCareer" value="Update" />
              <input name="btnReset" type="button" class="btn1" id="btnReset" value="Reset" onclick="window.location.href='careers.php';" /></td>
          </tr>
          <tr>
            <td colspan="2"><?php

//$sBasePath = $_SERVER['PHP_SELF'] ;
//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "_samples" ) ) ;



$oFCKeditor = new FCKeditor('CareerDescription') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;
$oFCKeditor->ToolbarSet = 'Basic';
$oFCKeditor->Height=300;
//$oFCKeditor->Width=540;

$oFCKeditor->Value = $CareerData;
$oFCKeditor->Config['EnterMode'] = 'br';

$oFCKeditor->Create() ;

?>            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
