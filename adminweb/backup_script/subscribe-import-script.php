<?php  session_start();  ?>
<?php
require_once('../../class/db_connect.php');
require_once('../admin-class.php');

$obj=new admin_class();

//check email from xls file (excel)
if ($_POST['IMPORTXLS']) {
	
	if (session_is_registered("SS_EMAIL")) { 	 //Clear session email list
		session_unregister("SS_EMAIL"); //clear session email		
		session_unregister("SS_SNAME");
		session_unregister("SS_AGE");
		session_unregister("SS_NATION");
		session_unregister("SS_SUBMIT_FROM");
		
	}
		
	$filename = basename($_FILES['file']['name']);
    $file_extension = strtolower(substr(strrchr($filename,"."),1));
	
	if ($_FILES['file']['size']>0) {
		if ($file_extension!='xls') {
			$return= '<script>alert("Invalid file extension.");</script>';
		} else {
			//Upload file to temp file
			$tmp='tmp/email.xls';
			$upload=move_uploaded_file($HTTP_POST_FILES['file']['tmp_name'],$tmp);
			
			
			//Read excel file			
			require_once("../../class/excel-reader/oleread.inc");
			require_once("../../class/excel-reader/reader.php");
			$data = new Spreadsheet_Excel_Reader(); //เรียกใช้ class
			  
			$data->setOutputEncoding('CP1251');
			$data->read($tmp); 
		
			$email='';
			$firstname='';
			$lastname='';
			
			$ValidEmail='';
			$InvalidEmail='';
			
			$arrSName=array();
			$arrValidEmail=array();
			$arrAge=array();
			$arrSNationality=array();
			$arrSubmitFrom=array();
			$arrInvalidEmail=array();			
		 
			for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++)
			{
			
					$SName=trim($data->sheets[0]['cells'][$i][1]);
					$email=trim($data->sheets[0]['cells'][$i][2]);
					$sAge=trim($data->sheets[0]['cells'][$i][3]);
					$sNationality=trim($data->sheets[0]['cells'][$i][4]);
					$SubmitFrom=trim($data->sheets[0]['cells'][$i][5]);
					
				if ($email!='') {
					if ($obj->ValidEmail($email)) {
						$arrSName[]=$SName;
						$arrValidEmail[]=$email;
						$arrAge[]=$sAge;
						$arrSNationality[]=$sNationality;
						$arrSubmitFrom[]=$SubmitFrom;
						
						
					} else { //invalid email address
						$arrInvalidEmail[]=$email;
					}
				}
			}
			
			//show valid email 
			$n=1;
			$EmailList='';
			foreach ($arrValidEmail as $valueValid) {
				$EmailList.=$valueValid;
				$ValidEmail.=$n.'. '.$valueValid.'<br />';				
				$n++;
			}
			
			//Create session for email import
				session_register("SS_SNAME");
				session_register("SS_EMAIL");
				session_register("SS_AGE");
				session_register("SS_NATION");
				session_register("SS_SUBMIT_FROM");
				
				$_SESSION['SS_SNAME']=$arrSName;
				$_SESSION['SS_EMAIL']=$arrValidEmail;
				$_SESSION['SS_AGE']=$arrAge;
				$_SESSION['SS_NATION']=$arrSNationality;
				$_SESSION['SS_SUBMIT_FROM']=$arrSubmitFrom;
			
			//show invalid email 
			$m=1;
			foreach ($arrInvalidEmail as $valueInvalid) {
				$InvalidEmail.=$m.'. '.$valueInvalid.'<br />';
				$m++;
			}
			
			$emailResult=' window.parent.document.getElementById("title1").innerHTML="E-mail address ('.number_format(count($arrValidEmail)).')"; ';
			$emailResult.=' window.parent.document.getElementById("title2").innerHTML="Invalid E-mail address ('.number_format(count($arrInvalidEmail)).')"; ';
			$emailResult.=' window.parent.document.getElementById("email1").innerHTML="'.$ValidEmail.'"; ';
			$emailResult.=' window.parent.document.getElementById("email2").innerHTML="'.$InvalidEmail.'"; ';
			$emailResult.=' window.parent.document.getElementById("btnImport").style.display="block"; ';			
								
			$return= '<script>'.$emailResult.'</script>';
			
			if (file_exists($tmp)) { unlink($tmp); }
			unset($ValidEmail,$InvalidEmail);
		}
	} 
	else {
		$return= '<script>alert("File not uploaded to the wanted location.");</script>';
	}
	
	echo $return;
	
}

//Import email to database 
if ($_POST['IMPORTEMAILNOW']) {
	$countall=0;
	$countinsert=0;
	$counterror=0;
	$countexist=0;
	$i=1;
	$j=1;
	$k=1;
	$n=0; //for array
	$Date=date('Y-m-d H:i:s');
	$IsStatus='2'; //1=submit from website, 2=import from excel file
	
	$Imported='';
	$Error='';
	$SubscribeFrom='E'; //W=website, E=excel
				
	foreach ($_SESSION['SS_EMAIL'] as $emails) {			//Check exist email address
		if (!$obj->CheckExistValue('sEmail',$obj->tblSubscribe,$emails)) { 				
			
			$sName=addslashes($_SESSION['SS_SNAME'][$n]);
			$Age=addslashes($_SESSION['SS_AGE'][$n]);	
			$Nation=addslashes($_SESSION['SS_NATION'][$n]);	
			$Submit=addslashes($_SESSION['SS_SUBMIT_FROM'][$n]);	
			
	
			
			$SQL="INSERT INTO ".$obj->tblSubscribe.
			 " (SubscribeID, sName, sEmail, sAge, sNationality, SubmitFrom, sDate,sStatus) ".
			 " VALUES ('NULL', '$sName', '$emails', '$Age', '$Nation','$Submit','$Date','1') ";
			$result=$obj->query($SQL);
			
			if ($result) {				
				$Imported.=$i.'. '.$emails.'<br />';
				$countinsert++;
				$i++;
			} else {
				$Error.=$j.'. '.$emails.'<br />';
				$counterror++;
				$j++;
			}			
		} else {
			$Exist.=$k.'. '.$emails.'<br />';
			$countexist++;
			$k++;
		}	
		$n++;		
	}
	
	if ($Exist!='') {
		$Imported.='<div style=\"border-bottom:1px solid #333;padding:10px 0 5px 0;\"><b>The e-mail you uploaded already exists.</b> ('.$countexist.')</div><div class=\"txt1\">'.$Exist.'</div>';
	}
	
	session_unregister("SS_EMAIL"); //clear session email		
	session_unregister("SS_SNAME");
	session_unregister("SS_AGE");
	session_unregister("SS_NATION");
	session_unregister("SS_SUBMIT_FROM");
	
	$emailResult=' window.parent.document.getElementById("title1").innerHTML="'.number_format($countinsert).' Imported"; ';
	$emailResult.=' window.parent.document.getElementById("title2").innerHTML="'.number_format($counterror).' Error"; ';
	$emailResult.=' window.parent.document.getElementById("email1").innerHTML="'.$Imported.'"; ';
	$emailResult.=' window.parent.document.getElementById("email2").innerHTML="'.$Error.'"; ';
	$emailResult.=' window.parent.document.getElementById("btnImport").innerHTML="<input type=\"button\" value=\"  Close  \" class=\"btnMenu\" onclick=\"window.parent.location.href=window.parent.location.href;\" />"; ';	
	$emailResult.=' window.parent.document.getElementById("file").value=""; ';	
	
	$return.='alert("Successfully imported email-address.");';
	echo '<script>'.$return.$emailResult.'</script>';
}
$obj->close();
?>

