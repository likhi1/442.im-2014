<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');
$Path='../offers-images';

$obj=new admin_class();


//For CMS ---------------------------------------------
$sBasePath=$obj->BasePath;
$_SESSION['UserFilesPath']=$obj->FilesPath.'/'.$obj->PresidentFilePath.'/';
$_SESSION['UserFilesAbsolutePath']=$obj->FilesAbsolutePath.'/'.$obj->PresidentFilePath.'/';
include_once("../fckeditor/fckeditor.php") ;
//-- end for CMS ------------------------------------


////offerID, Title, Thumbnail, ShortDefinition, Description, IsBook, BookToLink, IsDisplay, SortOrder
$Sql="SELECT * FROM ".$obj->tblOffer." WHERE offerID='".$_GET['id']."' LIMIT 1;  ";
$obj->query($Sql);
$Num=$obj->num_rows();


if ($Num>0) {
	$obj->moveNext();
	
		$offerID=$obj->getField('offerID');
		$Title=stripslashes($obj->getField('Title'));
		$Thumbnail=$obj->getField('Thumbnail');
		$ShortDefinition=stripslashes($obj->getField('ShortDefinition'));
		$Description=stripslashes($obj->getField('Description'));		
		$IsBook=$obj->getField('IsBook');
		
		$IsTerm=$obj->getField('IsTerm');
		$TermURL=$obj->getField('TermURL');
		$TermTarget=$obj->getField('TermTarget');
		$TermPopWidth=$obj->getField('TermPopWidth');
		$TermPopHeight=$obj->getField('TermPopHeight');
		
		$CurrencyConverter=$obj->getField('CurrencyConverter');
		$IsDisplay=$obj->getField('IsDisplay');
		$BookToLink=$obj->getField('BookToLink');
		$SortOrder=$obj->getField('SortOrder');
			
		if ($Thumbnail!='') {
			$THUMBNAIL='<img src="'.$Path.'/'.$Thumbnail.'" alt="" border="0" width="138" height="130" />';
		} else {
			$THUMBNAIL='';
		}
		
		if ($IsBook=='1') {
			$IsBook0='';
			$IsBook1=' checked="checked" ';			
		} else {
			$IsBook1='';
			$IsBook0=' checked="checked" ';			
		}
		
		if ($IsTerm=='1') {
			$IsTerm0='';
			$IsTerm1=' checked="checked" ';			
		} else {
			$IsTerm1='';
			$IsTerm0=' checked="checked" ';			
		}
		
		if ($TermTarget=='P') {
			$TermTargetN='';
			$TermTargetP=' checked="checked" ';			
		} else {
			$TermTargetP='';
			$TermTargetN=' checked="checked" ';			
		}
		
		if ($IsDisplay=='1') {
			$IsDisplay0='';
			$IsDisplay1=' checked="checked" ';			
		} else {
			$IsDisplay1='';
			$IsDisplay0=' checked="checked" ';			
		}
		
		if ($CurrencyConverter=='1') {
			$CurrencyConverter0='';
			$CurrencyConverter1=' checked="checked" ';			
		} else {
			$CurrencyConverter1='';
			$CurrencyConverter0=' checked="checked" ';			
		}
} //end if Num		
$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="../js/fnc_backend.js" type="text/javascript"></script>
<script type="text/javascript">
function onSubmit() {
	var frm=document.frmOffer;
	if (frm.Title.value=='') {
		alert('Title is required.');
		frm.Title.focus();
	} else {
		return true;
	}
	return false;
}

function returnEditOffer(txt) {
	if (txt=='true') {
			HideResult();	
			window.location.hash="top";
	} else {
		 if (txt=='err_type') {
			alert('Plese upload thumbnail picture file type of GIF or JPG or PNG. \ntry again');	
		} else if (txt=='err_size') {
			alert('Limited size of thumbnail picture to 200kb, try again.');	
		} else if (txt=='empty_file') {
			alert('Plese upload thumbnail picture');		
		} else if (txt=='false') {
			alert('Can not update offer, try again');		
		}
		window.location.href='offers-edit.php?id=<?=$_GET['id'];?>';	
	}
}

</script>

<title>CMS: Offers</title>
</head>

<body onload="TermsConditionButton();">
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt; <a href="offers.php">Manage Offers</a> &gt; Edit  offer </td>
      </tr>
      <tr>
        <td height="40" align="left" valign="bottom" class="head1"><?=$obj->SaveResult('Successfully saved offer.');?></td>
      </tr>
      
    </table>
      <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td width="721" align="center" valign="top"><form action="offers-script.php" method="post" enctype="multipart/form-data" name="frmOffer" target="savetarget" id="frmOffer" onsubmit="return onSubmit();">
            <table width="95%" border="0" cellpadding="3" cellspacing="0">
			  <tr>
			    <td height="5" colspan="3"></td>
			    </tr>
			  

              <tr>
                <td height="30" colspan="3" align="left" valign="bottom" class="txtBlack11"><table width="100%" border="0" cellpadding="2" cellspacing="0">
                  <tr>
                    <td width="19%" align="left" class="txtBlack11"><strong>Title <span class="noteRed10">*</span></strong></td>
                    <td width="1%" align="left" class="txtBlack11">:</td>
                    <td width="57%" align="left" class="txtBlack11"><input name="Title" type="text" class="txtBlack11" id="Title" value="<?=$Title;?>" style="width:95%; padding:5px;" /></td>
                    <td width="23%" rowspan="3" align="center" class="txtBlack11"><?=$THUMBNAIL;?></td>
                  </tr>
                  <tr>
                    <td align="left" class="txtBlack11"><strong>Change thumbnail</strong></td>
                    <td align="left" class="txtBlack11">:</td>
                    <td align="left" class="txtBlack11"><input name="file" type="file" class="txtBlack11" size="35" /></td>
                  </tr>
                  <tr>
                    <td align="left" class="txtBlack11">&nbsp;</td>
                    <td align="left" class="txtBlack11">&nbsp;</td>
                    <td align="left" class="noteHelp">Type of Thumbnail Picture: GIF, JPG, PNG <br />
                      Picture Dimensions: 138 x 130 pixels</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="30" colspan="3" align="left" valign="bottom" class="txtBlack11"><strong>Short Definition </strong></td>
                </tr>
              <tr>
                <td colspan="3" align="left" valign="top" class="txtBlack11"><?php

$oFCKeditor = new FCKeditor('ShortDefinition') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;
$oFCKeditor->ToolbarSet = 'Basic';
$oFCKeditor->Height=150;


$oFCKeditor->Value =$ShortDefinition ;
$oFCKeditor->Config['EnterMode'] = 'br';

$oFCKeditor->Create() ;
?></td>
                </tr>
              <tr>
                <td height="40" colspan="3" align="left" valign="bottom" class="txtBlack11"><strong>Description</strong></td>
                </tr>
              <tr>
                <td colspan="3" align="left" valign="top" class="txtBlack11"><?php

$oFCKeditor = new FCKeditor('Description') ;
$oFCKeditor->BasePath =$sBasePath;

$oFCKeditor->Config['CustomConfigurationsPath']='../config-1.js';

$oFCKeditor->Config['AutoDetectLanguage']	= false ;
$oFCKeditor->Config['DefaultLanguage']		= 'en' ;

$oFCKeditor->ToolbarSet = 'Standard';
$oFCKeditor->Height=300;


$oFCKeditor->Value = $Description;
$oFCKeditor->Config['EnterMode'] = 'br';

$oFCKeditor->Create() ;
?></td>
                </tr>
              <tr>
                <td colspan="3" align="left" class="txtBlack11">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3" align="left" class="txtBlack11"><table width="100%" border="0" cellpadding="2" cellspacing="1">
                  <tr>
                    <td width="50%" valign="top" class="txtBlack11"><fieldset>
                      <legend class="txtBlack11"><strong>Booking</strong></legend>
                      <table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                          <td colspan="2" class="txtBlack11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="27%" height="30" align="left" class="txtBlack11">Show Button</td>
                          <td width="73%" align="left" class="txtBlack11"><input name="IsBook" type="radio" value="1" <?=$IsBook1;?> />
Yes                 &nbsp;&nbsp;&nbsp;
<input name="IsBook" type="radio" value="0" <?=$IsBook0;?> />
No </td>
                        </tr>
                        <tr>
                          <td height="17" colspan="2" align="left" class="txtBlack11">Button Link : </td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center" class="txtBlack11"><input name="BookToLink" type="text" class="txtBlack11" id="BookToLink" value="<?=$BookToLink;?>" size="45" maxlength="255" /></td>
                        </tr>
                        <tr>
                          <td colspan="2" class="txtBlack11">&nbsp;</td>
                        </tr>
                      </table>
                      </fieldset>
                        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
                          <tr>
                            <td height="26" colspan="3" class="txtBlack11">&nbsp;</td>
                          </tr>
                          <tr>
                            <td width="47%" class="txtBlack11"><strong>Currency Converter</strong></td>
                            <td width="3%" class="txtBlack11"><strong>:</strong></td>
                            <td width="50%" class="txtBlack11"><input name="CurrencyConverter" id="CurrencyConverter" type="radio" value="1" <?=$CurrencyConverter1;?> />
Yes                 &nbsp;&nbsp;&nbsp;
<input name="CurrencyConverter" id="CurrencyConverter" type="radio" value="0" <?=$CurrencyConverter0;?> />
No</td>
                          </tr>
                          <tr>
                            <td class="txtBlack11"><strong>Published to front-end </strong></td>
                            <td class="txtBlack11"><strong>:</strong></td>
                            <td class="txtBlack11"><input name="IsDisplay" type="radio" value="1" <?=$IsDisplay1;?> />
Yes  &nbsp;&nbsp;&nbsp;
<input name="IsDisplay" type="radio" value="0" <?=$IsDisplay0;?> />
No </td>
                          </tr>
                      </table></td>
                    <td width="50%" valign="top" class="txtBlack11"><fieldset>
                      <legend class="txtBlack11"><strong>Terms &amp; Conditions</strong> </legend>
                      <table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                          <td colspan="2" class="txtBlack11">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="30%" height="24" align="left" class="txtBlack11">Show Button </td>
                          <td width="70%" align="left" class="txtBlack11"><input name="IsTerm" type="radio" value="1" onclick="TermsConditionButton();" <?=$IsTerm1;?>  />
                            Yes                 &nbsp;&nbsp;&nbsp;
                            <input name="IsTerm" type="radio" value="0"   onclick="TermsConditionButton();" <?=$IsTerm0;?>  />
                            No </td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left" class="txtBlack11">&nbsp;</td>
                          </tr>
                        <tr>
                          <td height="24" align="left" class="txtBlack11">Link to URL</td>
                          <td align="left" class="txtBlack11"><input name="TermURL" type="text" disabled="disabled" class="txtBlack11" id="TermURL" value="<?=$TermURL;?>" size="32" maxlength="255" /></td>
                        </tr>
                        <tr>
                          <td height="20" align="left" class="txtBlack11">&nbsp;</td>
                          <td align="left" valign="top" class="txtBlack11"><span class="noteGray10">full url or File Name </span></td>
                        </tr>
                        <tr>
                          <td height="24" align="left" class="txtBlack11">Link Target </td>
                          <td align="left" class="txtBlack11"><input name="TermTarget" type="radio" disabled="disabled" value="P"  onclick="OfferTermsConditionTarget();" <?=$TermTargetP?> />
Popup
  <input name="TermTarget" type="radio" value="N" disabled="disabled" onclick="OfferTermsConditionTarget();" <?=$TermTargetN?> />
New window </td>
                        </tr>
                        <tr>
                          <td height="24" align="left" class="txtBlack11">Popup Width </td>
                          <td align="left" class="txtBlack11"><input name="TermPopWidth" type="text" disabled="disabled" class="txtBlack11" id="TermPopWidth" value="<?=$TermPopWidth;?>" size="10" maxlength="4" />
pixels</td>
                        </tr>
                        <tr>
                          <td height="24" align="left" class="txtBlack11">Popup Height </td>
                          <td align="left" class="txtBlack11"><input name="TermPopHeight" type="text" disabled="disabled" class="txtBlack11" id="TermPopHeight" value="<?=$TermPopHeight;?>" size="10" maxlength="4" />
pixels</td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left" class="txtBlack11">&nbsp;</td>
                          </tr>
                      </table>
                      </fieldset></td>
                  </tr>
                </table></td>
              </tr>
              

              <tr>
                <td colspan="3" class="txtBlack11" id="line-bottom1">&nbsp;</td>
                </tr>
              <tr>
                <td height="42" colspan="3" align="center" class="txtBlack11">                 
				<input type="hidden" name="offerID" id="offerID" value="<?=$offerID;?>" />
				<input type="hidden" name="old_img" id="old_img" value="<?=$Thumbnail;?>" />
				 <input name="btnUpdate" type="submit" class="btn1" id="btnUpdate" value=" Save " />
                  <input name="btnReset" type="button" class="btn1" id="btnReset" value="Reset" onclick="window.location.href='offers-edit.php?id=<?=$_GET['id'];?>';" /></td>
                </tr>
              <tr>
                <td width="122" class="txtBlack11">&nbsp;</td>
                <td width="5" align="left" class="txtBlack11">&nbsp;</td>
                <td width="540" align="left" class="txtBlack11">&nbsp;</td>
              </tr>
            </table>
            </form>
          </td>
          <td width="141" align="left" valign="top"><input name="Button2" type="button" class="btnMenu" value="&laquo; Back" onclick="window.location.href='offers.php';" /></td>
        </tr>
      </table></td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
