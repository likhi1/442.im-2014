<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$SubscribeList='';
$Condition='';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();


///SubscribeID, sName, sEmail, sAge, sNationality, sDate, sStatus
$Sql="SELECT *  FROM ".$obj->tblSubscribe." WHERE SubscribeID = '".$_GET['id']."'  LIMIT 1; ";
$obj->query($Sql);
$NumAll=$obj->num_rows();

if ($NumAll>0) {
	$obj->moveNext();
		$SubscribeID=$obj->getField('SubscribeID');
		$sName=stripslashes($obj->getField('sName'));
		$sAge=$obj->getField('sAge');
		$sEmail=$obj->getField('sEmail');		
		$sNationality=$obj->getField('sNationality');
		$SubmitFrom=$obj->getField('SubmitFrom');

		$Age=$obj->getAgeList($sAge);
		$Nationality=$obj->getNationalityList($sNationality);
} else {
	$SubscribeList.='<tr><td align="left" height="50" colspan="7" class="txtBlack11">0 Result</td></tr>';
}//end if

$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter (Subscripe)</title>
<script language="javascript" src="../js/fnc_backend.js" type="text/javascript"></script>
<script language="javascript">
function onUpdate() {
	var frm=document.frmNewsletter;
	if (frm.sName.value=='') {
		alert('Name is required.');
		frm.sName.focus();
	} else if (frm.sEmail.value=='') {
		alert('E-mail is required.');
		frm.sEmail.focus();
	} else if (!checkemail(frm.sEmail.value)) {
		alert('Invalid e-mail address.');
		frm.sEmail.focus();	
	} else {
		return true;
	} 
	return false;
}

function returnEditSubscribe(txt) {
	if (txt=='true') {		
		HideResult();
	}else {
		alert('Can not delete event, try again later.');
	}
}


</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  <a href="../subscribe.php">Subscribe</a> &gt; Edit profile </td>
      </tr>
      
    </table>
     
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td width="721" align="left" valign="top"><form action="../subscribe-script.php" method="post" name="frmNewsletter" target="savetarget" id="frmNewsletter" onsubmit="return onUpdate();">
              <?=$obj->SaveResult('Successfully saved subscribe.');?>
              <table width="500" border="0" cellpadding="2" cellspacing="0">
                
                <tr>
                  <td height="5" colspan="2" ></td>
                </tr>
                <tr>
                  <td width="101" class="txtBlack11">Name<span class="noteRed10">*</span></td>
                  <td width="391" class="txtBlack12">                    <input name="sName" type="text" class="txtBlack11" id="sName" value="<?=$sName;?>" size="40" maxlength="50" />                  </td>
                </tr>
                <tr>
                  <td class="txtBlack11">Email<span class="noteRed10">*</span></td>
                  <td class="txtBlack12"><input name="sEmail" type="text" class="txtBlack11" id="sEmail" value="<?=$sEmail;?>" size="40" maxlength="50" /></td>
                </tr>
                <tr>
                  <td class="txtBlack11">&nbsp;</td>
                  <td class="txtBlack12">&nbsp;</td>
                </tr>
                <tr>
                  <td class="txtBlack11">Age<span class="noteRed10">*</span></td>
                  <td class="txtBlack12"><?=$Age;?>&nbsp;</td>
                </tr>
                <tr>
                  <td class="txtBlack11">Nationality</td>
                  <td class="txtBlack12"><?=$Nationality;?></td>
                </tr>
				     <tr>
                  <td class="txtBlack11">Subscribe From </td>
                  <td class="txtBlack11">
                    <select name="SubmitFrom" class="txtBlack11" id="SubmitFrom" style="width:200px;">                      
                      <option value="Grand President Hotel" <? echo ($SubmitFrom=='Grand President Hotel') ? 'selected="selected" ' : '';  ?>>Grand President Hotel</option>
					  <option value="President Park" <? echo ($SubmitFrom=='President Park') ? 'selected="selected" ' : '';  ?>>President Park</option>                      
                      <option value="President Solitaire" <? echo ($SubmitFrom=='President Solitaire') ? 'selected="selected" ' : '';  ?>>President Solitaire</option>
                      <option value="Royal President" <? echo ($SubmitFrom=='Royal President') ? 'selected="selected" ' : '';  ?>>Royal President</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="txtBlack12">&nbsp;</td>
                  <td class="txtBlack12">&nbsp;</td>
                </tr>
                <tr>
                  <td class="txtBlack12">&nbsp;</td>
                  <td class="txtBlack12">                   
				  <input type="hidden" name="SubscribeID" id="SubscribeID" value="<?=$SubscribeID;?>" />
				   <input name="btnUpdate" type="submit" class="btn1" id="btnUpdate" value="Update" />
                  <input name="btnReset" type="reset" class="btn1" id="btnReset" value="Reset" /></td>
                </tr>
              </table>
                        </form>
            </td>
            <td width="141" align="left" valign="top"><input name="Button2" type="button" class="btnMenu" value="« Back" onclick="window.location.href='subscribe.php';" /></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
