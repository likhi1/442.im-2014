<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$PL5 =' style="padding-left:5px;" ';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoSend='<img src="images/Email-Send.gif" border="0" alt="Send" /> ';
$icoView='<img src="images/view.gif" border="0" alt="View" /> ';
$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();

//HistoryID, Subject, Message, SendTo, SendQuery, SentDate

$Sql="SELECT HistoryID, Subject, SentDate, SenderName, SenderEmail, SendQuery FROM ".$obj->tblHistory." ORDER BY SentDate DESC ";
$obj->query($Sql);

$NewsletterList='';

if ($obj->num_rows()>0) {
	while ($obj->moveNext()) {
		$HistoryID=$obj->getField('HistoryID');
		$Subject=stripslashes($obj->getField('Subject'));
		$SentDate=$obj->getField('SentDate');
		$SenderEmail=$obj->getField('SenderEmail');
		$SenderName=$obj->getField('SenderName');
		$SendQuery=$obj->getField('SendQuery');
		
		if ($SendQuery!='') {
			$Query=$SendQuery;
		} else {
			$Query='---';
		}
		
		$View='<a href="newsletter-history-preview.php?hid='.$HistoryID.'" target="_blank">'.$icoView.'</a>';
		//$Send='<a href="newsletter-send.php?nid='.$NewsID.'">'.$icoSend.'</a>';
		$Edit='<a href="newsletter-send.php?hid='.$HistoryID.'">'.$icoEdit.'</a>';
		//$Delete='<a href="">'.$icoDelete.'</a>';
		$Delete='<a href="newsletter-scripts.php?id='.$HistoryID.'&act=del-history" onclick="return deleteHistory(\''.$Subject.'\');" class="text1" target="savetarget">'.$icoDelete.'</a>';
			
		$NewsletterList.='<tr  align="center" class="txtBlack11" '.$obj->lineOver().'>
                <td id="line-bottom1">'.date('M j, y H:i',strtotime($SentDate)).'</td>
				<td id="line-bottom1" align="left" '.$PL5.'>'.$Subject.'</td>
				<td id="line-bottom1"><span title="'.$SenderEmail.'">'.$SenderName.'</span></td>
				<td id="line-bottom1" align="left" '.$PL5.'>'.$Query.'</td>
				<td id="line-bottom1">'.$View.'</td>	
                <td id="line-bottom1">'.$Edit.'</td>
                <td id="line-bottom1">'.$Delete.'</td>
              </tr>';		
	}//end while
} //end if	
$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter</title>
<script language="javascript">
function deleteHistory(Subject) {
	var chk=confirm('Are you sure you want to delete history: \"'+Subject+'\".');
	if (chk==true) {
		return true;
	} else {
		return false;
	}
}

function returnDelete(txt) {
if (txt=='true') {
	window.location.href=window.location.href;
} else {
	alert('Can not delete history,try again later.');
}
}
</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt;  <a href="newsletter.php">Newsletter</a> &gt; History </td>
      </tr>
      
    </table>
     
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td width="682">&nbsp;</td>
            <td width="180">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="1">
              <tr>
                <td width="17%" align="center" class="headTable">Date/Time</td>
                <td width="30%" align="center" class="headTable">Subject</td>
                <td width="15%" align="center" class="headTable">Sender Name </td>
                <td width="17%" align="center" class="headTable">Recipient type </td>
                <td width="6%" align="center" class="headTable">View</td>
                <td width="9%" align="center" class="headTable">Edit/Send</td>
                <td width="6%" align="center" class="headTable">Delete</td>
              </tr>
             <?=$NewsletterList; ?>
            </table></td>
            <td align="center" valign="top"><input name="btnNews2" type="button" class="btnMenu" id="btnNews2" value="Template" onclick="window.location.href='newsletter-list.php';" />
            <input name="btnNews22" type="button" class="btnMenu" id="btnNews22" value="Send Newsletter" onclick="window.location.href='newsletter-send.php';" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" width="0" height="0" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
