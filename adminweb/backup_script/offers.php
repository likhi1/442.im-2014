<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');
$Path='../offers-images';
$OfferList='';

$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> ';

$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> ';


$obj=new admin_class();

////offerID, Title, Thumbnail, ShortDefinition, Description, IsBook, BookToLink, IsDisplay, SortOrder
$Sql="SELECT offerID, Title, Thumbnail, IsBook, IsTerm, IsDisplay, SortOrder FROM ".$obj->tblOffer." ORDER BY SortOrder ";
$obj->query($Sql);
$NumAll=$obj->num_rows();



if ($NumAll>0) {
	while ($obj->moveNext()) {
		$offerID=$obj->getField('offerID');
		$Title=stripslashes($obj->getField('Title'));
		$Thumbnail=$obj->getField('Thumbnail');
		$IsBook=$obj->getField('IsBook');
		$IsTerm=$obj->getField('IsTerm');
		$IsDisplay=$obj->getField('IsDisplay');
		$SortOrder=$obj->getField('SortOrder');
			
		if ($Thumbnail!='') {
			$THUMBNAIL='<img src="'.$Path.'/'.$Thumbnail.'" alt="" border="0" width="100" height="94" />';
		} else {
			$THUMBNAIL='xxx';
		}
		
		if ($IsBook=='1') {
			$IsBook=$icoTrue;
		} else {
			$IsBook=$icoFalse;
		}
		
		if ($IsTerm=='1') {
			$IsTerm=$icoTrue;
		} else {
			$IsTerm=$icoFalse;
		}
		
		if ($IsDisplay=='1') {
			$IsDisplay=$icoTrue;
		} else {
			$IsDisplay=$icoFalse;
		}
		
		$boxOrder='
			<input type="hidden" name="offerID[]" id="offerID[]" value="'.$offerID.'" />
			<input type="text" name="SortOrder[]" id="SortOrder[]" value="'.$SortOrder.'" class="input1" maxlength="3" />
		';
		
		$Edit='<a href="offers-edit.php?id='.$offerID.'">'.$icoEdit.'</a>';
		$Delete='<a href="offers-script.php?id='.$offerID.'&img='.$Thumbnail.'&act=delete-offer" onclick="return deleteOffer();" class="text1" target="savetarget">'.$icoDelete.'</a>';
				
		$OfferList.='
			<tr  align="center" class="txtBlack11">
		       <td id="line-bottom1">'.$boxOrder.'</td>	
              <td id="line-bottom1">'.$THUMBNAIL.'</td>
              <td id="line-bottom1" align="left" style="padding-left:5px;">'.$Title.'</td>
              <td id="line-bottom1">'.$IsDisplay.'</td>
              <td id="line-bottom1">'.$IsBook.'</td>
			  <td id="line-bottom1">'.$IsTerm.'</td>
              <td id="line-bottom1">'.$Edit.'</td>
              <td id="line-bottom1">'.$Delete.'</td>
            </tr>
		';
	}//end while
} else {

}//end if

$obj->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Offers</title>
<script language="javascript">
function deleteOffer() {
	var chk=confirm('Are you sure you want to delete this offer.');
	if (chk==true) {
		return true;
	} else {
		return false;
	}
}

function returnDeleteOffer(txt) {
	if (txt=='true') {
		window.location.href='offers.php';
	}else {
		alert('Can not delete offer, try again later.');
	}
}

function returnNo() {
	window.location.href='offers.php';
}
</script>
</head>

<body>
<? include_once('../inc_navigation.php');?>
<table width="900" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" class="bg1"><table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td height="40" align="left" class="head1"><a href="../home.php">HOME</a> &gt; Manage Offers</td>
      </tr>
      
    </table>
      <form action="offers-script.php" method="post" name="frmNo" target="savetarget" id="frmNo">
        <table width="870" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td width="721" align="left" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="1">
                <tr>
                  <td height="16" align="center" class="headTable">Order</td>
                  <td align="center" class="headTable">Thumbnail </td>
                  <td align="center" class="headTable">Title</td>
                  <td align="center" class="headTable">Published</td>
                  <td align="center" class="headTable">Book Button </td>
                  <td align="center" class="headTable">Terms &amp; <br />
                    Condition</td>
                  <td align="center" class="headTable">Edit</td>
                  <td align="center" class="headTable">Delete</td>
                </tr>
                <?=$OfferList;?>
            </table></td>
            <td width="141" align="left" valign="top"><input name="Button" type="button" class="btnMenu" value="+Add new offer" onclick="window.location.href='offers-add.php';" />
                <input name="btnUpdateOrder" type="submit" class="btnMenu" id="btnUpdateOrder" value="Update Order" />
                <input name="btnReset" type="reset" class="btnMenu" id="btnReset" value="Reset Order" />
                <input name="Button2" type="button" class="btnMenu" value="« Back" onclick="history.back();" /></td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<? include_once('../inc_footer.php');?>
<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
</body>
</html>
