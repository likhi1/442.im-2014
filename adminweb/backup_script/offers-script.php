<?
include_once('../inc_chk_no_have_sess.php');
require_once('../admin-class.php');

$obj=new admin_class();
$Path='../offers-images';
$arrImgType=array('gif','jpg','png','swf','psd','bmp', 'tiff (intel byte order)','tiff (motorola byte order)','jpc','jp2','jpx','jb2','swc','iff','wbmp','xbm');


if ($_POST['btnSave']) {
	//THUMBNAIL ---------------------------------------
	$picSize=$_FILES['file']['size'];
	$picLimitSize=204800; //200kb
	$today=date('Y-m-d-H-i-s');
	
	if ($picSize>0) { //check file size
			$arrPic=getimagesize($_FILES['file']['tmp_name']);
			$picType=$arrPic[2]; //image type
			
			if ($picType!='1' && $picType!='2' && $picType!='3') {
				$return='err_type';
			} elseif ($picSize>$picLimitSize) {
				$return='err_size';
			} else { /// true
					
					$picTypeName=$arrImgType[($picType-1)]; //find picture type from array
					$picName='offers-'.$today.'.'.$picTypeName;	 //new picture name
					 
					 	$Title=addslashes($_POST['Title']);
						$ShortDefinition=addslashes($_POST['ShortDefinition']);
						$Description=addslashes($_POST['Description']);
						
						$BookToLink=$_POST['BookToLink'];
						$IsBook=$_POST['IsBook'];
						
						$IsTerm=$_POST['IsTerm'];
						$TermURL=$_POST['TermURL'];
						$TermTarget=$_POST['TermTarget']; //P=popup, N=new window
						$TermPopWidth=$_POST['TermPopWidth'];
						$TermPopHeight=$_POST['TermPopHeight'];
						
						
						$IsDisplay=$_POST['IsDisplay'];
						$CurrencyConverter=$_POST['CurrencyConverter'];
						
						$SortOrder=$obj->getNextOfferOrder();
							
//offerID, Title, Thumbnail, ShortDefinition, Description, IsBook, BookToLink, IsTerm, TermURL, TermTarget, TermPopWidth, TermPopHeight, CurrencyConverter, IsDisplay, SortOrder, LastUpdate
						$Sql="INSERT INTO ".$obj->tblOffer." (offerID, Title, Thumbnail, ShortDefinition, Description, IsBook,  BookToLink,  IsTerm, TermURL, TermTarget, TermPopWidth, TermPopHeight, CurrencyConverter, IsDisplay, SortOrder, LastUpdate) ".
								" VALUES (NULL,'$Title', '$picName', '$ShortDefinition', '$Description', '$IsBook', '$BookToLink', '$IsTerm', '$TermURL', '$TermTarget', '$TermPopWidth', '$TermPopHeight', '$CurrencyConverter',  '$IsDisplay', '$SortOrder', NOW()) ";
						$result=$obj->query($Sql);
						if ($result) {
							$return='true';
							$obj->WritePromotionRSS();
							//upload ----------
							$rootfile=$Path.'/'.$picName;
							@copy($_FILES['file']['tmp_name'],$rootfile);	
						} else {
							$return='false';
						}																						
			} //end if check image
			  
	} else { // empty file
			$return='empty_file';
	} 
	
	
	$returnScript='<script>window.parent.returnAddOffer("'.$return.'");</script>';
}//end btnSave 

//-- Update Offer ----------------------------
if ($_POST['btnUpdate']) {
	$offerID=$_POST['offerID'];
	$Title=addslashes($_POST['Title']);
	$ShortDefinition=addslashes($_POST['ShortDefinition']);
	$Description=addslashes($_POST['Description']);

	$BookToLink=$_POST['BookToLink'];
	$IsBook=$_POST['IsBook'];
	
	$IsTerm=$_POST['IsTerm'];
	$TermURL=$_POST['TermURL'];
	$TermTarget=$_POST['TermTarget']; //P=popup, N=new window
	$TermPopWidth=$_POST['TermPopWidth'];
	$TermPopHeight=$_POST['TermPopHeight'];
	
	$IsDisplay=$_POST['IsDisplay'];
	$CurrencyConverter=$_POST['CurrencyConverter'];
	
	$oldImg=$_POST['old_img'];
	

	$Sql=" UPDATE ".$obj->tblOffer.
			" SET Title='$Title', ShortDefinition='$ShortDefinition', Description='$Description', IsBook='$IsBook', ".
			" BookToLink='$BookToLink', IsTerm='$IsTerm', TermURL='$TermURL', TermTarget='$TermTarget', TermPopWidth='$TermPopWidth', TermPopHeight='$TermPopHeight', CurrencyConverter='$CurrencyConverter', IsDisplay='$IsDisplay', LastUpdate=NOW() ".
			" WHERE offerID='$offerID' LIMIT 1; ";		
	$result=$obj->query($Sql);
	
	if ($result) {
		$return='true';
		$obj->WritePromotionRSS();
		
		//THUMBNAIL ---------------------------------------
		$picSize=$_FILES['file']['size'];
		$picLimitSize=204800; //200kb
		$today=date('Y-m-d-H-i-s');
		
		if ($picSize>0) { //check file size
				$arrPic=getimagesize($_FILES['file']['tmp_name']);
				$picType=$arrPic[2]; //image type
				
				if ($picType!='1' && $picType!='2' && $picType!='3') {
					$return='err_type';
				} elseif ($picSize>$picLimitSize) {
					$return='err_size';
				} else { /// true
						
						$picTypeName=$arrImgType[($picType-1)]; //find picture type from array
						$picName='offers-'.$today.'.'.$picTypeName;	 //new picture name						 							
								
							$Sql=" UPDATE ".$obj->tblOffer." SET Thumbnail='$picName' WHERE offerID='$offerID' LIMIT 1;  ";
							$result=$obj->query($Sql);
							if ($result) {
																
								//Delete old image
								if (file_exists($Path.'/'.$oldImg)) {
									@unlink($Path.'/'.$oldImg);
								}
								
								//upload ----------
								$rootfile=$Path.'/'.$picName;
								@copy($_FILES['file']['tmp_name'],$rootfile);	
							} 
							$return='true';																				
				} //end if check image
				  
		} //End Thumbnail Edit
	} else {
		$return='false';
	}
	
	$returnScript='<script>window.parent.returnEditOffer("'.$return.'");</script>';
}//end btnUpdate

//Update Order No all.
if ($_POST['btnUpdateOrder']) {
	$num=count($_POST['SortOrder']);

	for ($i=0;$i<$num;$i++) {
		$Sql="UPDATE ".$obj->tblOffer." SET SortOrder='".$_POST['SortOrder'][$i]."' WHERE offerID='".$_POST['offerID'][$i]."' LIMIT 1; ";
		$obj->query($Sql);	
	} //end for

	$obj->WritePromotionRSS();
	$obj->clearResult();
	$returnScript='<script>window.parent.returnNo();</script>';	
} //end update no order


//------Delete Product --------------
if ($_GET['act']=='delete-offer' && $_GET['id']!='') {

	$result=$obj->query('DELETE FROM '.$obj->tblOffer.' WHERE offerID="'.$_GET['id'].'" LIMIT 1; ');
	
	if ($result) {
		if (file_exists($Path.'/'.$_GET['img'])) {
			@unlink($Path.'/'.$_GET['img']);	
		}
		$return ='true';
		$obj->WritePromotionRSS();
	} else {
		$return='false';
	}

	$returnScript='<script>window.parent.returnDeleteOffer("'.$return.'");</script>';	
}//End Delte offer




$obj->close();

echo $returnScript;
?>