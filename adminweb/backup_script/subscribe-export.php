<?
header("Content-Type:application/msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=subscribe-info.xls");

 //--------------------------------------------------------------------------------------

//	include_once('../inc_chk_no_have_sess.php');
	require_once('../admin-class.php');
	
	$obj=new admin_class();
	
	$Sql = urldecode($_GET['sql']);	
	$Sql = str_replace("-","%",$Sql);
	$Sql = str_replace("|","'",$Sql);
	
	$obj->query($Sql);
	$NumAll=$obj->num_rows();

		
	if ($NumAll>0) {
			$rows=0;
			
			require_once "../../class/excel/class.writeexcel_workbook.inc.php";
			require_once "../../class/excel/class.writeexcel_worksheet.inc.php";
			
			$fname = tempnam(tmpfile(), "subscribe.xls"); 
			
		
			$workbook = &new writeexcel_workbook($fname);
			$worksheet = &$workbook->addworksheet();
			
		
			$worksheet->set_column(0, 0, 25);
			$worksheet->set_column(1, 1, 35);
			$worksheet->set_column(2, 2, 20);
			$worksheet->set_column(3, 3, 20);
			$worksheet->set_column(4, 4, 30);
			
			
			$header =& $workbook->addformat();
			$header->set_bold();
			$header->set_align('center');
			$header->set_size(10);
			$header->set_color('black');
			$header->set_fg_color('silver');
			
			
			$txtLeft =& $workbook->addformat();
			$txtLeft->set_align('left');
			
			$f_ID =& $workbook->addformat();
			$f_ID->set_align('center');
			
			
			$worksheet->set_row(0, 16);//ความสูงของบรรทัด
			
			# Write some text header
			
			$worksheet->write(0, 0,  "Name", $header);
			$worksheet->write(0, 1,  "E-mail address", $header);
			$worksheet->write(0, 2,  "Age", $header);
			$worksheet->write(0, 3,  "Nationality", $header);
			$worksheet->write(0, 4,  "Subscribe From", $header);
			$worksheet->write(0, 5,  "Subscribe Date", $header);
	
			while ($obj->moveNext()) {
						          
				$sName=$obj->getField('sName');
				$sEmail=stripslashes($obj->getField('sEmail'));
				$sAge=$obj->getField('sAge');
				$sNationality=$obj->getField('sNationality');
				$SubmitFrom=$obj->getField('SubmitFrom');
				$sDate=$obj->getField('sDate');				

				$rows++;
				//$worksheet->set_row($rows, 30);
				
				$worksheet->write($rows, 0,  $sName, $txtLeft);  
				$worksheet->write($rows, 1,  $sEmail, $txtLeft);
				$worksheet->write($rows, 2,  $sAge, $txtLeft);  
				$worksheet->write($rows, 3,  $sNationality, $txtLeft); 
				$worksheet->write($rows, 4,  $SubmitFrom, $txtLeft);  
				$worksheet->write($rows, 5,  date('M j, Y H:i',strtotime($sDate)), $txtLeft); 
				
				
			} //end while
			
			$workbook->close();

			$fh=fopen($fname, "rb");
			fpassthru($fh);
			unlink($fname);		

		
	} else { 	// <0
		echo '<script>alert("File can not be found."); history.back();</script>';
	}
			
	$obj->close();
	
?>
