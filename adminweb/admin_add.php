<?php
session_start();  
/*----- if not admin level 3  not allow acess -------------------------------------*/
if (  $_SESSION['adminSession']==''   ||  $_SESSION['adminLevel']!=  3 ) {    
echo '<meta http-equiv="refresh" content="0;URL=login.php" />';
exit() ;
}

require_once('../common/connect.php');  
require_once('../common/global.php');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Contact Us</title>
<?php  include_once('inc_script_header.php'); ?>
 
</head>

<body>
<div  id="container">  
<? include_once('inc_navigation.php');?>
<div id="content"> 



<div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>  >   
<a href="<?php  breadCrumb() ;?>">Account Add</a> 
</div>  
<!--   end  class="breadCrumb" --> 

<form action="admin_add_script.php" method="post" name="frmAddAcount"    onsubmit="return  chkAddAccount()">
        <table width="100%" border="0" cellspacing="5">
          <tr>
            <th width="12" rowspan="8" align="right"  >&nbsp;</th>
            <th colspan="2" align="left"  style=" font-weight:normal" >Create  new  level user for control backend ... </th>
          </tr>
          <tr>
            <th align="right"  >&nbsp;</th>
            <th align="left"  >&nbsp;</th>
          </tr>
          <tr>
            <th width="130" align="right"  ><label  > Name :</label></th>
            <th width="840" align="left"  > <input type="text" name="adminName" id="adminName" /></th>
          </tr>
          <tr>
            <th align="right"  ><label  > Email :</label></th>
            <th align="left"  > <input type="text" name="adminEmail" id="adminEmail" /></th>
          </tr>
          <tr>
            <th align="right"  ><label  > Pass :</label></th>
            <th align="left"  > <input type="text" name="adminPass" id="adminPass" /></th>
          </tr>
          <tr>
            <th align="right"  ><label  >Confirm  Pass :</label></th>
            <th align="left"  > <input type="text" name="adminPass2" id="adminPass2" /></th>
          </tr>
          <tr>
            <th align="right"  ><label for="user_name3">Acess Level :</label></th>
            <th align="left"  >  
              <select name="adminLevel" id="adminLevel" >
                <option value=""> -- Please Select -- </option>
                <!--      <option value="1">Editor</option>-->
                <option value="2">Manager</option>
              </select></th>
          </tr>
          <tr>
            <td height="20">&nbsp;</td>
            <td><input type="submit" name="button" id="btnAddAccount" value="Add Account"  />
              <input type="reset" name="button2" id="button2" value="Reset" /></td>
          </tr>
        </table>
      </form>

</div><!--  end  id="content" --->
 
<div class="clear"></div>
<? include_once('inc_footer.php');?>
</div><!--  end  id="container" -->

<!--<iframe src="" id="savetarget" name="savetarget" style=" display:none;"></iframe>-->
</body>
</html>
