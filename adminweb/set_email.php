<?php
session_start(); 
/*----- if not admin level 3  not allow acess -------------------------------------*/
if (  $_SESSION['adminSession']==''   ||  $_SESSION['adminLevel']!=  3 ) {    
echo '<meta http-equiv="refresh" content="0;URL=login.php" />';
exit() ;
}
  
require_once('../common/global.php');
require_once( '../common/connect.php' ); 

require_once('admin-class.php');  
$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> '; 
$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> '; 
$obj=new admin_class();
$obj->getConfig();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>CMS: Newsletter</title>
<?php  include_once('inc_script_header.php'); ?>
<script language="javascript">
<!--
function onUpdateSetting() {
	var frm=document.frmSetting;
	var result=document.getElementById('config_result');	
	
	var note1=document.getElementById('note1');
	var note2=document.getElementById('note2');
	var note3=document.getElementById('note3');
	
	var n=0;
	
	if (frm.MEETING_EMAIL_TO.value=='') {
		note1.innerHTML='* required.';
		frm.MEETING_EMAIL_TO.focus();				
		n++;
	} else {
		note1.innerHTML='*';
	}
	
	if (frm.QUESTIONNAIRE_EMAIL_TO.value=='') {
		note2.innerHTML='* required.';
		frm.QUESTIONNAIRE_EMAIL_TO.focus();				
		n++;
	} else {
		note2.innerHTML='*';
	}
	
	if (frm.EMAIL_SYSTEM.value=='') {
		note3.innerHTML='* required.';
		frm.EMAIL_SYSTEM.focus();				
		n++;
	} else {
		note3.innerHTML='*';
	}
	
	if (n<=0) {
		return true;
	} else {
		return false;
	}
	
}

var interval='';
var sumTime=0;



function returnOnUpdateSetting(txt) {
	if (txt=='true') {
		HideResult();	
	} else {
		alert('Can not update, try again later');
	}
}
//-->
</script>
</head>

<body>
<div  id="container">
  <?php  include_once('inc_navigation.php');?>
  <div id="content">
  
  <div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>  >   
<a href="<?php  breadCrumb() ;?>">Setting System Email</a> 
</div>  
<!--   end  class="breadCrumb" -->


<h1>&raquo; Setting System E-mail</h1>
<?php echo $obj->SaveResult('<span class="T" >Successfully saved setting.<span>');?>
 
 <form action="set_email_scripts.php" method="post" name="frmSetting" target="savetarget" id="frmSetting" onsubmit="return onUpdateSetting();">
                 <table width="380" border="0" cellpadding="3" cellspacing="0">
                          <tr>
                            <td colspan="2" align="left" bgcolor="#3F3C2B" class="headTable"><strong>&nbsp;Alert to e-mail for new submit contact us.</strong></td>
                          </tr>
                          <tr>
                            <td width="50" align="right" bgcolor="#e1e1e1" class="txtBlack11"><strong>To:</strong></td>
                            <td width="338" align="left" bgcolor="#e1e1e1" class="txtBlack11"><input name="QUESTIONNAIRE_EMAIL_TO" type="text" class="txtBlack11" id="QUESTIONNAIRE_EMAIL_TO" value="<?=$obj->context['QUESTIONNAIRE_EMAIL_TO'];?>" size="40" />
                              <span class="noteRed10" id="note2">*</span></td>
                          </tr>
                          <tr>
                            <td align="right" bgcolor="#e1e1e1" class="txtBlack11"><strong>Cc:</strong></td>
                            <td align="left" bgcolor="#e1e1e1" class="txtBlack11"><input name="QUESTIONNAIRE_EMAIL_CC" type="text" class="txtBlack11" id="QUESTIONNAIRE_EMAIL_CC" value="<?=$obj->context['QUESTIONNAIRE_EMAIL_CC'];?>" size="40" /></td>
                          </tr>
                          <tr>
                            <td height="10" bgcolor="#e1e1e1"></td>
                            <td height="10" align="left" valign="top" bgcolor="#e1e1e1"><span class="noteGray10">* Separate email by a comma ( , )</span></td>
                          </tr>
                          <!--<tr>
                      <td height="20" bgcolor="#e1e1e1"></td>
                      <td height="10" align="left" bgcolor="#e1e1e1"><span class="noteGray10">* See to <a href="../guest-questionnaire.html" target="_blank">Application form</a></span></td>
                    </tr>-->
                        </table> <br />

         <table width="380" border="0" cellpadding="3" cellspacing="0">
                          <tr>
                            <td colspan="2" align="left" bgcolor="#3F3C2B" class="headTable"><strong>&nbsp;E-mail System (For auto-reply and no-reply)</strong></td>
                          </tr>
                          <tr>
                            <td width="50" align="right" bgcolor="#e1e1e1" class="txtBlack11"><strong>From:</strong></td>
                            <td width="338" align="left" bgcolor="#e1e1e1" class="txtBlack11"><input name="EMAIL_SYSTEM" type="text" class="txtBlack11" id="EMAIL_SYSTEM" value="<?php echo $obj->context['EMAIL_SYSTEM'];?>" size="40" />
                              <span class="noteRed10" id="note3">*</span></td>
                          </tr>
                          <tr>
                            <td height="10" bgcolor="#e1e1e1"></td>
                            <td height="10" align="left" bgcolor="#e1e1e1"><span class="noteGray10">* Required e-mail address. </span></td>
                          </tr>
                        </table><br />
<input name="btnUpdateEmail" type="submit" class="btn1" id="btnUpdateEmail" value=" Update " />
                        <input name="btnUpdateSetting2" type="reset" class="btn1" id="btnUpdateSetting2" value="  Reset  " /> 
                
                </form></td>
            </tr>
            <tr>
              <td align="center">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
    </table>
  </div>
  <!--  end  id="content" --->
  
  <div class="clear"></div>
  <?php  include_once('inc_footer.php');?>
</div>
<!--  end  id="container" -->

<iframe src="" id="savetarget" name="savetarget" style="border:0px;width:0px;height:0px;margin:0;padding:0;"></iframe>
<?php $obj->close(); ?>
</body>
</html>
