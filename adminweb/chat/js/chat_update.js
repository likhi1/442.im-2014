// JavaScript Document


/*-----Initial ------------------------------------------------------------------------------------------------------------------------------------------------------*/
// 1 minute  = 1000*60  
var setFastUpdate   =   30000 ;
var setSlowUpdate  =  30000 ;  

/*------Define Function ---------------------------------------------------------------------------------------------------------*/  
		/*------ chat fontend update --------------------------*/ 	
		function chat_fontend_update(){ 
		$.post("chat_update.php" ,  {  fontend_uid : $("#user-id").val()   , fontend_uname : $("#user-name").val()   } , 
				function(data){  
				 $('#msg-hold').html(data); 
				})      
				setTimeout('chat_fontend_update()', setSlowUpdate ); 
		} 
		
       /*------ chat backend update---------------------------*/ 	
		function chat_backend_update(){   
		$.post("chat_update.php" ,   {  backend_uid : $("#user-id").val()  ,   backend_uname : $("#user-name").val()   } , 
				function(data){  
				$('#msg-hold').html(data); 
				})     
		setTimeout('chat_backend_update()',  setSlowUpdate  );  
		} 
   
		  
$(document).ready(function(e) { 		 
/*------- Run Function-------------*/  
chat_fontend_update();
chat_backend_update();
 
});// end ready
