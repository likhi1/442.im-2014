<?php
session_start(); 
/*----- if not admin level 3  not allow acess -------------------------------------*/
if (  $_SESSION['adminSession']==''   ||  $_SESSION['adminLevel']!=  3 ) {    
echo '<meta http-equiv="refresh" content="0;URL=login.php" />';
exit() ;
}
 
require_once('../common/global.php');
require_once( '../common/connect.php' ); 
 
 require_once('admin-class.php');  
$icoTrue='<img src="images/chkTrue.png" border="0" alt="Yes" /> ';
$icoFalse='<img src="images/none.png" border="0" alt="No" /> '; 
$icoEdit='<img src="images/edit.png" border="0" alt="Edit" /> ';
$icoDelete='<img src="images/delete.png" border="0" alt="Delete" /> '; 
$obj=new admin_class();
$obj->getConfig();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>Administration Login</title>
<?php  include_once('inc_script_header.php'); ?>

<script type="text/javascript">
 
function returnOnUpdateSetting(txt) {
	if (txt=='true') {
		window.location.href='set_pass.php';
		alert('เปลี่ยนพาสเวิสเรียบร้อยแล้วครับ.');
	}else {
		alert('ไม่สามารถเปลี่ยนพาสเวิสได้ครับ.');
	}
}

function PasswdSubmit() {
	var frm = document.form1;
	var  valueP1 = document.getElementById('Password1').value ;
	var  valueP2 = document.getElementById('Password2').value ;
	
	//alert(valueP1+"="+valueP2 );
	
	if (frm.Password.value=='') {
		alert("กรุณากรอก รหัสผ่านใหม่ ของคุณด้วยครับ");
		document.getElementById('Password1').focus();
		return false;
	} else if (frm.Password2.value=='')  {
		alert("กรุณากรอก รหัสผ่านใหม่ ของคุณด้วยครับ");
		document.getElementById('Password2').focus();
		return false;
	} else if ( valueP1  !=  valueP2)  {
		alert("กรุณากรอก รหัสผ่าน ให้เหมือนกัน ด้วยครับ");
		document.getElementById('Password2').focus();
		return false;
	} else {
		return true;
	}
}	
 
</script>
</head>

<body>
<div  id="container">  
<?php  include_once('inc_navigation.php');?>
<div id="content"> 


<div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>  >   
<a href="<?php  breadCrumb() ;?>">Password Setting</a> 
</div>  
<!--   end  class="breadCrumb" -->

<h1>&raquo; Password Setting </h1> 
<table width="380" border="0"   cellpadding="2" cellspacing="1">
      <form id="form" name="form1" action="set_pass_script.php" method="post"    target="savetarget" onsubmit="return PasswdSubmit();">
          <tr>
            <td colspan="2"  align="left" bgcolor="#3F3C2B" class="headTable" >&nbsp;</td>
          </tr>
          <tr>
            <td width="108" align="right" bgcolor="#e1e1e1" class="txtBlack11" >New Password :</td>
            <td width="261" class="txtBlack11" bgcolor="#e1e1e1" ><input name="password1" type="text" class="txtBlack11" id="password1" /></td>
          </tr>
          <tr>
            <td height="35" align="right" bgcolor="#e1e1e1" class="txtBlack11">Again Password : </td>
            <td class="txtBlack11" bgcolor="#e1e1e1" ><input name="password2" type="text" class="txtBlack11" id="password2" /></td>
          </tr>
          <tr>
            <td height="35" class="txtBlack11" bgcolor="#e1e1e1" >&nbsp;</td>
            <td class="txtBlack11" bgcolor="#e1e1e1" ><input name="btnSubmit" type="submit" class="btn1" id="btnSubmit" value="Submit" /></td>
          </tr>
        </form>
  </table>
  
 
</div><!--  end  id="content" --->
 
<div class="clear"></div>
<? include_once('inc_footer.php');?>
</div><!--  end  id="container" -->

<iframe src="" id="savetarget" name="savetarget" style=" display:none; width:800px;"></iframe>
</body>
</html>
