<?php


     /*
     ###############################################
     ####                                       ####
     ####    Author : Harish Chauhan            ####
     ####    Date   : 31 Dec,2004               ####
     ####    Updated:                           ####
     ####                                       ####
     ###############################################

     */

	//if you want to send mail through PHP mail function then just commnet ir.
	require("smtp.inc.php");


	class MIMEMAIL
	{
		var $parts;
		var $to;
		var $cc;
		var $bcc;
		var $from;
		var $headers;
		var $subject;
		var $body;
		var $html;
		var $host;
		var $port;
		
		function MIMEMAIL()
		{
			$this->parts=array();
			$this->to="";
			$this->cc="";
			$this->bcc="";
			$this->from="";
			$this->subject="";
			$this->body="";
			$this->headers="";
			$this->html=false;
		}
		
		function add_attachment($message,$name="",$ctype="application/octet-stream")
		{
			$this->parts[]=array(
							"ctype"=>$ctype,
							"message"=>$message,
							"encode"=>"base64",
							"name"=>$name
							);			
		}
		
		function build_message($part)
		{
			$message=$part["message"];
			$message=chunk_split(base64_encode($message));
			$encoding="base64";
			return "Content-Type: ".$part["ctype"].($part["name"]? ";name=\"".$part["name"]."\"" : "").
					"\nContent-Transfer-Encoding: $encoding\n\n$message\n";
			
		}
		
		function build_multipart()
		{
			$boundry="HKC".md5(uniqid(time()));
			$multipart="Content-Type: multipart/mixed; boundary= \"$boundry\"\n\n";
			$multipart.="This is a MIME encoded message.\n\n--$boundry";
			
			for($i=sizeof($this->parts)-1;$i>=0;$i--)
			{
				$multipart.="\n".$this->build_message($this->parts[$i])."--$boundry";
			}
			return $multipart.="--\n";
		}
		
		function get_mail($complete=true)
		{
			$mime="";
			if(!empty($this->from))
				$mime.="From: ".$this->from."\n";
			if(!empty($this->headers))
				$mime.=$this->headers."\n";
			if($complete)
			{
				if(!empty($this->cc))
					$mime.="Cc: ".$this->cc."\n";
				if(!empty($this->bcc))
					$mime.="Bcc: ".$this->bcc."\n";
				if(!empty($this->subject))
					$mime.="Subject: ".$this->subject."\n";
			}
			
			if(!empty($this->body))
				$this->add_attachment($this->body,"",($this->html? "text/html":"text/plain"));
			$mime.= "MIME-Version: 1.0\n".$this->build_multipart();
			return $mime;
		}
		
		function send()
		{
			if(!empty($this->cc))
				$mime=$this->get_mail(true);
			else
				$mime=$this->get_mail(false);

			if(!empty($this->host))
				ini_set("SMTP",$this->host);
				
			$smtp=new SMTPMAIL;
			return $smtp->send_smtp_mail($this->to,$this->subject,$mime,$this->cc,$this->from);
			//echo $smtp->error;
			//if you want to send mail through PHP mail function then just uncommnet ir.
			//return mail($this->to,$this->subject,"",$mime);
		}
	}
	
	
	//////Example
	
	
	  /*$fp=fopen("ram.jpg","r");
	  $data=fread($fp,filesize("ram.jpg"));
	  fclose($fp);
	  
	  ini_set("SMTP","192.168.0.26");
	  $mail=new MIMEMAIL();
	  $mail->from="harishc@templatearena.com";
	  $mail->to="harishc@templatearena.com";
	  $mail->subject="welcome";
	  $mail->body="<b>This is simple mail</b>";
	  $mail->html=true;
	  $mail->add_attachment($data,"ram.jpg" ,"image/jpeg" );
	  $mail->send();*/
	  
	 ///////////////End

?>