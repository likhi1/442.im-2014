<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="index,follow" name="robots" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
    <link href="Css/developer-style.css" rel="stylesheet"  media="screen" type="text/css" />
    <script src="Js/developer-functions.js" type="text/javascript"></script>
    <script src="Js/jquery-latest.js" type="text/javascript"></script>
    <meta content="iPod,iPhone,Webkit,iWebkit,Website,Create,mobile,Tutorial,free" name="keywords" />
<title>ฝากถอน 24 ชั่วโมง</title>
    <script type="text/javascript" >
	    onload = function ()
		{
			var msg1 = '<? echo $_GET["msg1"]; ?>';
			onFooter(msg1);
		}
		function onFooter(msgName)
		{
		if(msgName == '1')
			alert('ระบบได้บันทึก เบอร์โทร ของคุณเรียบร้อยแล้วครับ');
		else
		if(msgName == '0')
			alert('ระบบไม่สามารถบันทึก เบอร์โทร กรุณาลองใหม่อีกครั้ง');
		}
		function onClickPhone() {
			window.location = "SendPhone.php?txtPhone="+document.getElementById("txtPhone").value+"&txtLink="+document.getElementById("txtLink").value;
        }
		</script>
</head>

<body>
<div id="topbar"  class="rad">
	<div id="title">ฝากถอน 24 ชั่วโมง</div>
	<div id="leftbutton">
		<a href="Index.php" class="noeffect">Home</a> </div>
</div>
<div id="content">
	<ul class="pageitem"  style="background-color:#6A0304;color:#fff; ">
    <li class="textbox"><span class="header" style="font-size:11px;color:#FFF"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="width:10px;" valign="top"  > 1.</td><td><span style="color:#FFFF00;font-size:12px;">(ท่านสามารถโทรขอรับเลขที่บัญชีจาก Call Center 0837333334-9, 0823777770-6, 0822209992-6)</span> หลังจากนั้นทางบริษัทจะจัดการทุกอย่างให้ท่านโดยไม่มีเงื่อนไขใดๆ </td></tr></table></span>
		</li>
<li class="menu"><a href="https://ebank.kasikornbank.com/kcyber/login_th.html"><img alt="list" src="thumbs/kbk.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.กสิกรไทย</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://www.scbeasy.com/v1.4/site/presignon/index.asp"><img alt="list" src="thumbs/csb.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.ไทยพาณิชย์</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://www.tmbdirect.com/"><img alt="list" src="thumbs/tmb.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.ทหารไทย</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://www.krungsrionline.com/cgi-bin/bvisapi.dll/krungsri_ib/login/login.jsp"><img alt="list" src="thumbs/ksc.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.กรุงศรีอยุธยา</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://www.ktbonline.ktb.co.th/ibhtdocs/th_firstpageconsumer.html"><img alt="list" src="thumbs/ktc.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.กรุงไทย</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://www.scibinet.com/retail/Login.do?action=form&lang=en_US"><img alt="list" src="thumbs/nkt.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.นครหลวงไทย</span><span class="arrow"></span></a></li>
<li class="menu"><a href="https://ibanking.bangkokbank.com/SignOn.aspx"><img alt="list" src="thumbs/btk.gif" /><span class="name" style="font-size:9pt;color:#FFFF00">ธ.กรุงเทพ</span><span class="arrow"></span></a></li>
<li class="textbox"><span class="header" style="font-size:11px;color:#fff"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="width:10px" valign="top"  >2.</td><td>เพื่อความสะดวกในการปรับยอดของสมาชิก ควรโอนพร้อมเศษ<span style="color:#FFFF00;font-size:12px;" > เช่น 1,001 บาท หรือ 2,000.9 บาท *ปรับยอดตามจริง*</span></td></tr></table></span></li>
<li class="textbox"><span class="header" style="font-size:11px;color:#fff"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="width:10px" valign="top"  >3.</td><td>หลังจากโอนเงินแล้วควรโทรแจ้งภายใน 15 นาที เพื่อแสดงความเป็นเจ้าของ <span style="color:#FFFF00;font-size:12px;" >(ห้ามโทรก่อนโอนเด็ดขาด)</span> ควรเก็บหลักฐานการโอนไว้จนกว่าจะมีการปรับยอดเงินเสร็จสมบูรณ์ <span style="color:#FFFF00" >(เราจะปรับยอดให้เร็วที่สุดเพื่อความต่อเนื่องในการเล่นของท่านสมาชิก)</span></td></tr></table></span></li>
<li class="textbox"><span class="header" style="font-size:11px;color:#fff"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="width:10px" valign="top"  >4.</td><td>หากมีข้อผิดพลาดใดๆในบัญชีของท่านโปรดแจ้งกับทางบริษัทให้เร็วที่สุด ภายใน 24 ชั่วโมง</td></tr></table></span></li>
</ul>
</div>
<div id="footer"   >
<ul class="pageitem" style="background-color:#290000" >
<li class="menu" style="height:90px;text-align:center ;" >
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;" ><tr><td colspan="2">
            <img src="thumbs/442_14.jpg" alt="list" style="width:100%;height:60px;" />
</td></tr><tr><td colspan="2" align="center">
<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" style="width:82px;height:20px;" >
    <input id="txtPhone" type="text" style="width:80px;" />
</td><td style="width:25px" valign="top"   >
    <a href="javascript:onClickPhone();" ><img src="Img/set2.gif" alt="list" style="width:20px;height:20px" /></a>
</td></tr></table>
</td></tr></table>
            </li></ul>
			<input id="txtLink" name="txtLink" type="hidden" value="Bank.php?" />
</div>
</body>
</html>
