<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="index,follow" name="robots" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
    <link href="Css/developer-style.css" rel="stylesheet"  media="screen" type="text/css" />
    <script src="Js/developer-functions.js" type="text/javascript"></script>
    <script src="Js/jquery-latest.js" type="text/javascript"></script>
    <meta content="iPod,iPhone,Webkit,iWebkit,Website,Create,mobile,Tutorial,free" name="keywords" />
<title>Contact Us</title>
    <script type="text/javascript" >
	    onload = function ()
		{
			var msg = '<? echo $_GET["msg"]; ?>';
			var msg1 = '<? echo $_GET["msg1"]; ?>';
			if(msg != '')
			{
				alert(msg);
			}
			onFooter(msg1);
		}
		function onFooter(msgName)
		{
		if(msgName == '1')
			alert('ระบบได้บันทึก เบอร์โทร ของคุณเรียบร้อยแล้วครับ');
		else
		if(msgName == '0')
			alert('ระบบไม่สามารถบันทึก เบอร์โทร กรุณาลองใหม่อีกครั้ง');
		}
        function onClickPhone() {
			window.location = "SendPhone.php?txtPhone="+document.getElementById("txtPhone").value+"&txtLink="+document.getElementById("txtLink").value;
        }
        function onClickSubmit() {
            var msg = "";
            if (document.getElementById("txtEmail").value == "")
                msg = "กรุณากรอก อีเมลล์ ของคุณด้วยครับ";
            else
                if ((document.getElementById("txtEmail").value.indexOf("@") == -1) || 
				    (document.getElementById("txtEmail").value.indexOf(".") == -1))
                    msg = "กรุณากรอก อีเมลล์ ให้ถูกต้องครับ";

            if (document.getElementById("txtPhoneC").value == "")
                msg = "กรุณากรอก หมายเลขโทรศัพท์ ของคุณด้วยครับ";
            else
                if (!chkNum(document.getElementById("txtPhoneC").value))
                    msg = "กรุณากรอก หมายเลขโทรศัพท์ ให้ถูกต้องครับ";

            //if (document.getElementById("txtSec").value == "")
            //    msg = "ระบบไม่สามารถบันทึก ข้อมูลของคุณ กรุณาลองใหม่อีกครั้ง";
            if (msg != "") {
                alert(msg);
                return false;
            }
            return true;
        }
        function clear() {
            document.getElementById("bntNo").click();
        }
        function chkNum(num) {
            var nuO = parseFloat(num);
            if (isNaN(nuO))
                return false;
            else
                return true;
        }
</script>
</head>

<body>
    <div id="topbar" class="rad">
	<div id="title">Contact Us</div>
	<div id="leftbutton"><a href="Index.php" class="noeffect">Home</a></div>
</div>
<div >
 <form action="SendContact.php"  method="get" id="content" name="content">
	<ul class="pageitem"  style="background-color:#6A0304;">
        <li class="textbox"><table border="0" cellpadding="0" cellspacing="0" style="width:260px">
        <tr>
        <td align="right" ><span style="color:#fff;font-size:small;">อีเมล์ : <input type="text" id="txtEmail" name="txtEmail" value=""  style="border-color : Red ;background-color:#360a01;color:#fff;" /></span></td>
        </tr>
        </table></li>
        <li class="textbox"><table border="0" cellpadding="0" cellspacing="0" style="width:260px">
        <tr>
        <td align="right" ><span style="color:#fff;font-size:small;">หมายเลขโทรศัพท์ : <input type="text" id="txtPhoneC" name="txtPhoneC" value="" style="border-color : Red ;background-color:#360a01;color:#fff;" /></span></td>
        </tr>
        </table></li>
        <li class="textbox"><table border="0" cellpadding="0" cellspacing="0" style="width:260px">
        <tr>
        <td align="right" valign="top" style="width:112px"  ><span style="color:#fff;font-size:small;">ข้อความ :&nbsp;&nbsp;</span></td>
        <td align="left"   ><textarea id="txtMsg" name="txtMsg" cols="10" style="width:143px; border-color : Red ;background-color:#360a01;color:#fff;" rows="4"></textarea></td>
        </tr>
        </table></li>
       <!-- <li class="textbox"><table border="0" cellpadding="0" cellspacing="0" style="width:260px">
        <tr>
        <td align="right" ><span style="color:#fff;font-size:small;">Security Code : <input type="text" id="txtSec" name="txtSec" value="" style="border-color : Red ;background-color:#360a01;color:#fff;" /></span></td>
        </tr>
        </table></li>-->
        <li class="menu button">
        <input type="submit"   id="bntOK"  value ="ตกลง" 
                style="cursor:Pointer;color:#ff9903;" onclick="return onClickSubmit();" /></li>
        <li class="menu button">
            <input name="bntNo" type="reset" value="ยกเลิก"  style="cursor:Pointer;color:#ff9903;" /></li>
    </ul>
     </form>
</div>
<div id="footer" >
<ul class="pageitem" style="background-color:#290000" >
<li class="menu" style="height:90px;text-align:center ;" >
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;" ><tr><td colspan="2">
            <img src="thumbs/442_14.jpg" alt="list" style="width:100%;height:60px;" />
</td></tr><tr><td colspan="2" align="center">
<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top" style="width:82px;height:20px;" >
    <input id="txtPhone" name="txtPhone" type="text" style="width:80px;" />
</td><td style="width:25px" valign="top"   >
    <a href="javascript:onClickPhone();" ><img src="Img/set2.gif" alt="list" style="width:20px;height:20px" /></a>
</td></tr></table>
</td></tr></table>
            </li></ul>
<input id="txtLink" name="txtLink" type="hidden" value="Contact.php?" />
</div>
</body>
</html>
