﻿<?php 
/*------ Set time Zone  ---------------------------------------------------------------------------------------------*/
 date_default_timezone_set('Asia/Bangkok'); 
  
/*------ Thai mounth ----------------------------------------------------------------------------------------------*/
function  DateThai($strDate) { 
$strYear = date("Y",strtotime($strDate))+543; 
$strYear  = substr($strYear, -2); // <= option  

$strMonth= date("n",strtotime($strDate));
$strDay= date("j",strtotime($strDate));
$strHour= date("H",strtotime($strDate));
$strMinute= date("i",strtotime($strDate));
$strSeconds= date("s",strtotime($strDate));
$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
$strMonthThai=$strMonthCut[$strMonth];
return "$strDay$strMonthThai$strYear - $strHour:$strMinute";
}


/*------------ Method Date Time ---------------------------------------------------------------------------------*/	
//Date Time Formate from yyyy-mm-dd h:i:s example : 2008-10-30 12:30:00  
	
 $shortMonth=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
 $longMonth=array("January","February","March","April","May","June","July","August","September","October","November","December");
 $thaiMonth = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
 $engDay =array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
 	
	function convert2Timestamp($txt) { 
		$D=substr($txt,8,2);
		$M=substr($txt,5,2);
		$Y=substr($txt,0,4);
		$H=substr($txt,11,2);
		$I=substr($txt,14,2);
		$S=substr($txt,17,2); 
		return mktime($H,$I,$S,$M,$D,$Y);
	}	
			  
	 function ShortDate($txt) {
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year;
	}	

	function ShortDateTime($txt){
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,8);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year." ".$Time;
	}	

   function ShortThaiDateTime($txt){
		$Year=  substr(substr($txt,0,4)+543   ,  2);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,5);
		$Month=$Month-1;
		//return $DayNo." ".$thaiMonth[$Month].$Year." - ".$Time;
		
		 return $DayNo." ". $Month." ". $Year." - ".$Time;
		 

 
       // return $txt; // check  format of DateTime
	}	
	

 /*------ Resize Photo (Fake)  -------------------------------------------------------------------------------------------*/
function Resize($quiz_attach  ){
 
 $propertyImage =	getimagesize( "attach/$quiz_attach"  );	

 //print_r($propertyImage) ;
		if ($propertyImage[0]>630 ){
		    $w= 630;
			$h=(630*$propertyImage[1])/$propertyImage[0] ;
		}else{
			$w= $propertyImage[0];
			$h= $propertyImage[1];
		}	
		$output  =  '<img src="attach/'.$quiz_attach.'" width="'.$w.'"  height="'.$h.'"  />' ;
		echo  $output ;
		//echo "<hr>";
		return $output ;
 }

 
 /*------  Random Text  For Captcha   ----------------------------------------------------------------------------------------------*/
 function RanDomStr(){
		$str2ran = 'abcdefghijklmnpqrstuvwxyz123456789'; //string ที่เป็นไปได้ที่จะใช้ในการ random ซึ่งสามารถเพิ่มลดได้ตามความต้องการ
		$str_result = "";  //สตริงว่างสำหรับจะรับค่าจากการ random
		while(strlen($str_result)<4){  //วนลูปจนกว่าจะได้สตริงตามความยาวที่ต้องการ
			$str_result .= substr($str2ran,(rand()%strlen($str2ran)),1); //ต่อ string จาก substring ที่ได้จากการ random ตำแหน่ง ทีละ 1 ตัว 
																													//จนกว่าจะครบตรามความยาวที่ส่งมา
		} 
		 
		 return $str_result ;    
	}
  
 /*------ make nav active menu    ----------------------------------------------------------------------------------------------*/ 
function NavSelect ($clickmenu){
	$url = $_SERVER['PHP_SELF'];
	$array = explode("/",$url);
	$c =count($array);
	$uri = $c-1; //last array
	 
	 if($array[$uri]==$clickmenu ){ 
	   return  'class="selected"' ;
	 }else{
	   return false; 
	 }
}
 
 
  /*------ Copy & Create Resize image    ----------------------------------------------------------------------------------------------*/  
    function  ResizePic( $photo_n ,  $fileupload_tmp_n  ,  $fileupload_n  ){
			$images = $fileupload_tmp_n  ; 
			//$new_images = "trumb_".$_FILES["fileupload"]["name"];
			$new_images =  $photo_n ;
			copy($fileupload_tmp_n , "attach/".$fileupload_n ) ;
			 
			$width = 620; // fix width
			$size = getimagesize($images);//array
			$height = round($width*$size[1]/$size[0]); // get new width 
			$images_orig = imagecreatefromjpeg($images);
			$photox =  imagesx($images_orig); 
			$photoy =  imagesy($images_orig);
			$images_fin = imagecreatetruecolor($width, $height);
			imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photox, $photoy); 
			imagejpeg($images_fin, "attach/".$new_images);
			imagedestroy($images_orig);
			imagedestroy($images_fin);   
	} 
	
	
 /*------ Check device  iPad iPhone iPod Andriod ----------------------------------------------------------------------------------------------*/ 
function  ChecKDevice( ) {
    $browserFrom = $_SERVER['HTTP_USER_AGENT']; 
	if( strstr($browserFrom , 'iPhone' )  ||  strstr($browserFrom , 'iPad' )   ||   strstr($browserFrom , 'iPod' )     ||    strstr($browserFrom  , 'Android' )       ){
	return 'true'; 
	}else{
	return 'false'; 	
	}
} 

 /*------ Check  browser Chrome ----------------------------------------------------------------------------------------------*/ 
function  is_chrome( ) {
    $txt = $_SERVER['HTTP_USER_AGENT']; 
	if( strstr($txt  , 'Chrome'  )   ){
	return 'true'; 
	}else{
	return 'false'; 	
	}
}
 
 
  /*------ Get Path ----------------------------------------------------------------------------------------------*/ 
function  getPath( ) {
$pathType =  gettype($_SERVER['QUERY_STRING']) ;
$pathQuery =  $_SERVER['QUERY_STRING']; 
$pathSelf  =  $_SERVER['PHP_SELF'];
$pathSelf  =  $_SERVER['DOCUMENT_ROOT'];
$pathSCRIPT_NAME = $_SERVER['SCRIPT_NAME'];
$pathHTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
$pathREQUEST_URI = $_SERVER['REQUEST_URI'];
$pathHTTP_HOST = $_SERVER['HTTP_HOST'];

echo $pathHTTP_HOST ;
 
//return $pathName  ; 
 
}
	
 /*------ AutoLink Youtube  ----------------------------------------------------------------------------------------------*/ 	
	
$youtubeLink1  = "http://youtu.be/zul8ACjZI18";
$youtubeLink2  = "http://www.youtube.com/watch?v=zul8ACjZI18&feature=related";  
	
function AutoLink($string){
	// force http: on www.
	$string = str_replace( "www.", "http://www.", $string );
	// eliminate duplicates after force
	$string = str_replace( "http://http://www.", "http://www.", $string );
	$string = str_replace( "https://http://www.", "https://www.", $string ); 
	// The Regular Expression filter
	$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	// Check if there is a url in the text

	$m = preg_match_all($reg_exUrl, $string, $match); 
	
	if ($m) { 
		$links=$match[0]; 
		for ($j=0;$j<$m;$j++) {  
			if(substr($links[$j], 0, 18) == 'http://www.youtube'){ 
			$string=str_replace($links[$j],'<a href="'.$links[$j].'" rel="nofollow" target="_blank">'.$links[$j].'</a>',$string).'<br /><iframe title="YouTube video player" class="youtube-player" type="text/html" width="320" height="185" src="http://www.youtube.com/embed/'.substr($links[$j], -11).'" frameborder="0" allowFullScreen></iframe><br />';
			}else{ 
			$string=str_replace($links[$j],'<a href="'.$links[$j].'" rel="nofollow" target="_blank">'.$links[$j].'</a>',$string); 
			}  
		} //end for ($j=0;$j<$m;$j++)
		
	} //end if ($m)  

   return ($string);
} 
?> 