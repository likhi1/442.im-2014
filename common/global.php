﻿<?php
/*------ Set time Zone  ---------------------------------------------------------------------------------------------*/
date_default_timezone_set('Asia/Bangkok'); 
  
//echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'; 

/*------ Thai mounth ----------------------------------------------------------------------------------------------*/
function  dateThai($strDate) { 
$strYear = date("Y",strtotime($strDate))+543; 
$strYear  = substr($strYear, -2); // <= option  

$strMonth= date("n",strtotime($strDate));
$strDay= date("j",strtotime($strDate));
$strHour= date("H",strtotime($strDate));
$strMinute= date("i",strtotime($strDate));
$strSeconds= date("s",strtotime($strDate));
$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
$strMonthThai=$strMonthCut[$strMonth];
//return "$strDay$strMonthThai$strYear ($strHour:$strMinute) ";
return "$strDay$strMonthThai$strYear $strHour:$strMinute น."; 
}

/*------ Thai mounth ----------------------------------------------------------------------------------------------*/
function setIdAddDb( ){ 
		$y  =  date('Y')+543 ; 
		$yNew = substr($y,2,2); 
		$datetime = date('md-his');
		$numberRun  =  $yNew.$datetime ; 
		//echo $numberRun ; 
		return   $numberRun;
		
		/*
		$majorKey1  =  substr( (date("Y") ) , 2,2 )  ; // +543  Thai Year
		$majorKey2  = date("m") ; 
		$num = '';
		$num = sprintf("%04s", $num+1);
		$LiveMatchId  =  $majorKey1.$majorKey2.$num;
		*/		 
}

/*------------ Captcha Eng --------------------------------------------------------------------------------*/	 
 function ranDomStr($length){ 
		$str2ran = '0123456789'; // string   for   random 
		$str_result = "";  //decare var
		while(strlen($str_result)<$length){  //loop while  strlen($str_result) < leangth
			$str_result .= substr($str2ran,(rand()%strlen($str2ran)),1); // join string by  condition 
			//ต่อ string จาก substring ที่ได้จากการ random ตำแหน่ง ทีละ 1 ตัว  จนกว่าจะครบตรามความยาวที่ส่งมา
		}
		return($str_result);//return value out of function
}
 
/*------------ Captcha Thai ---------------------------------------------------------------------------------*/	 
//-------set  random string -----------------------//
function random_password($length,$validchars) {
	$numchars = mb_strlen ($validchars,'utf-8');
	$password = '';

	// each loop random 1 character
	for ($i = 0; $i < $length; $i++) {
		mt_srand();
		// random index of valid characters
		$index = mt_rand(0, $numchars - 1); 
		// get character at index and append to password
		$password .= mb_substr($validchars, $index, 1,'utf-8');
		if(mb_strlen($password,'utf-8')==$length){
			break;	
		}
	}
	return $password;
}

//------- set image captcha -----------------------//
function imgCode($code){
		if($code!=""){ 
	            // set var 
				$font = '../class/captcha_thai/PSL-Doungkamol.ttf';
				$font_size =26;
				$string = $code; // Random String 
				 // set  Text color &  BG
				$im = imagecreatefromjpeg("../class/captcha_thai/bg.jpg"); // Path From Upload Temp
				$color = imagecolorallocate($im, 0, 0, 0); // Color  Back 
				
				// set Position 
				$pxX =   5; //$pxX =   imagesx($im) /2 ;
				$pxY = 20; 
				imagettftext($im, $font_size, 0, $pxX, $pxY, $color, $font, $string);
				$file_path = "../class/captcha_thai/test.jpg";  
				imagepng($im,$file_path);
				imagedestroy($im); 
				
				 return '<img src="../class/captcha_thai/test.jpg">';
				 
		}else{
			 
			return "you  not set  Random String  ";
		}
}

/*------------ set defalt photo  when no pic ----------------------------------------------------*/	
 function  setDefaltPhoto($fileName , $photoSize='' , $dir ){
	  
	   $opDir = opendir($dir); 
	   
    	while(  false!==   ( $fileName = readdir($opDir ) )   ){
			$file[] = $fileName ; 
		}
		
		//$count  = count($file[] ) ;  
		
		if( $count <1 ){
			switch($photoSize ){
			   case "trumbBig"    : $pic = '<img  src="../images/update2012/defalt_photo_big.jpg">' ;  break  ;
			   case "trumbSmall" : $pic =  '<img  src="../images/update2012/defalt_photo_small.jpg">' ; break  ;
			   default  : "กำหนดขนาดภาพ ที่ไม่มีอยู่" ; 
		   }//end switch  
		} //end if 
		
	   return  $pic ;
} 
 

/*------------ Limit show String with ...   ----------------------------------------------------*/ 
function cutStr($str, $maxChars='', $holder=''){ 
    if (strlen($str) > $maxChars ){
			$str = iconv_substr($str, 0, $maxChars,"UTF-8") . $holder;
	} 
	return $str;
} 


/*------------ Split URI (For check) ----------------------------------------------------*/	
 function  splitUri($uri){ 
	$arrUri = explode('/' , $uri);
	$arrNum  = count($arrUri) - 1 ; //get last array 
	return  $arrUri[$arrNum]   ; 
}


  
 
 /*------------ Set Category of News ----------------------------------------------------*/	
 function  selectCateNews($cat){ 
     switch($cat){
		case 1: $catType =  'ข่าวบอล' ;break;
	    case 2: $catType =  'วิเคราะห์บอล' ;break; 
		case 3: $catType =  'ข่าวทั่วไป' ;break; 
		case 4: $catType =  'โปรโมชั่น&วิธีการพนัน'  ;break;
		case 5: $catType =  'ทำSEO' ;break;
		default: $catType = 'ไม่มีหมวดนี้';  
	 }
	 return  $catType   ; 
} 
  
 /*------------ Set Category of leage football  ----------------------------------------------------*/	
 function selectCatLeage($cat){
	switch($cat){
		case 1: $catType = 'Premier League ' ; break;
		case 2: $catType = 'La Liga League'; break;
		case 3:  $catType ='Thailand Premier League ' ; break;
		case 4 :  $catType ='Bundesliga League '; break;
		default : $catyType = 'ไม่มี Leage'; 
	}
	 return $catType ;
 }
 
   
  /*------------ Set  Default   Meta Tag  For SEO     ----------------------------------------------------*/	 
  
  function checkMeta( $val , $fieldName ){
			if( empty($val) ){
				
				$title = "แทงบอลออนไลน์ผ่านเน็ต พนันบอลออนไลน์ แทงสเต็ปบอล Sbobet Ibcbet เว็บเล่นบอลออนไลน์ บาคาร่าออนไลน์ จีคลับ";
				$keywords  =  "แทงบอลออนไลน์, แทงบอลผ่านเน็ต,  เล่นบอลออนไลน์, เล่นบอลผ่านเน็ต, แทงบอล, เล่นบอล, เว็บบอล, ผลบอล, ผลบอลสด,วิเคราะห์บอล, แทงบาคาร่า,บอลสเต็ป, ผลบอลออนไลน์, ตารางบอล,  sbo, sbobet,  ช่องทางเข้าsbobet, ช่องทางเข้าsbo,เข้าsbobetไม่ได้, แทงบาส, แทงบอลบาคาร่า,    แทงสเต็ป, สเต็ปบอล, casino, bacara, baccarat, casino online, poker, blackjack, slot,holiday,7m,  ibc, ibcbet,  holiday, palace,   g club,  บาคาร่า, บาคาร่าออนไลน์,  คาสิโนออนไลน์  , livescore  , ผลบอลเมื่อคืน  ,  ผลฟุตบอล  ,  ผลบอลพรีเมียร์ลีก   , ผลฟุตบอลพรีเมียร์ลีก  ,  วิเคราะห์บอล วิเคราะห์บอลวันนี้   , วิเคราะห์ฟุตบอล   , ฟุตบอล ,   บอลวันนี้   , ทีเด็ดฟุตบอล  ,  ทีเด็ดฟุตบอลคืนนี้  ,  ทีเด็ดฟุตบอลวันนี้  ,  ฟุตบอลวันนี้   , โปรแกรมฟุตบอล ฟุตบอลพรีเมียร์ลีก  ,  ข่าวฟุตบอล  ,  คลิบฟุตบอล ,  เกมส์ฟุตบอล" ;
			   $description  = "เว็บโปรโมชั่นบอล แทงบอลออนไลน์ผ่านเน็ต พนันบอลออนไลน์ แทงสเต็ปบอล พร้อมช่องทางเข้า SbobetและIbcbet" ;
				
				switch($fieldName){ 
					case "title" 				:	 $valNew =  $title				; break ;
					case "keywords"		:	 $valNew  =  $keywords		; break ;
					case  "description"	: 	 $valNew	=  $description	; break ;
					default : "ค่าที่ตรง $fieldName ไม่มี";
				} //end switch  
				
				return    $valNew  ; 
    }else{ 
		        return    $val; 
	}
	
}

 
 
/*------------ Method Date Time ----------------------------------------------------*/	
//Date Time Formate from yyyy-mm-dd h:i:s example : 2008-10-30 12:30:00  
	
 $shortMonth=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
 $longMonth=array("January","February","March","April","May","June","July","August","September","October","November","December");
 $thaiMonth = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
 $engDay =array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
 	
	function convert2Timestamp($txt) { 
		$D=substr($txt,8,2);
		$M=substr($txt,5,2);
		$Y=substr($txt,0,4);
		$H=substr($txt,11,2);
		$I=substr($txt,14,2);
		$S=substr($txt,17,2); 
		return mktime($H,$I,$S,$M,$D,$Y);
	}	
			  
	 function shortDate($txt) {
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year;
	}	
	  

	function shortDateTime($txt){
		$Year=substr($txt,0,4);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,8);
		$Month=$Month-1;
		return $this->shortMonth[$Month]." ".$DayNo.", ".$Year." ".$Time;
	}	

 function shortThaiDate($txt) {
		$thaiMonth = array( "ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$Year=  substr($txt,0,4)+543   ;
		$Month= intval( substr($txt,5,2) ) ;   //   intval() Get the integer value of a variable 
		 $ThaiMonthNew = $thaiMonth[$Month]; 
		 $DayNo=  intval(  substr($txt,8,2)   ) ; 
		
		   return $DayNo." ". $ThaiMonthNew." ". $Year ;
		// return $txt; // check  format of DateTime
	}	

   function shortThaiDateTime($txt){
		$Year=  substr(substr($txt,0,4)+543   ,  2);
		$Month=substr($txt,5,2);
		$DayNo=substr($txt,8,2);
		$Time=substr($txt,11,5);
		$Month=$Month-1; 
		 return $DayNo." ". $Month." ". $Year." - ".$Time; 
 
       // return $txt; // check  format of DateTime
	}	
	

 /*------ Resize Photo (Fake)  -------------------------------------------------------------------------------------------*/
function resize($quiz_attach  ){
 
 $propertyImage =	getimagesize( "attach/$quiz_attach"  );	

 //print_r($propertyImage) ;
		if ($propertyImage[0]>630 ){
		    $w= 630;
			$h=(630*$propertyImage[1])/$propertyImage[0] ;
		}else{
			$w= $propertyImage[0];
			$h= $propertyImage[1];
		}	
		$output  =  '<img src="attach/'.$quiz_attach.'" width="'.$w.'"  height="'.$h.'"  />' ;
		echo  $output ;
		//echo "<hr>";
		return $output ;
 }
 
 /*------  Bread Crumb  --------------------------------------------------------------------------------------------------------*/ 
function breadCrumb(){   
$uri  =  explode(  '/' , $_SERVER['PHP_SELF'] )  ; 
//print_r( $uri  );  
$uriNum   = count($uri )- 1;  // last array  
//$uriNum   = count($uri )- 2;   //  folder of file 
echo   $uri[$uriNum]; 
	
}

 

 /*------ make nav active menu    ----------------------------------------------------------------------------------------------*/ 
function navSelect ($clickmenu){
	$url = $_SERVER['PHP_SELF'];
	$array = explode("/",$url);
	$c =count($array);
	$uri = $c-1; //last array
	 
	 if($array[$uri]==$clickmenu ){ 
	   return  'class="selected"' ;
	 }else{
	   return false; 
	 }
}
  
 /*------ Check device  iPad iPhone iPod Andriod ----------------------------------------------------------------------------------------------*/ 
function  checkHTML5Support( ) {
    $browserVer = $_SERVER['HTTP_USER_AGENT']; 
	if(   strstr($browserVer , 'MSIE 6.0' )  ||  strstr($browserVer , 'MSIE 7.0' )   ||  strstr($browserVer , 'MSIE 8.0' )  ||  strstr($browserVer , 'iPad' )  ||  strstr($browserVer , 'iPhone' )     ){
	return 'true'; 
	}else{
	return 'false'; 	
	}
} 	
	
 /*------ Check device  iPad iPhone iPod Andriod ----------------------------------------------------------------------------------------------*/ 
function  ChecKDevice( ) {
    $browserFrom = $_SERVER['HTTP_USER_AGENT']; 
	if( strstr($browserFrom , 'iPhone' )  ||  strstr($browserFrom , 'iPad' )   ||   strstr($browserFrom , 'iPod' )     ||    strstr($browserFrom  , 'Android' )       ){
	return 'true'; 
	}else{
	return 'false'; 	
	}
} 

 /*------ Check  browser Chrome ----------------------------------------------------------------------------------------------*/ 
function  is_chrome( ) {
    $txt = $_SERVER['HTTP_USER_AGENT']; 
	if( strstr($txt  , 'Chrome'  )   ){
	return 'true'; 
	}else{
	return 'false'; 	
	}
}






















 
 
  /*------ Get Path ----------------------------------------------------------------------------------------------*/ 
function  getPath( ) {
$pathType =  gettype($_SERVER['QUERY_STRING']) ;
$pathQuery =  $_SERVER['QUERY_STRING']; 
$pathSelf  =  $_SERVER['PHP_SELF'];
$pathSelf  =  $_SERVER['DOCUMENT_ROOT'];
$pathSCRIPT_NAME = $_SERVER['SCRIPT_NAME'];
$pathHTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
$pathREQUEST_URI = $_SERVER['REQUEST_URI'];
$pathHTTP_HOST = $_SERVER['HTTP_HOST'];

echo $pathHTTP_HOST ;
 
//return $pathName  ; 
 
}

 
  /*------  Resize image   & Upload   ----------------------------------------------------------------------------------------------*/  
    function  resizePic(  $newName ,  $oriName , $tmpName , $lastName   ){   // ResizePic( $photo_n ,  $fileupload_tmp_n  ,  $fileupload_n   )  
		     
			 //--- set var  ------------------// 
			$pathUpServer =   "../upload/"   ;  
			copy($tmpName  , $pathUpServer.$oriName ) ;  
			$size = getimagesize($tmpName);//array 
 
            if($size[0]> 620 ){ // check defalt width
				$width = 620; 
			    $height = round($width*$size[1]/$size[0]); // get new width   
			}else{
				$width  = $size[0] ; 
				$height = $size[1]; 
			}
			
				if($lastName == "jpg"  || $lastName == "jpeg"   ){ // create by check extention
					   $images_ori  = imagecreatefromjpeg($tmpName);
					}else{
					   $images_ori  = imagecreatefromgif($tmpName);
					}
				$photox =  imagesx($images_ori ); 
				$photoy =  imagesy($images_ori );
				$images_fin = imagecreatetruecolor($width, $height);
				imagecopyresampled($images_fin, $images_ori , 0, 0, 0, 0, $width+1, $height+1, $photox, $photoy); 
				imagejpeg($images_fin,  $pathUpServer. $newName);
				imagedestroy($images_ori );
				imagedestroy($images_fin); 
			
			unlink($pathUpServer.$oriName);   //  Delete Original  File
	} 
	
 /*------ AutoLink Youtube  ----------------------------------------------------------------------------------------------*/ 	 
$youtubeLink1  = "http://youtu.be/zul8ACjZI18";
$youtubeLink2  = "http://www.youtube.com/watch?v=zul8ACjZI18&feature=related";  
	
function autoLink($string){
	// force http: on www.
	$string = str_replace( "www.", "http://www.", $string );
	// eliminate duplicates after force
	$string = str_replace( "http://http://www.", "http://www.", $string );
	$string = str_replace( "https://http://www.", "https://www.", $string ); 
	// The Regular Expression filter
	$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	// Check if there is a url in the text

	$m = preg_match_all($reg_exUrl, $string, $match); 
	
	if ($m) { 
		$links=$match[0]; 
		for ($j=0;$j<$m;$j++) {  
			if(substr($links[$j], 0, 18) == 'http://www.youtube'){ 
			$string=str_replace($links[$j],'<a href="'.$links[$j].'" rel="nofollow" target="_blank">'.$links[$j].'</a>',$string).'<br /><iframe title="YouTube video player" class="youtube-player" type="text/html" width="320" height="185" src="http://www.youtube.com/embed/'.substr($links[$j], -11).'" frameborder="0" allowFullScreen></iframe><br />';
			}else{ 
			$string=str_replace($links[$j],'<a href="'.$links[$j].'" rel="nofollow" target="_blank">'.$links[$j].'</a>',$string); 
			}  
		} //end for ($j=0;$j<$m;$j++)
		
	} //end if ($m)  

   return ($string);
} 
?>