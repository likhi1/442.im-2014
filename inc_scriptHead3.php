 <!------  Base  ------------------------------------------>
<link href="<?php echo $base_url  ?>css/global.css" rel="stylesheet" media="all" />  
<script src="<?php echo $base_url   ?>Scripts/swfobject_modified.js" type="text/javascript"></script>
<script src="<?php echo $base_url   ?>js/442js/jquery-1.9.1.js" type="text/javascript"></script> 

<!--  Custom Script ---------------------------------------->  
<script type="text/javascript" src="<?php echo $base_url   ?>js/442js/_jsLibrary.js"></script> 
<script type="text/javascript" src="<?php echo $base_url   ?>js/442js/customValidate.js"></script> 

<!--  JqueryUI Modal   ---------------------------------------->
<link href="<?php echo $base_url  ?>js/442js/jquery-ui-1.10.4.custom.css" rel="stylesheet" media="all" /> 
<script src="<?php echo $base_url   ?>js/442js/jquery-ui-1.10.3.js" type="text/javascript"></script> 
 <script src="<?php echo $base_url   ?>js/442js/config-jqueryUiModal3.js" type="text/javascript"></script>   
   
<!--  JqueryPlugin  BXSlide  ---------------------------------------->
<link href="<?php echo $base_url  ?>js/442js/jquery.bxslider.css" rel="stylesheet" media="all" />   
<script src="<?php echo $base_url   ?>js/442js/youtubeAPI.js" type="text/javascript"></script><!--  For use YouTube video in BXSlide -->
<script src="<?php echo $base_url   ?>js/442js/bxslider.js" type="text/javascript"></script>
 <script src="<?php echo $base_url   ?>js/442js/config-Bxslide.js" type="text/javascript"></script> 
    
<!--  Override StylSheet  ----------------------------------------> 
<link href="<?php echo $base_url  ?>js/442js/Override-Jquery-UI.css" rel="stylesheet" media="all" />   
     
 
<script> 
$(function () {  

    /////// Set Preload Images
 
    PreLoadImages(

    				"<?php echo $base_url   ?>images/update2014/442tel.png"  ,  
    		       "<?php echo $base_url   ?>images/update2014/442_LogoWeb.png" ,  
    		       "<?php echo $base_url   ?>images/update2012/btn-join_now.gif" ,
    		       "<?php echo $base_url   ?>images/fontend/buttonDialogClose.png" ,
    		       "<?php echo $base_url   ?>images/update2014/liveChatButton.png"  ,

				   "<?php echo $base_url   ?>images/banner/popupTor-Small.png"  ,
				   "<?php echo $base_url   ?>images/banner/442big_bonus_01.gif"  , 
    		       "<?php echo $base_url   ?>images/keyWeb/Premier-League/Premier-League-Open.jpg"  ,
    	          "<?php echo $base_url   ?>images/keyWeb/Casino/Casino1.jpg"  ,
                  "<?php echo $base_url   ?>images/keyWeb/Casino/Casino2.jpg" 
     ); 
 
    
	/////// Set  JqueryUI Modal Set 
 
	  DialogLiveChat(false, false, "#dialogLiveChat", '#openDialogLiveChat1', 300, 600, "right top" , "right bottom" , window , "fixed-dialog" );
	 DialogLiveChat(false, false, "#dialogLiveChat", '#openDialogLiveChat2', 300, 600, "right center" , "right center" , window , "fixed-dialog" );
 
	DialogBasic(false, true, "#dialogOTP", '#openDialogOTP', 350, 600, "center", "top-100");  
	DialogBasic(false, true, "#dialogLine", '#openDialogLine', 250, 250, "center", "center"); 
	DialogBasic(true, false, "#dialogBannerFloat", null, 300, 470,  "left  Bottom" ,   "left  Bottom"  ,  "#bxslider1"  );
	DialogBasic(false, true, "#modalOpenAccount", '#btnOpenAccount1', 530, 320);
	DialogBasic(false, true, "#modalOpenAccount", '#btnOpenAccount2', 530, 320); 

 
 
}); //endReady

</script>
 
 
 
   

 