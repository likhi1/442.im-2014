<?php
include_once('inc_chk_no_have_sess.php'); 
require_once('connectCms/connect_db_im_cms.php'); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/global_backend.css" type="text/css" rel="stylesheet" />
<title>Administration Login</title>
<?php  include_once('inc_script_header.php'); ?> 
</head>

<body>
<div  id="container">  
<? include_once('inc_navigation.php');?>
<div id="content"> 

 
<div class="breadCrumb">
<a href="<?php  echo  $base_url ;?>/adminweb/">Home</a>   
 </div>  <!--   end  class="breadCrumb" --></strong>
 
 
<!-- Start  CK Finder------------------------------------------------------------------------------------->	
<script type="text/javascript" src="../ckfinder/ckfinder.js"></script> 
<script type="text/javascript"> 
//--------- show info image  ---------------//  
function showFileInfo( fileUrl, data ) {
	var msg = 'The selected URL is: <a href="' + fileUrl + '">' + fileUrl + '</a><br /><br />'; 
	if ( fileUrl != data['fileUrl'] )
	msg += '<b>File url:</b> ' + data['fileUrl'] + '<br />';
	msg += '<b>File size:</b> ' + data['fileSize'] + 'KB<br />';
	msg += '<b>Last modifed:</b> ' + data['fileDate']; 
	this.openMsgDialog( "Selected file", msg );
}

//--------- Set  "CKFinder" class ---------------//  
var finder = new CKFinder(); 
finder.basePath = '../'; 	// The path for the installation of CKFinder (default = "/ckfinder/").
finder.selectActionFunction = showFileInfo;
finder.create(); 
</script>
<!-- End  CK Finder------------------------------------------------------------------------------------->


</div><!--  end  id="content" --->
 
<div class="clear"></div>
<? include_once('inc_footer.php');?>
 <?php //print_r($_SESSION); ?>
</div><!--  end  id="container" -->


 <iframe src="" id="savetarget" name="savetarget" style=" display:none; width:800px;"></iframe>

</body>
</html>
