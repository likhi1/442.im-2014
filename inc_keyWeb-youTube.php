 <script>
//////========================  Global Var
// Global Var - Youtube API
var areaShowVideo;
var videoId;
var ytPlayer;
// Global Var - BxSlide
var selectorSlide;
var objSlider; // set  globalVarible from  setBxSlider()
var numSlide;
var lengthImage;
var lengthVideo;
var currentSlide;
var slideStartAuto; 
/////========================   Run After Youtube API Loaded
//    function onYouTubeIframeAPIReady() {
//
//    }

////==========================  Youtube API
function setYouTubeVideo( _areaShowVideo ,  _videoId) { 
	ytPlayer = new YT.Player(_areaShowVideo, {
		  width: 992,
		 height: 464,
		frameborder: 0,
		playerVars: { 'autoplay': 0, 'controls': 0, modestbranding: 1, rel: 0, showinfo: 0, enablejsapi: 1, version: 3 },
		videoId: _videoId,
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange 
		}
	});
} //end fnc
 
function onPlayerReady(event) { 
	event.target.playVideo();
}

// var done = false;
function onPlayerStateChange(event) {
	//console.log(" >>>>>> onPlayerStateChange ");
	//   console.log(ytPlayer); 
	//  if (event.data == ytPlayer.PlayerState.PLAYING && !done) {
	if (event.data === 0) {
		//console.log(" >>>>>>  onPlayerStateChange  =>  eventData =  0 ");
		event.target.destroy();
		objSlider.goToNextSlide();  // console.log("Finish Video");
	}  
} // end fnc

//////==========================  Jquery Bxslide
function setBxSlider(_selectorSlide, _slideStartAuto, _pager, _pause) {
	objSlider = _selectorSlide.bxSlider({
		mode: 'fade',  //  'horizontal', 'vertical', 'fade'
		auto: _slideStartAuto, // Set slide auto start ( If slide have Video set false ) 
		controls: false,
		pager: _pager,
		pause: _pause,
		autoHover: true,
		//video: true,
		pagerType: 'full',
		// startingSlide: 0, 
		onSliderLoad: function (currentIndex) { //  Executes  once  on first time  loaded
			//console.log(">>>>>>> onSliderLoad ");
			var startIndexSlide = currentIndex;   
			$('#bxslider1>li').eq(0).addClass('active-slide');  // Set class .active-slide  for  first  slide
			//console.log(" startIndexSlide =" + startIndexSlide); 
		}, 
		onSlideBefore: function ($slideElement, oldIndex, newIndex) { // Executes  before each slide transition.
			//console.log(">>>> onSlideBefore ");
			currentSlide = addActiveClass(oldIndex, newIndex);  // Set Class Active
			checkSlideType(); 
		}
		
	});
}

function checkSlideType() {
	//========= Is Image Slide or Video Slide
	if (!currentSlide.children().is('img')) {  //&& (currentSlide.attr('class') == "active-slide")
		objSlider.stopAuto();
		//console.log("=== SlideVideo ===");
		objSlider.stopAuto();  // Stop BxSlide Play   

		areaShowVideo = currentSlide.children().attr('id');
		//console.log("areaShowVideo = " + areaShowVideo);

		var strVideoId = currentSlide.children().attr("href");
		videoId = (strVideoId.replace("http://www.youtube.com/embed/", "")).substring(0, 11);
		//console.log("videoId = " + videoId);

		//============ Create instance  of  Youtube API
		setYouTubeVideo(areaShowVideo, videoId);
		 
	} else {
		objSlider.startAuto();   // Start BxSlide Play   
		//console.log("=== SlideImage ===");
	}
}

////============ Add class active to BxSlide
function addActiveClass(_oldIndex, _newIndex) {
	
	//=======  Add .active-slide
	$('#bxslider1>li').removeClass('active-slide');

	if ((_newIndex) >= numSlide)  // protect last slide
		$('#bxslider1>li').eq(0).addClass('active-slide');
	else
		$('#bxslider1>li').eq(_newIndex).addClass('active-slide');

	//========= Get videoId & areaShowVideo
	currentSlide = $('#bxslider1 .active-slide');

	//======== Test Show Index by Jquery
	//alert($('#bxslider1 li.active-slide').index()) ; 

	//console.log("lengthVideo = " + lengthVideo);
	//console.log("newIndex = " + _newIndex);
	
	return currentSlide;

} //end fnc


////////=========== Dom Ready  ( This Run After Dom Create )
$(function () {
	/*
	 * If define varible from sector in Jquery  have to  set varible after dom create !!!!!!
	*
	*/
	selectorSlide = $("#bxslider1"); //  Set varible from sector after dom create
	numSlide = $('#bxslider1>li').size();
	lengthImage = $('#bxslider1>li').children('img').size();
	lengthVideo = numSlide - lengthImage;
	
	//====== Set  BxSlide Load
	setBxSlider(selectorSlide, true, true, 3000); //5500
}); 

</script> 

<div class="demo-wrap">
<ul id="bxslider1">

 <li><img src="images/keyWeb/Premier-League/Premier-League-Open.jpg" /></li> 
 
<li><img src="images/keyWeb/Premier-League/Premier-League-01.jpg" /></li>  
<li><img src="images/keyWeb/Premier-League/Premier-League-02.jpg" /></li>  
<li><img src="images/keyWeb/Premier-League/Premier-League-03.jpg" /></li>
<li><img src="images/keyWeb/Premier-League/Premier-League-04.jpg" /></li>
 

 <li><img src="images/keyWeb/Casino/Casino1.jpg" /></li>    
 <li><img src="images/keyWeb/Casino/Casino3.jpg" /></li>  
<li>
<a id="video1" href="http://www.youtube.com/embed/2ElxeOmqWnk?controls=0&modestbranding=1&rel=0&showinfo=0"></a>
</li>
<li>
<a id="video2" href="http://www.youtube.com/embed/mV3L8e355d8?controls=0&modestbranding=1&rel=0&showinfo=0"></a>
</li>
 
</ul>
</div>





