$(document).ready(function () {
    $(".tabs").tabs();
    $("#tabs-rotate").tabs({
        fx: {
            opacity: 'toggle'
        }
    }).tabs('rotate', 100);
    $('#tabs-rotate').hover(function () {
        $(this).tabs('rotate', 0, false);
    }, function () {
        $(this).tabs({
            fx: {
                opacity: 'toggle'
            }
        }).tabs('rotate', 100);
    });
});