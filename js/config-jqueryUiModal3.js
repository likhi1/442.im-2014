﻿
function DialogBasic( isOpen ,  isModal,  dialogContentId, dialogButtonId, widthSize, heightSize, positionX, positionY  , targetElement ,  addClass ) {
    //========  Jquery UI-Dialog  LiveChat
    jQuery(dialogContentId).dialog({
        show: "fade",
        hide: "fade",
        autoOpen: isOpen,
        dialogClass: addClass , // Fix position to bottom
        modal: isModal,
        width: widthSize,
        height : heightSize ,  
        //draggable: false ,
        rsesizable: false,    // option   
        position: { my: positionX, at: positionY , of: targetElement },
        //position: { my: "right  top", at: "right   bottom", of: window },
        open: function () { // bind close when click overlay 
            jQuery(dialogContentId + " .buttonDialogClose").bind('click', function () {
                jQuery(dialogContentId).dialog('close'); 
            })
        }   
      
    });
  
    ////// command event  dialog
    $(dialogButtonId).click(function (e) {
        //console.log(dialogButtonId+" Clicked");
        e.preventDefault();
      
        if (dialogButtonId == "#openDialogOTP" ){  //////  Set  src of DialogOTP
        	
						var  otpApp  =  'http://apps.4-4-2.im.1327soon2b.co.uk/CashCredit/Module/Clogin.aspx?telno=' ; 
						var otpTelno  = $("#telnoOTP").val() ; 
						
						 if ($("#telnoOTP").val() != "" &&  $("#telnoOTP").val() != null) { 
							 	 $("#dialogOTP iframe").attr('src', otpApp+otpTelno  ) ;   
						} else { 
								alert("กรุณากรอกข้อมูลให้ครบและถูกต้อง"); 
								return false ;
						}
        	    
        } 
        
		$(dialogContentId).dialog('open'); 
       
    }); // end click
    
    /////// Config UI All 
    $(".ui-dialog-titlebar").hide();  
   

} // end function DialogBasic
 
 
 
 
 
 
 
 
 
function DialogLiveChat( isOpen ,  isModal,  dialogContentId, dialogButtonId, widthSize, heightSize, positionX, positionY  , targetElement ,  addClass ) {

	var  btnLiveChat2 = $("#openDialogLiveChat2") ;

    //========  Jquery UI-Dialog  LiveChat
    jQuery(dialogContentId).dialog({
        show: {  effect: "drop",  direction: "up" ,  duration: 1000   },  
        hide: {  effect: "drop",  direction: "up" , duration: 1000   },  
        autoOpen: isOpen,
        dialogClass: addClass , // Fix position to bottom
        modal: isModal,
        width: widthSize,
        height : heightSize ,  
        //draggable: false ,
        rsesizable: false,    // option   
        position: { my: positionX, at: positionY , of: targetElement },
        //position: { my: "right  top", at: "right   bottom", of: window },
        open: function () { // bind close when click overlay 
            jQuery(dialogContentId + " .buttonDialogClose").bind('click', function () {
                jQuery(dialogContentId).dialog('close');  
            	btnLiveChat2.slideDown(); 
            })
        }   
		
		
		
      
    }); 
  
    ////// command event  dialog
    $(dialogButtonId).click(function (e) { 
        e.preventDefault();
      
        //if(dialogButtonId=="#openDialogLiveChat1" || dialogButtonId=="#openDialogLiveChat2"  ){  //////  Check hide buttonLiveChat
        //	btnLiveChat2.slideDown();  
        //}  
		     btnLiveChat2.slideUp(); 
        	 $(dialogContentId).dialog('open'); 
       
    }); // end click
    
    /////// Config UI All  
    $(".ui-dialog-titlebar").hide();  

} // end function DialogLiveChat
 