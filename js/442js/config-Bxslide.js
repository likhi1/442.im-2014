
//////========================  Global Var ===================/////
//////// BxSlide & Youtube Varible
var areaShowVideo;
var videoId;
var bxslider1ViewportSize;
//var ytPlayer;  
var objSlider; //  Control BxSlide  start & stop
var currentSlide ;

  
//////===================  BxSlide  Basic =========================/////
function hideBxSlider(_elmId1) {
    _elmId1.parent().hide();
}


function bxSliderBasic(_selectorSlide, _pager, _pause) {
    _selectorSlide.bxSlider({
        mode: 'fade',  //  'horizontal', 'vertical', 'fade'
        auto: true,
        controls: false,
        pager: _pager,
        pause: _pause,
        autoHover: true, 
        pagerType: 'full',
        //startingSlide: 0 
    });
}
 
//////=================== BxSlide with YouTube =========================/////
function getViewportSize() {
    bxslider1ViewportSize = $('.bx-viewport').height();
    // bxslider1ImageHeight = $('#bxslider1 img')[0].height;  //  Size Image change  follow  #bxslider1  viewport
}


function bxSliderYoutube(_selectorSlide, _slideStartAuto, _pager, _pause) {
    objSlider = _selectorSlide.bxSlider({
        mode: 'fade',  //  'horizontal', 'vertical', 'fade'
        auto: _slideStartAuto, // Set slide auto start ( If slide have Video set false ) 
        controls: false, /// true  or false
        pager: _pager,
        pause: _pause,
        autoHover: true,
        //video: true,
        pagerType: 'full',
        // startingSlide: 0, 
        onSliderLoad: function (currentIndex) { //  Executes  once  on first time  loaded
           // console.log(">>>>>>> onSliderLoad ");
            var startIndexSlide = currentIndex;
            $('#bxslider1>li').eq(0).addClass('active-slide');  // Set class .active-slide  for  first  slide
           // console.log(" startIndexSlide =" + startIndexSlide);
        },
        onSlideBefore: function ($slideElement, oldIndex, newIndex) { // Executes  before each slide transition.
            //console.log(">>>> onSlideBefore ");
            currentSlide = addActiveClass( oldIndex, newIndex);  // Set Class Active
            checkSlideType();
        }

    });
}

////////// Add class active to BxSlide
function addActiveClass( _oldIndex, _newIndex) {
    numSlide = $('#bxslider1>li').size();

    //=======  Add .active-slide
    $('#bxslider1>li').removeClass('active-slide');
    if ((_newIndex) >= numSlide)  // protect last slide
        $('#bxslider1>li').eq(0).addClass('active-slide');
    else
        $('#bxslider1>li').eq(_newIndex).addClass('active-slide');

    //========= Get videoId & areaShowVideo
    currentSlide = $('#bxslider1 .active-slide'); 
    //console.log("newIndex = " + _newIndex);
    return currentSlide;
} //end fnc

////////// CheckSlideType Image or Video
function checkSlideType() {  
    if (!currentSlide.children().is('img')) {  //&& (currentSlide.attr('class') == "active-slide")
        objSlider.stopAuto();
        //console.log("=== SlideVideo ===");   
        areaShowVideo = currentSlide.children().attr('id');
        //console.log("areaShowVideo = " + areaShowVideo);

        var strVideoId = currentSlide.children().attr("href");
        videoId = (strVideoId.replace("http://www.youtube.com/embed/", "")).substring(0, 11);
        //console.log("videoId = " + videoId);

        //============ Create instance  of  Youtube API
        setYouTubeVideo(areaShowVideo, videoId);
    } else {
        objSlider.startAuto();   // Start BxSlide Play   
        //console.log("=== SlideImage ===");
    }
}
 
////////// Have to load youTube befor use this  Method 
function setYouTubeVideo(_areaShowVideo, _videoId) {
    getViewportSize(); //  Get  heigt form bxslider1  ViewportSize  
    //console.log(bxslider1ViewportSize);

    ytPlayer = new YT.Player(_areaShowVideo, {
        width: '100%',
        height: bxslider1ViewportSize,  // Set Responsive Video Height
        frameborder: 0,
        playerVars: { 'autoplay': 0, 'controls': 0, modestbranding: 1, rel: 0, showinfo: 0, enablejsapi: 1, version: 3 },
        videoId: _videoId,
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
} //end fnc

function onPlayerReady(event) {
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    // console.log(" >>>>>> onPlayerStateChange ");
    // console.log(ytPlayer);  
    if (event.data === 0) {  //    event.data == ytPlayer.PlayerState.PLAYING && !done
        //console.log(" >>>>>>  onPlayerStateChange  =>  eventData =  0 ");
        event.target.destroy();
        objSlider.goToNextSlide();  // console.log("Finish Video");
    }
} // end fnc
 
 
 