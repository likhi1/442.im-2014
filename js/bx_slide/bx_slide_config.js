// JavaScript Document 
 $(document).ready(function(e) { 
 
   /*---  Keyweb -------------------------------------------------*/ 
    $('#slider1').bxSlider({ 
			  mode: 'fade',  //  'horizontal', 'vertical', 'fade'
			  auto: true, controls: true, pause: 5500, autoHover:true ,pagerType :'full'
			  //startingSlide:  0  
    }); 

   /*--- Banner Side   -------------------------------------------------*/ 	
	    $('#slider2').bxSlider({ 
			  mode: 'fade',  //  'horizontal', 'vertical', 'fade'
			   auto: true, controls: false,  pause: 4500, autoHover:true  
			  //startingSlide:  0  
    }) 
  
   /*---  Banner Float   -------------------------------------------------*/ 	
    $('#slider3').bxSlider({ 
 		  mode: 'fade',  //  'horizontal', 'vertical', 'fade'
 			   auto: true, controls: false,  pause: 4500, autoHover:true  
 			  //startingSlide:  0  
     });
	
	 
}

);