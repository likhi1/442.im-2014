// JavaScript Document
$(document).ready(function () { 
	$('#navMenu li').append('<div class="hover"><\/div>'); 
	$('#navMenu li').hover( 
		function() { 
			$(this).children('div').stop(true, true).fadeIn('15000'); 
		},  
		function() { 
			$(this).children('div').stop(true, true).fadeOut('15000');	 
	}).click (function () {  
		$(this).addClass('selected'); 
	}); 
}); 