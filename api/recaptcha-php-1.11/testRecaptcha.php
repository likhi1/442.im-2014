  <html>
    <body> <!-- the body tag is required or the CAPTCHA may not show on some browsers -->
      <!-- your HTML content -->

      <form method="post" action="testRecapcha-script.php">
        <?php
          require_once('recaptchalib.php');
          $publickey = "6LeGmPQSAAAAAHRTGgXg-4XlxMJtijsKjrNfA_P9"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
        ?>
        <input type="submit" />
      </form>

      <!-- more of your HTML content -->
    </body>
  </html>