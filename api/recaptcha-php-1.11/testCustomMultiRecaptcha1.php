  <script type='text/javascript' src='//code.jquery.com/jquery-1.9.1.js'></script>
  <link rel="stylesheet" type="text/css" href="/css/result-light.css">
<script type='text/javascript' src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<script type='text/javascript'>

        $(function () {
		    /////// Cone  recaptcha
            //$('form').after($('form').clone()  );    

			////// execute reload Captcha
            function reloadCaptcha(challenge) {
                $(':input[name=recaptcha_response_field]').val(''); // clear text in input befor reload
                $(':input[name=recaptcha_challenge_field]').val(challenge); // Set  challenge
                $('img.recaptcha').attr('src', '//www.google.com/recaptcha/api/image?c=' + challenge); // Set image recaptcha
				console.log( challenge);
            }
			/////  execute   after recaptcha reload
            Recaptcha.finish_reload = function (challenge, b, c) {
                reloadCaptcha(challenge);
            }
			
			//////  execute callback
            Recaptcha.challenge_callback = function () {
                reloadCaptcha(RecaptchaState.challenge); 
            }

			///// create recaptcha
            Recaptcha.create("6LeGmPQSAAAAAHRTGgXg-4XlxMJtijsKjrNfA_P9");  // plublic key

			///// event  on get   another recaptcha
            $(document).on('click', '.reload_captcha', function (e) {
                e.preventDefault();
                Recaptcha.reload();
            });

        });//  ready  end 
    </script>
	
</head>
<body>
  <form>
    <input type="hidden" name="recaptcha_challenge_field" value="" />
    <input type="text" name="recaptcha_response_field" placeholder="Enter the Words Below" required/>
    <br>
    <img class="recaptcha" height="57" width="300" src="">
    <br>	<a href="#" class="reload_captcha">Get another CAPTCHA</a>
    <br>
    <button>Submit</button>
</form>
