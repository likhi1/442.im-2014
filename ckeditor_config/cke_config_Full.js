﻿// JavaScript Document
var cke_config={
	
	//------ Set Config Gernaral -----------------------------------// 
	skin:'v2', // kama | office2003 | v2
	//uiColor :'#000', // ใช้กับ  skin kama
	language:'th', // th / en and more..... 
	enterMode: 2,// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div> 
	shiftEnterMode	:1,// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>	
	height :400, // กำหนดความสูง
	width :510, // กำ//หนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
	removePlugins : 'elementspath' , //remove show path
	
	//------ Set Plugin  -----------------------------------// 
	 extraPlugins : 'autogrow', // เรียกใช้ plugin ให้สามารถขยายขนาดความสูงตามเนื้อหาข้อมูล
     extraPlugins : 'pastetext',
	 autoGrow_maxHeight : 600,	 // กำหนดความสูงตามเนื้อหาสูงสุด ถ้าเนื้อหาสูงกว่า จะแสดง scrollbar
	 forcePasteAsPlainText : true,
	
	//------ Set Config Advance -----------------------------------// 
	//entities : false,
	// forceEnterMode : true,
	// autoParagraph :2,
	//  FillEmptyBlocks : true,
	// stylesSet : '',
	//protectedSource : ( /<\?[\s\S]*?\?>/g ),   // PHP code
	// FormatSource	 : true,
	// FormatOutput : true,
	//extraPlugins : 'bbcode',
	//fullPage : true,   // กำหนดให้สามารถแก้ไขแบบเโค้ดเต็ม คือมีแท็กตั้งแต่ <html> ถึง </html> 
	
	//------ Set Tool ----------------------------------// 	
	toolbar : [ // Start  Set Toolbar  
					['Source','-','Save','NewPage','Preview','-','Templates'],
					['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
					['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
					['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
					'/',
					['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
					['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
					['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					['Link','Unlink','Anchor'],
					['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
					'/',
					['Styles','Format','Font','FontSize'],
					['TextColor','BGColor'],
					['Maximize', 'ShowBlocks','-','About']
				],  // End   Set Toolbar  
				
 

		//--------- Add ckfinder -------------------------------------------------------//				
		filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'		
				
		 

} // End  var cke_config

 
/*
ฟังก์ฃัน InsertHTML สามารถประยุกต์ใช้งานกรณีใช้กับเว็บบอร์ด และมีการอ้างอิง ความคิดเห็น
ฟังก์ชัน SetContents สามารถประยุกต์ใช้ Reset ค่าใน CKEditor ให้ว่างหรือล้างค่าได้
ฟังก์ชัน GetContents สามารถประยุกต์ไว้สำหรับเช็คว่า CKEditor มีค่าว่าหรือไม่ก่อน Submit
ฟังก์ชัน ExecuteCommand สามารถไว้ใช้งานเพิ่มเรียกใช้คำสั่ง ของ CKEditor จากภายนอก เช่น
ExecuteCommand('link',editorObj) เป็นต้น
*/

function InsertHTML(htmlValue,editorObj){// ฟังก์ขันสำหรับไว้แทรก HTML Code
	if(editorObj.mode=='wysiwyg'){
		editorObj.insertHtml(htmlValue);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}	
}
function SetContents(htmlValue,editorObj){ // ฟังก์ชันสำหรับแทนที่ข้อความทั้งหมด
	editorObj.setData(htmlValue);
}
function GetContents(editorObj){// ฟังก์ชันสำหรับดึงค่ามาใช้งาน
	return editorObj.getData();
}
function ExecuteCommand(commandName,editorObj){// ฟังก์ชันสำหรับเรียกใช้ คำสั่งใน CKEditor
	if(editorObj.mode=='wysiwyg'){
		editorObj.execCommand(commandName);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}
}
