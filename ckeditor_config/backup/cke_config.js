﻿// JavaScript Document
var cke_config={
	skin:'v2', // kama | office2003 | v2
	language:'th', // th / en and more.....
	removePlugins : 'elementspath' , //remove show path
	extraPlugins :'uicolor',// เรียกใช้ plugin ให้สามารถแสดง UIColor Toolbar ได้
	uiColor :'#FFFF00', // กำหนดสีของ ckeditor
	extraPlugins : 'autogrow', // เรียกใช้ plugin ให้สามารถขยายขนาดความสูงตามเนื้อหาข้อมูล
	//extraPlugins : 'bbcode',
	autoGrow_maxHeight : 600,	 // กำหนดความสูงตามเนื้อหาสูงสุด ถ้าเนื้อหาสูงกว่า จะแสดง scrollbar
	removePlugins : 'elementspath' ,
	enterMode: 1,// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div> 
	shiftEnterMode	:2,// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>
	forceEnterMode : true,
	height :200, // กำหนดความสูง
	width :380, // กำหนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
/*		fullPage : true, // กำหนดให้สามารถแก้ไขแบบเโค้ดเต็ม คือมีแท็กตั้งแต่ <html> ถึง </html>*/
	toolbar : [
					// ['Source','-','Templates'],
					 ['Bold','Italic','Underline','TextColor' ,  'BackgroudColor'  ],
					//['Strike','NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
					//['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					//['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','UIColor']
				]
}



/*
ฟังก์ฃัน InsertHTML สามารถประยุกต์ใช้งานกรณีใช้กับเว็บบอร์ด และมีการอ้างอิง ความคิดเห็น
ฟังก์ชัน SetContents สามารถประยุกต์ใช้ Reset ค่าใน CKEditor ให้ว่างหรือล้างค่าได้
ฟังก์ชัน GetContents สามารถประยุกต์ไว้สำหรับเช็คว่า CKEditor มีค่าว่าหรือไม่ก่อน Submit
ฟังก์ชัน ExecuteCommand สามารถไว้ใช้งานเพิ่มเรียกใช้คำสั่ง ของ CKEditor จากภายนอก เช่น
ExecuteCommand('link',editorObj) เป็นต้น
*/

function InsertHTML(htmlValue,editorObj){// ฟังก์ขันสำหรับไว้แทรก HTML Code
	if(editorObj.mode=='wysiwyg'){
		editorObj.insertHtml(htmlValue);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}	
}
function SetContents(htmlValue,editorObj){ // ฟังก์ชันสำหรับแทนที่ข้อความทั้งหมด
	editorObj.setData(htmlValue);
}
function GetContents(editorObj){// ฟังก์ชันสำหรับดึงค่ามาใช้งาน
	return editorObj.getData();
}
function ExecuteCommand(commandName,editorObj){// ฟังก์ชันสำหรับเรียกใช้ คำสั่งใน CKEditor
	if(editorObj.mode=='wysiwyg'){
		editorObj.execCommand(commandName);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}
}
