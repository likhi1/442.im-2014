﻿// JavaScript Document
var cke_config={
	
	//------ Set Config Gernaral -----------------------------------// 
	skin:'v2', // kama | office2003 | v2
	//uiColor :'#FFFF00', // กำหนดสีของ ckeditor
	language:'th', // th / en and more..... 
	enterMode : 2,// กดปุ่ม Shift กับ Enter     1= แทรกแท็ก <p>       2 = แทรก <br>       3 = แทรก <div> 
	shiftEnterMode : 2, // กดปุ่ม Enter          1= แทรกแท็ก <p>       2 = แทรก <br>       3 = แทรก <div>
	forcePasteAsPlainText : true,   // Paste Pain Text
	
	height :200, // กำหนดความสูง
	width :380, // กำหนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
    removePlugins : 'elementspath' , //remove show path
	toolbarCanCollapse : false ,   //not allow toolbar collapse
	resize_enabled : false, //remove resize
	
	
	 
	//------ Set Plugin  -----------------------------------//  
	//  bbcode  =  อนุญาติให้ใช้ bb tag
	//  autogrow  = Text Area  ยืดตามข้อมูล และความสูงที่กำหนด
    //  pastetext  =  
	 extraPlugins : 'autogrow',  
	 autoGrow_maxHeight : 300,	 // property  of plugin  autogrow  ( กำหนดความสูงตามเนื้อหาสูงสุด ถ้าเนื้อหาสูงกว่า จะแสดง scrollbar)
	
	 
   
	//------ Set Tool ----------------------------------// 	
	toolbar : [ // Start  Set Toolbar  
	
					// ['Source','-','Templates'],
					['Bold','Italic','Underline','Strike', '-', 'BulletedList','NumberedList'], ['TextColor','BGColor'],
					//   '/',
					//[ 'Outdent','Indent','Blockquote'], 
					//['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','UIColor']
					//   '/',
					//['Link','Unlink'],  ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],  
					//['Font','FontSize'],  ['About'] 
					
				]   // End  Set Toolbar 
	
}



/*
ฟังก์ฃัน InsertHTML สามารถประยุกต์ใช้งานกรณีใช้กับเว็บบอร์ด และมีการอ้างอิง ความคิดเห็น
ฟังก์ชัน SetContents สามารถประยุกต์ใช้ Reset ค่าใน CKEditor ให้ว่างหรือล้างค่าได้
ฟังก์ชัน GetContents สามารถประยุกต์ไว้สำหรับเช็คว่า CKEditor มีค่าว่าหรือไม่ก่อน Submit
ฟังก์ชัน ExecuteCommand สามารถไว้ใช้งานเพิ่มเรียกใช้คำสั่ง ของ CKEditor จากภายนอก เช่น
ExecuteCommand('link',editorObj) เป็นต้น
*/

function InsertHTML(htmlValue,editorObj){// ฟังก์ขันสำหรับไว้แทรก HTML Code
	if(editorObj.mode=='wysiwyg'){
		editorObj.insertHtml(htmlValue);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}	
}
function SetContents(htmlValue,editorObj){ // ฟังก์ชันสำหรับแทนที่ข้อความทั้งหมด
	editorObj.setData(htmlValue);
}
function GetContents(editorObj){// ฟังก์ชันสำหรับดึงค่ามาใช้งาน
	return editorObj.getData();
}
function ExecuteCommand(commandName,editorObj){// ฟังก์ชันสำหรับเรียกใช้ คำสั่งใน CKEditor
	if(editorObj.mode=='wysiwyg'){
		editorObj.execCommand(commandName);
	}else{
		alert( 'You must be on WYSIWYG mode!');
	}
}
