<?php
session_start ();
require_once ('common/global.php');
require_once ('common/connect.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/global.css" />
<script src="js/jquery_last.js"></script>
<script src="js/global.js"></script>


<form action="account_open_script.php" method="post" target=""
	name="frm_accOpen" onsubmit="return chkAccount() ; ">
	<div class="pageAccOpen">
		<div>
			<label>บริการที่ต้องการสมัคร :</label> <select name="SerIdRef"
				class="select" id="SerIdRef">
				<option selected="selected" value="">เลือกบริการ</option>
    
   <?php
			$sql = "select * from tb_acc_service   ";
			$result = mysql_query ( $sql, $link );
			while ( $r = mysql_fetch_array ( $result ) ) {
				?>
<option value="<?php echo $r['SerId'] ?>"><?php echo $r['SerName'] ?>	</option>
 
  <?php
			} // end while
			?>                                  
    </select><span class="star">*</span>
		</div>
		<div>
			<label>Username :</label> <input type="text" name="AccUser"
				id="AccUser" /><span class="star">*</span>
		</div>
		<div>
			<label>Password :</label> <input type="text" name="AccPass"
				id="AccPass" /><span class="star">*</span>
		</div>
		<div>
			<label>Retype-Password :</label> <input type="text" name="AccPass2"
				id="AccPass2" /><span class="star">*</span>
		</div>
		<div>
			<label>อีเมล :</label> <input type="text" name="AccEmail"
				id="AccEmail" /><span class="star">*</span>
		</div>
		<div>
			<label>ชื่อ :</label> <input type="text" name="AccNameF"
				id="AccNameF" /><span class="star">*</span>

		</div>
		<div>
			<label>นามสกุล :</label> <input type="text" name="AccNameL"
				id="AccNameL" /><span class="star">*</span>
		</div>

		<div>
			<label>เพศ :</label> <input type="radio" name="AccGen" value="1"
				id="AccGen" checked="checked" /> ชาย <input type="radio"
				name="AccGen" value="2" id="AccGen" /> หญิง
		</div>
		<div>
			<label>วันที่เกิด :</label> <select name="AccBirthDay"
				id="AccBirthDay">
				<option selected="selected" value="">วันที่</option>
     <?php
					for($i = 1; $i <= 31; $i ++) {
						$v = ($i < 10 ? "0" . $i : $i);
						echo '<option value="' . $v . '">' . $v . '</option>';
					}
					?>
    </select> <select name="AccBirthMonth" id="AccBirthMonth">
				<option selected="selected" value="">เดือน</option>
     <?php
					for($i = 1; $i < 13; $i ++) {
						$v = ($i < 10 ? "0" . $i : $i);
						$arrayM = array (
								'',
								'มกราคม',
								'กุมภาพันธ์',
								'มีนาคม',
								'เมษายน',
								'พฤษภาคม',
								'มิถุนายน',
								'กรกฎาคม',
								'สิงหาคม',
								'กันยายน',
								'ตุลาคม',
								'พฤศจิกายน',
								'ธันวาคม' 
						);
						echo '<option value="' . $v . '">' . $arrayM [$i] . '</option>';
					}
					?>
    </select> <select name="AccBirthYear" id="AccBirthYear">
				<option selected="selected" value="">ปี</option>
      <?php
						for($i = 1; $i < 100; $i ++) {
							$y = 2556 - $i;
							echo '<option value="' . $y . '">' . $y . '</option>';
						}
						?>
    </select> <span class="star">*</span>
		</div>

		<div>
			<label>มือถือ :</label> <input type="text" name="AccMobile"
				id="AccMobile" /> <span class="star">*</span>

		</div>
		<div>
			<label>แฟกต์ :</label> <input type="text" name="AccFax" id="AccFax" />
		</div>

		<div>
			<label>ที่อยู่ :</label> <input type="text" name="AccAddress"
				id="AccAddress" />
		</div>
		<div>
			<label>จังหวัด :</label> <select name="AccProvince" class="select"
				id="AccProvince">
				<option selected="selected" value="">เลือกจังหวัด</option>
				<option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option> 
				<option value="กระบี่">กระบี่</option>
				<option value="กาญจนบุรี">กาญจนบุรี</option>
				<option value="กาฬสินธุ์">กาฬสินธุ์</option>
				<option value="กำแพงเพชร">กำแพงเพชร</option> 
				<option value="ขอนแก่น">ขอนแก่น</option>
				<option value="จันทบุรี">จันทบุรี</option> 
				<option value="ฉะเชิงเทรา">ฉะเชิงเทรา</option>
				<option value="ชลบุรี">ชลบุรี</option>
				<option value="ชัยนาท">ชัยนาท</option>
				<option value="ชัยภูมิ">ชัยภูมิ</option>
				<option value="ชุมพร">ชุมพร</option>
				<option value="เชียงราย">เชียงราย</option>
				<option value="เชียงใหม่">เชียงใหม่</option>
				<option value="ตรัง">ตรัง</option>
				<option value="ตราด">ตราด</option>
				<option value="ตาก">ตาก</option>
				<option value="นครนายก">นครนายก</option>
				<option value="นครปฐม">นครปฐม</option>
				<option value="นครพนม">นครพนม</option>
				<option value="นครราชสีมา">นครราชสีมา</option>
				<option value="นครศรีธรรมราช">นครศรีธรรมราช</option>
				<option value="นครสวรรค์">นครสวรรค์</option>
				<option value="นนทบุรี">นนทบุรี</option>
				<option value="นราธิวาส">นราธิวาส</option>
				<option value="น่าน">น่าน</option>
				<option value="บึงกาฬ">บึงกาฬ</option>
				<option value="บุรีรัมย์">บุรีรัมย์</option>
				<option value="ปทุมธานี">ปทุมธานี</option>
				<option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์</option>
				<option value="ปราจีนบุรี">ปราจีนบุรี</option>
				<option value="ปัตตานี">ปัตตานี</option>
				<option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา</option>
				<option value="พังงา">พังงา</option>
				<option value="พัทลุง">พัทลุง</option>
				<option value="พิจิตร">พิจิตร</option>
				<option value="พิษณุโลก">พิษณุโลก</option>
				<option value="เพชรบุรี">เพชรบุรี</option>
				<option value="เพชรบูรณ์">เพชรบูรณ์</option>
				<option value="แพร่">แพร่</option>
				<option value="พะเยา">พะเยา</option>
				<option value="ภูเก็ต">ภูเก็ต</option>
				<option value="มหาสารคาม">มหาสารคาม</option>
				<option value="มุกดาหาร">มุกดาหาร</option>
				<option value="แม่ฮ่องสอน">แม่ฮ่องสอน</option>
				<option value="ยะลา">ยะลา</option>
				<option value="ยโสธร">ยโสธร</option>
				<option value="ร้อยเอ็ด">ร้อยเอ็ด</option>
				<option value="ระนอง">ระนอง</option>
				<option value="ระยอง">ระยอง</option>
				<option value="ราชบุรี">ราชบุรี</option>
				<option value="ลพบุรี">ลพบุรี</option>
				<option value="ลำปาง">ลำปาง</option>
				<option value="ลำพูน">ลำพูน</option>
				<option value="เลย">เลย</option>
				<option value="ศรีสะเกษ">ศรีสะเกษ</option>
				<option value="สกลนคร">สกลนคร</option>
				<option value="สงขลา">สงขลา</option>
				<option value="สตูล">สตูล</option>
				<option value="สมุทรปราการ">สมุทรปราการ</option> 
				<option value="สมุทรสงคราม">สมุทรสงคราม</option> 
				<option value="สมุทรสาคร">สมุทรสาคร</option>
				<option value="สระแก้ว">สระแก้ว</option>
				<option value="สระบุรี">สระบุรี</option>
				<option value="สิงห์บุรี">สิงห์บุรี</option>
				<option value="สุโขทัย">สุโขทัย</option>
				<option value="สุพรรณบุรี">สุพรรณบุรี</option>
				<option value="สุราษฎร์ธานี">สุราษฎร์ธานี</option>
				<option value="สุรินทร์">สุรินทร์</option>
				<option value="หนองคาย">หนองคาย</option>
				<option value="หนองบัวลำภู">หนองบัวลำภู</option> 
				<option value="อ่างทอง">อ่างทอง</option>
				<option value="อุดรธานี">อุดรธานี</option>
				<option value="อุทัยธานี">อุทัยธานี</option>
				<option value="อุตรดิตถ์">อุตรดิตถ์</option>
				<option value="อุบลราชธานี">อุบลราชธานี</option>
				<option value="อำนาจเจริญ">อำนาจเจริญ</option>

			</select> <span class="star">*</span>
		</div>

		<div class="checkBox">
			<input type="checkbox" name="AccGetSms" id="AccGetSms" value="1"
				checked="checked" /> <span>เลือกขอรับเบอร์บัญชีธนาคารที่ต้องการทาง
				SMS เพื่อฝากเงินเข้าบัญชีในการเปิดเล่น
				(เมื่อทำการฝากเงินแล้วต้องแจ้งกับทางเว็บ 442 )</span>
		</div>
		<div class="checkBox">
			<input type="checkbox" name="AccGetPro" value="1" id="AccGetPro"
				checked="checked" /> <span>ขอรับข้อมูลข่าวสารและโปรโมชั่นจากทางเว็บ
				442</span>
		</div>




		<div>
			<label>Capcha : </label>  
    <?php  $text = ranDomStr(4) ;  //get str from  function ranDomStr() ;    ?>
		<span style="margin-right: 10px"> <img
				src="class/mycaptcha/createCaptcha.php?text=<?php  echo $text;?>">
			</span> <input type="text" name="Captcha" class="captcha"
				id="Captcha" /> <input type="hidden" name="Captcha2" class="captcha"
				id="Captcha2" value="<?php echo $text; ?>" /> <span class="star">*</span>
		</div>
		<div class="submit">
			<input type="submit" value="ตกลง" /> <input type="reset"
				value="รีเซ็ต" />
		</div>


	</div>
	<!---  class="pageAccOpen"    --->

</form>







