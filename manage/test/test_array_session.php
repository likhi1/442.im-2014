 <?php 
##### page1.php #####
// this starts the session 
session_start(); 

// echo variable from the session, we set this on our other page 
echo "Our color value is ".$_SESSION['color']; 
echo "Our size value is ".$_SESSION['size']; 
echo "Our shape value is ".$_SESSION['shape']; 
 
##### page2.php #####
session_start(); 
Print_r ($_SESSION);
?> 

 
 
<?php 
##### page1.php #####
session_start(); 

// makes an array 
$colors=array('red', 'yellow', 'blue'); 
// adds it to our session 
$_SESSION['color']=$colors; 
$_SESSION['size']='small'; 
$_SESSION['shape']='round'; 
print "Done";

##### page2.php #####
session_start(); 
Print_r ($_SESSION);
echo "<p>";

//echo a single entry from the array
echo $_SESSION['color'][2];
?> 



<?php
##### page1.php #####
session_start();
$my_array=array('cat', 'dog', 'mouse', 'bird', 'crocodile', 'wombat', 'koala', 'kangaroo');
$_SESSION['animals']=$my_array;
echo 'Putting array into a session variable';

##### page2.php #####
session_start();

// loop through the session array with foreach
foreach($_SESSION['animals'] as $key=>$value)
{
// and print out the values
echo 'The value of $_SESSION['."'".$key."'".'] is '."'".$value."'".' <br />';
}
?>