// JavaScript Document 

/*-----Define Function ------------------------------------------------------------------------------------------------------------------------------------------------------*/
var setSlowUpdate  =  1000*60  ; // 1 minute = 60000
var setFastUpdate   =  1000*60*60  ;

function update_fontend(){ 
	$.post("manage/script_data.php" ,  
			{  user_id_pk_fontend : $("#user_name_fontend").attr("rel")   ,  user_name_fontend : $("#user_name_fontend").html()   } , 
			function(data){  
			$("#im_message_detail").html(data);	
			})      
	setTimeout('update_fontend()', setSlowUpdate); 
} 
  
function update_backend(){   
		$.post("manage/script_data.php" ,  
				{  user_id_pk_backend : $("#user_name_backend").attr("rel")   ,  user_name_backend : $("#user_name_backend").html()   } , 
				function(data){  
				$("#im_message_detail").html(data);	
				})     
	setTimeout('update_backend()', setSlowUpdate); // update data from db  every 1 second
} 
   
function showListuser (){
	/*----- Select message  when select user -------------------*/ 				  
	$('#user_list li').each(function(index, element) {  
	$('#user_list li').click( function(){ 
	
	$("#im_message_area").show(); 
	
	/*-----add  user_name_backend    attr ("rel")  ----------------*/
	var user_name_backend  = $(this).children("a").html()  ;
	$("#user_name_backend").html(  user_name_backend  ); 
	
	var user_name_backend_rel  = $(this).children("a").attr("rel")  ;
	$("#user_name_backend").attr(  "rel" ,user_name_backend_rel  );  
	
	
	/*-----add  text  hidden  attr ("rel")  ----------------*/
	var user_name   = $(this).children("a").html()  ;
	$("#admin_message  #user_name").attr("value",   user_name ); 
	
	var user_id_pk  =  $(this).children().attr("rel") ; 
	$("#admin_message  #user_id_pk").attr("value",   user_id_pk  ); 
	
	
	/*----- update for Admin ---------------*/
	update_backend();   // if  Admin   get  User  start Update
	
	 
	$.post("manage/script_data.php" ,  {  
			user_id_pk : $(this).children("a").attr("rel")   ,   user_name :  $(this).children("a").html()  ,   user_status : $(this).attr("class")   } ,  
			function(data){   
			$("#im_message_detail").html(data);
			})
			
	 })// endclick 
	}); //end each 
}//end  showListuser
  
function update_select_user_list(){  
	$.post("manage/script_list_user.php" ,  { } ,  function(data){   
			$("#user_list").html(data);	  
			showListuser ()  // Run Function 
			})     
	 setTimeout('update_select_user_list()', setFastUpdate); // update data from db  every 1 second
}  


 function admin_status(){
	$.post( "manage/script_status_admin.php" , {} , function(data){ 
		$("#user_status").html("Live Chat - "+data); 
		    // alert(data);
			if(data == "ONLINE" ){
				$("#user_status").css("color",  "green" )
			}else{
				$("#user_status").css("color" ,  "red" )
			}   
		}) //end post
		
  setTimeout( 'admin_status()'  , setFastUpdate);
}//end  admin_status



$(document).ready(function(e) {   
/*------  Function  Run ---------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*----- check admin status -----------*/  
admin_status();  
/*-----  update  User  List   -----------*/
update_select_user_list()

/*----- update for User   -----------*/
if($("#user_name_fontend").html() != null ){   //  if get  User  start Update
//alert ("now update  user message");
update_fontend();   
} 
 
 
/*----- Event  open close message box -----------------------------------------------------------------------------------------------------------------------------------------*/
$('.btn_close').click(function(){ $("#im_message_area").hide(); })
$("#im_btn_status").click ( function (){  $("#im_message_area").show(); })  

 

/*----- Event  admin  send message ------------------------------------------------------------------------------------------------------------------------------------------------*/ 
$("#btn_admin_message").click(function (){  
/*----- check null -----------*/  
if ( $("#admin_message #user_id_pk").val() == ''  ){
	alert ("เลือก user  ก่อน");	
	return  false ;
	} 
if ( $("#admin_message :text").val()  == '' ){
	alert ("ใส่ข้อความด้วย");	 
	return  false ;
	}  
$('#im_message_detail').html("");// clear val be fir uodate 

//$("#btn_admin_message").parent().siblings( $("#im_message_detail") ).css({"background-color" :"blue"  , "display" : "block"});
$.post("manage/script_data.php" , 
			{ admin_message_detail  : $("#admin_message #admin_message_detail").val()   ,    
			user_id_pk  : $("#admin_message  #user_id_pk").val()  ,  
			user_name :  $("#user_name_backend").html()  } , 
			function(data){ 
			$('#im_message_detail').html(data); 
			$("#admin_message  #admin_message_detail").val("")  ; 
			})   
})//end click  
 
 
 
/*----- Event  register message --------------------------------------------------------------------------------------------------------------------------------------------------*/ 
$("#btn_register_message").click(function (){  
function checkemail(e){
	var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
	var returnval=emailfilter.test(e); 
	return returnval;
	//alert  ( returnval )   ; // if  test () match  RexExp   this  return  true    //  if not match  return   false 
}  
if( $("#register_message #name").val() == '' ||  $("#register_message #email").val()  == ''  ){     
	alert("ใส่ข้อความให้ครบด้วย") ;  
	return false;   
} 
else  if($("#register_message #name" ).val().length  >  10 ){ 
	alert("ตั้งชื่อไม่เกิน 10 ตัวอักษร") ;  
	return false;   
}
else  if(  checkemail( $("#register_message #email").val() )  ==  false  ){     
	alert("กรุณากรอกรูปแบบอีเมลให้ถูกต้อง") ;  
	return false;   
} 
$.post("manage/script_data.php" , 
		{ user_name  : $("#register_message #name").val()   ,  
		user_email : $("#register_message  #email").val() }  ,
		function(data){    
			if(data =="true" ){    	 
			alert("ชื่อนี้มีแล้ว กรุณาเปลี่ยนชื่อด้วย") ;  
			}else{     	
			window.location.reload(); //chang register to user message  
			$('#im_message_area').after(data);   	
			} 
		})
})//end click   



/*----- Event   user  message ------------------------------------------------------------------------------------------------------------------------------------------------*/ 
$("#btn_user_message").click(function (){  
/*----- check null -----------*/  
if ( $("#admin_message :text").val()  == '' ){
	alert ("ใส่ข้อความด้วย");	 
	return  false ;
}  
$.post("manage/script_data.php" , 
		{ user_message_detail  : $("#user_message  #user_message_detail").val()   ,  
		user_id_pk : $("#user_message  #user_id_pk").val()  ,    
		user_name : $("#user_message  #user_name").val()   }  , 
		function(data){ 
		//$('#im_message_detail').html(data); 
		$("#user_message  #user_message_detail").val("")  ;
		})
		
		})//end click  
}); //end ready


