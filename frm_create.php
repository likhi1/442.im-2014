<?php
ob_start();
session_start();
require_once('../class/db_connect.php');
require_once('../class/db_class.php');

$obj=new db_class();
$Hostname = $obj->Hostname;
if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod'))
{
  header('Location: http://442.im/iphone/Index.php');
  exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to your #1 Sports &amp; Casino Online</title>
<link href="../css/core.css" rel="stylesheet" media="all" />
<link  type="text/css"  rel="stylesheet"  href="../css/webboard.css"  />
<script type="text/javascript" src="../js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.4.min.js"></script> 
<script src="../Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">

function  check_add_quiz  (){ 
		var frm =document.frm_add_quiz  ; 
		
		if (frm.quiz_topic.value==''  ||  frm.quiz_message.value==''  ||   frm.quiz_name.value==''  ||  frm.quiz_email.value=='' ) { 
			alert("กรอกข้อมูลให้ครบด้วย");
			return false;	  
		}else {
			return true;
	}
 }
  
function OnSubmit()  { 
   var frm=document.form1;
	
	if (frm.PhoneNumber.value=='') {
		alert("กรุณากรอก หมายเลขเบอร์โทร ของคุณด้วยครับ");
		document.getElementById('PhoneNumber').focus();
		return false;
	}else if (!checkAllmobile(frm.PhoneNumber.value)) {
		alert("กรุณากรอก หมายเลขเบอร์โทร ให้ถูกต้องครับ");
		document.getElementById('PhoneNumber').focus();
		return false;
	}else{
		return true;
	}
}

 
function checkAllmobile(sValue){
	var re10digit=/^\d+$/;
	// search()
	if (sValue.search(re10digit)==-1){ //if match failed
		//alert("false");
		return false;
	}else{
		//alert("true");
		return true;
	}
}

function returnSend(txt) { 
	if (txt=='true') {
		alert('ระบบได้บันทึก เบอรฺ์โทร ของคุณเรียบร้อยแล้วครับ');
		window.location.href = window.location.href;
	} else {
		alert('ระบบไม่สามารถบันทึก เบอรฺ์โทรของคุณ กรุณาลองใหม่อีกครั้ง');
	}
	
}
function returnContactSend(txt) { 
	if (txt=='true') {
		alert('ระบบได้บันทึก ข้อมูล ของคุณเรียบร้อยแล้วครับ');
		window.location.href = window.location.href;
	} else {
		alert('ระบบไม่สามารถบันทึก ข้อมูลของคุณ กรุณาลองใหม่อีกครั้ง.');
	}
	
}
function ContactSubmit() { 
	var frm=document.frmSendEmail; 
	
	if (frm.email.value=='') {
		alert("กรุณากรอก อีเมลล์ ของคุณด้วยครับ");
		document.getElementById('email').focus();
		return false;
	}else if (!checkemail(frm.email.value)){
		alert("กรุณากรอก อีเมลล์ ให้ถูกต้องครับ");
		document.getElementById('email').focus();
		return false;
	}else if (frm.tel.value=='') {
		alert("กรุณากรอก หมายเลขโทรศัพท์ ของคุณด้วยครับ");
		document.getElementById('tel').focus();
		return false;
	}else if (!checkAllmobile(frm.tel.value)) {
		alert("กรุณากรอก หมายเลขโทรศัพท์ ให้ถูกต้องครับ");
		document.getElementById('tel').focus();
		return false;
	}else if(frm.securityCode.value==''){
		//alert(txt+'Your information:\n- Security Code');
		alert(txt+'โปรดกรอก Security Code');
		frm.securityCode.focus();
	}else{
		return true;
	}
}
/*Check Email Formating-----------------------------------------*/
var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
function checkemail(e){
	var returnval=emailfilter.test(e);
	return returnval;
}
</script>
 
</head>

<body><div id="preContainer">




<div id="container2"> 
   <?php include("inc_navigation_front.php")?>

<div id="content">
<div id="sideLeft">

<div class="section">
<div class="webboard">
<div  class="navWebboard"><a href='main.php'>ดูกระทู้ทั้งหมด</a>  &nbsp; 
<a href="frm_create.php">สร้างกระทู้  </a> &nbsp;
<a href='frm_login.php'>จัดการกระทู้</a> 
</div>


 

<form   method="post" action="add_quiz.php"  name="frm_add_quiz"  enctype="multipart/form-data"   >
  <table width="545" border="0" cellpadding="2" >
    <tr>
      <td width="94"><div  class="textLabel " >Topic :</div></td>
      <td width="437"><input type="text" name="quiz_topic"     maxlength="60" /><span  class="important"> *</span></td>
    </tr>
    <tr>
      <td><div class="textLabel "  >Message :</div></td>
      <td><textarea name="quiz_message" cols="45" rows="5"></textarea><span  class="important"> *</span></td>
    </tr>
    <tr>
      <td><div class="textLabel " >Name :</div></td>
      <td><input type="text" name="quiz_name"   maxlength="15" /><span  class="important"> *</span></td>
    </tr>
    <tr>
      <td><div class="textLabel " >Email :</div></td>
      <td><input type="text" name="quiz_email"   /><span  class="important"> *</span></td>
    </tr>
    <tr>
      <td><div class="textLabel " >Attach :</div></td>
      <td><input type="file" name="quiz_attach"    /> 
      *** ไม่เกิน 1 mb ***</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

 
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="button"   value="Submit"   
      onclick="JavaScript: return   check_add_quiz  ( )"  /></td>
    </tr>
  </table>
</form>

</div>

</div>





<div class="liveScore">
<table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><img src="../images/442_60.jpg" width="767" height="50" alt=""></td>
        </tr>
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><img src="../images/442_61.jpg" width="28" height="22" alt=""></td>
              </tr>
              <tr>
                <td><img src="../images/442_65.jpg" width="28" height="86" alt=""></td>
              </tr>
            </table></td>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/442_62.jpg" width="272" height="22" alt=""></td>
                    </tr>
                    <tr>
                      <td height="86" valign="top" background="../images/442_66.jpg"><div id="channel" style="float:left;">
                          <?php

	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='1' ";
	$Sql.=" ORDER BY ScoreID LIMIT 0, 4";
	$obj->query($Sql);
	
	while ($obj->moveNext()){
	
	$ScoreID=$obj->getField('ScoreID');
	$Name=$obj->getField('Name');
	$Link=$obj->getField('Link');
	
		if(!empty($Name)){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.gif" width="26" height="9" /><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
		}
	}
?>
                        </div>
                        <div id="channel" style="padding-left:10px; float:left;">
                          <?php

	
	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='1' ";
	$Sql.=" ORDER BY ScoreID LIMIT 4, 8;";
	$obj->query($Sql);
	
	while ($obj->moveNext()){

		$ScoreID=$obj->getField('ScoreID');
		$Name=$obj->getField('Name');
		$Link=$obj->getField('Link');
	
		if(!empty($Name)){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.gif" width="26" height="9" /><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
		}
	}
?>
                        </div></td>
                    </tr>
                  </table></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/442_63.jpg" width="262" height="22" alt=""></td>
                    </tr>
                    <tr>
                      <td height="86" valign="top" background="../images/442_67.jpg"><div id="channel" style="float:left;">
                          <?php

	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='2' ";
	$Sql.=" ORDER BY ScoreID LIMIT 0, 4;";
	$obj->query($Sql);
	
	while ($obj->moveNext()){
	
	$ScoreID=$obj->getField('ScoreID');
	$Name=$obj->getField('Name');
	$Link=$obj->getField('Link');
	
		if(!empty($Name)){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.jpg" width="26" height="9" /><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
		}
	}
?>
                        </div>
                        <div id="channel" style="padding-left:10px; float:left;">
                          <?php

	
	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='2' ";
	$Sql.=" ORDER BY ScoreID LIMIT 4, 8;";
	$obj->query($Sql);
	
	while ($obj->moveNext()){
		
		$ScoreID=$obj->getField('ScoreID');
		$Name=$obj->getField('Name');
		$Link=$obj->getField('Link');
	
	
		if(!empty($Name)){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.gif" width="26" height="9" /><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
		}
	}
?>
                        </div></td>
                    </tr>
                  </table></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../images/442_64.jpg" width="205" height="22" alt=""></td>
                    </tr>
                    <tr>
                      <td height="86" valign="top" background="../images/442_68.jpg"><div id="channel" style="float:left;">
                          <?php

	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='3' ";
	$Sql.=" ORDER BY ScoreID LIMIT 0, 4;";
	$obj->query($Sql);
	
	while ($obj->moveNext()){
	
	$ScoreID=$obj->getField('ScoreID');
	$Name=$obj->getField('Name');
	$Link=$obj->getField('Link');
	
		if(!empty($Name)){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.gif" width="26" height="9" /><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
		}
	}
?>
                        </div>
                        <div id="channel" style="padding-left:10px; float:left;">
                          <?php

	
	$Sql="SELECT * FROM ".$obj->tblScore;
	$Sql.=" Where `GameID`='3' ";
	$Sql.=" ORDER BY ScoreID LIMIT 4, 8;";
	$obj->query($Sql);
	
	while ($obj->moveNext()){
?>
                          <div style=" float:left; width:65px;"> <a href="<?=$Link;?>" target="_blank">
                            <?=$Name;?>
                            </a> </div>
                          <div style="float:left;margin-top:3px;"> <img src="../images/icon_live.gif" width="26" height="9"/><br />
                          </div>
                          <div style="clear:both; height:5px;"></div>
                          <?php
	}
?>
                        </div></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
</div>      
      
</div>
<div id="sideRight">
  <?php
  include ('side_right.php');  
?>
</div>
<div class="clear"></div>
</div>
 
 
</div>
 
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25080703-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
/*
var browser=navigator.userAgent.toLowerCase();   
var users_browser = (browser.indexOf('iPhone')!=-1);   
if (users_browser)   
{   
    document.location.href='442.im/iphone/Index.php';   
}*/
  })();
 
</script></div>
<script type="text/javascript">
swfobject.registerObject("kasikornID");
swfobject.registerObject("tmbID");
swfobject.registerObject("nakornluangID");
swfobject.registerObject("scbID");
swfobject.registerObject("krungthaiID");
swfobject.registerObject("ayudhyaID");
</script>
</body>
</html>