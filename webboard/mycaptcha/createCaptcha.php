<?php
// Set the content-type
header('Content-Type: image/png');

// Create the image
$im = imagecreatetruecolor(80, 30);

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, 399, 29, $gray);

// The text to draw
//$text = 'Testing...';
 $text  = $_GET[text]; // get var passing from  form.php 
 
// Replace path by your own font path
$font = "CaflischScriptPro-Regular.otf";

 // Add the text
imagettftext($im, 25, 0, 10, 20, $white, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im);
imagedestroy($im);
?>